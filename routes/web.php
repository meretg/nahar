<?php



Route::get('/', function () {
    return view('login');
});
Route::get('reception', function () {
    return view('index');
})->name('reception');
Route::get('/patient', function () {
    return view('patient');
})->name('patient');
Route::get('/add', function () {
    return view('adddetails');
});
Route::get('/bill', function () {
    return view('bill');
});
Route::get('/store', function () {
    return view('store');
});
Route::get('/admin_dashboard', function () {
    return view('admin_dashboard');
});
Route::get('/login', function () {
    return view('login');
});
Route::get('/reports', function () {
    return view('reports');
});
Route::post('user_login','UserController@login');
Route::get('user_logout','UserController@logout');
Route::resource('receptionist', 'receptionistController');
Route::post('patient', 'receptionistController@edit');//return view
Route::post('add', 'receptionistController@send');// get data for patient
Route::get('view', 'receptionistController@show');

Route::post('show', 'receptionistController@view');

//Route::group(['as' => 'receptionist.'], function () {
Route::post('search', 'receptionistController@search');
//});


Route::resource('billing', 'billingController');
Route::resource('examination', 'examinationController');
Route::post('addFile', 'examinationController@update');
Route::resource('operation', 'operationController');
Route::resource('store', 'StoreController');
Route::post('select', 'StoreController@select');
Route::get('check', 'billingController@check');
Route::get('showItems', 'billingController@showItems');


Route::resource('user', 'UserController');
Route::resource('reception', 'receptionController');
Route::get('update', 'UserController@update');
Route::post('destroy', 'UserController@destroy');
Route::post('deleteDoctor', 'UserController@deleteDoctor');
Route::post('addDoctor', 'UserController@addDoctor');
Route::post('addService', 'UserController@addService');
Route::post('editService', 'UserController@editService');
Route::post('editDoctor', 'UserController@editDoctor');
Route::post('viewDoctor', 'UserController@viewDoctor');
Route::post('delete', 'StoreController@destroy');

Route::post('update', 'StoreController@update');
Route::post('transform', 'StoreController@transform');
Route::post('filter', 'receptionistController@filter');

Route::resource('reports', 'reportController');
