@extends("master")
@section("content")
<section id="patient">
  <hr>
<h2 class="col-xs-6 " style="text-align:center; color:#243fb2;">  قسم المرضي  </h2>
<hr>
@if(Session::has('error'))

<div class="alert alert-danger alert-dismissible fade show" style="width: 70%; margin-top: 1%;">
   <button type="button" class="close" data-dismiss="alert">&times;</button>
{{ Session::get('error') }}
</div>
@endif
    <form class="form-horizontal" enctype="multipart/form-data" action="receptionist" method = "get">



  <div class="input-group mb-3">



  <div class="input-group-prepend">

    <select class="custom-select" name="select">
      <option  selected  value="none">البحث عن طريق</option>
      <option  value="name">اسم المريض</option>
      <option  value="code">كود المريض</option>
      <option  value="NID">رقم البطاقة</option>
      <option  value="phone">رقم الموبايل</option>
    </select>
  </div>

  <input type="text" name="data" class="form-control" aria-label="Text input with dropdown button">
  <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">

      <button type="submit" class="btn btn-white">
        <i class="fa fa-search"></i>  بحث
      </button>

</div>
</form>
<div>
    <form class="form-horizontal" enctype="multipart/form-data" action="receptionist" method = "get">
      <div class="row">
        <!-- <div class="col-md-1">
          </div> -->
          <label style="padding-right:2%;  color:#3250C9;">من:</label>

        <div class="col-md-3">
      <input class="form-control" required type="date" format="dd/MM/yyyy" name="from" id="from_date">
      <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
    </div>
    <!-- <div class="col-md-1" >
        </div> -->
<label style="padding-right:3%;  color:#3250C9;">الي:</label>

    <div class="col-md-3">

      <input class="form-control" required type="date" format="dd/MM/yyyy" name="to" id="to_date">
<input hidden value="1" name="filter">
    </div>
    <div class="col-md-1">
    <button type="submit" class="btn btn-white">
      <i class="fa fa-filter"></i> تصفية
    </button>
  </div>
</div>
    </form>
</div>


<hr>
<h2 class="col-xs-6 ">المرضي</h2>
<hr>

<div class="responsive">
<table class="table table-hover table-responsive">
  <col width="100">
  <col width="200">
  <col width="100">
  <col width="200">
  <col width="200">
  <col width="200">

    <thead>
      <tr>
        <th >كود المريض</th>
        <th >اسم المريض</th>
        <th >الحالة</th>
        <th >تاريخ المريض</th>
        <th >اجمالي المبلغ المدفوع</th>
        <th>عرض بيانات المريض</th>

      </tr>
    </thead>
    <tbody>
              @foreach($patients as $patient)
              <tr>
                        <td >{{$patient->code}} <br/></td>
                        <td>{{$patient->name}}</td>
                        <td>
                          @if($patient->status == 'pending')
                            لم يتم الدفع
                          @else
                            تم الدفع
                          @endif
                        </td>
                        <td>
                          @foreach($patient->services as $service)
                            @if($service->examinationType == "checkUp")
                              كشف
                            @elseif($service->examinationType == "normalExamination")
                              فحص عادي
                            @elseif($service->examinationType == "lasikExamination")
                              فحص ليزك
                            @elseif($service->examinationType == "normalOperation")
                              عمليات صغرى وكبرى
                            @elseif($service->examinationType == "lasikOperation")
                              عمليات ليزك
                              @else
                              اعاده كشف
                            @endif
                            <br>
                          @endforeach
                          <br>
                          <button type="button" class="btn btn-white" data-toggle="modal" data-target="#viewpatientModal"  onclick = "patient_view('{{$patient->id}}');" ><i class="fa fa-eye"></i></button>
                          <form id="add_form" class="form-horizontal" enctype="multipart/form-data" action="add" method = "post">
                            <input hidden value="{{$patient->id}}" name="id">
                            <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
                          <button type="submit" class="btn btn-success"  ><i class="fa fa-plus"></i></button>
                         </form>
                        </td>
                        <td>{{$patient->total}}</td>
                        <td><button type="button" class="btn btn-white" data-toggle="modal" data-target="#viewpatientdetails"  onclick = "patient_detailse('{{$patient->id}}');" ><i class="fa fa-eye"></i></button></td>
                    </tr>
              @endforeach
    </tbody>
  </table>
</div>
<div class="modal fade" id="viewpatientModal" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">عرض تفاصيل تاريخ المريض</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
          <form class="form-horizontal">
            <div  id="patient_data_detailse">
            </div>


<div class="form-group">
  <div class=" col-sm-12">
    <button  id="save_update" class="btn btn-white "data-dismiss="modal" >الغاء</button>
  </div>
</div>
      </form>
    </div>

  </div>
</div>
</div>

<div class="modal fade" id="viewpatientdetails" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">عرض بيانات المريض</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
          <form class="form-horizontal">

<div class="form-group row">
  <label class="control-label col-sm-3" >كود المريض :</label>
  <div class="col-sm-9">
    <span class="text" id="code"></span>
  </div>
</div>
<div class="form-group row">
  <label class="control-label col-sm-3" >اسم المريض :</label>
  <div class="col-sm-9">
    <span class="text" id="name"></span>

  </div>
</div>


<div class="form-group row">
  <label class="control-label col-sm-3" >رقم الموبايل : </label>
  <div class="col-sm-9">
    <span class="text" id="phone" name=""></span>

  </div>
</div>
<div class="form-group row">
  <label class="control-label col-sm-3" >العنوان : </label>
  <div class="col-sm-9">
    <span class="text" id="address" name=""></span>

  </div>
</div>
<div class="form-group row">
  <label class="control-label col-sm-3" >تاريخ الميلاد : </label>
  <div class="col-sm-9">
    <span class="text" id="dob" ></span>

  </div>
</div>
<div class="form-group row">
  <label class="control-label col-sm-4" >اسم ولي الامر :</label>
  <div class="col-sm-8">
    <span class="text" id="parentname"></span>

  </div>
</div>
<div class="form-group row">
  <label class="control-label col-sm-5" >رقم موبايل ولي الامر :</label>
  <div class="col-sm-7">
    <span class="text" id="parentphone"></span>

  </div>
</div>
<div class="form-group row">
  <label class="control-label col-sm-6" >رقم بطاقة المريض / ولي الامر :</label>
  <div class="col-sm-6">
    <span class="text" id="NID"></span>

  </div>
</div>
<div class="form-group row">
  <label class="control-label col-sm-3" >اسم المرافق :</label>
  <div class="col-sm-9">
    <span class="text" id="companionName" ></span>

  </div>
</div>
<div class="form-group row">
  <label class="control-label col-sm-5" >رقم موبايل المرافق :</label>
  <div class="col-sm-7">
    <span class="text" id="companionPhone"></span>

  </div>
</div>

</div>
<div class="form-group ">
  <div class=" col-sm-12">
    <button  class="btn btn-white float-left"data-dismiss="modal" >الغاء</button>
  </div>
</div>
      </form>
    </div>

  </div>
</div>

</section>
<script>

function patient_view(id)
{
  var html="";

  $.ajax({
              url: "{{ URL::to('view') }}",
              type: "get",
              dataType: 'json',
              data: {"id":id},
              success: function(response)
              {


                for(var i=0;i<response.length;i++)
                {
                  if(response[i].status == 'pending')
                  {
                    status = 'لم يتم الدفع';
                  }
                  else
                  {
                    status = 'تم الدفع';
                  }

                  if(response[i].examinationType == "checkUp")
                  {
                    examinationType = ' كشف';
                  }
                  else if(response[i].examinationType == "normalExamination")
                  {
                    type=response[i].details.diagonistics;
                    examinationType = 'فحص عادي';
                  }
                  else if(response[i].examinationType == "lasikExamination")
                  {
                    type=response[i].details.investigation;
                    examinationType = 'فحص ليزك';
                  }
                  else if(response[i].examinationType == "normalOperation")
                  {
                    type=response[i].details.operation;
                    examinationType = ' عمليات صغرى وكبرى';
                  }
                  else
                  {
                    type=response[i].details.treatment;
                    examinationType =  'عمليات ليزك ';
                  }
                  if(response[i].serviceStatus=='pending')
                  {
                    serviceStatus= ' قيد الانتظار ';
                    reason="";
                  }
                  else if(response[i].serviceStatus=='rejected')
                  {
                    serviceStatus=' تم الرفض ';
                    reason="السبب :"+response[i].details.reason;
                  }
                  else {
                    serviceStatus=' تم ';
                    reason="";

                  }

                  html+='<div id="accordion"><div class="card"><div class="card-header" aria-expanded="true" aria-controls="collapseOne" id="headingOne"><div class="row"><div class="col-md-10"><h5  class="mb-0"><button class="btn btn-link" >'+examinationType
                      +'</button></h5></div><div class="col-md-2">  <a  data-toggle="collapse" href="#'+i+
                      '" class="btn btn-white mb-0"  ><i class="fa fa-plus"></i></a></div></div></div><div id="'+i+'" class="collapse" aria-labelledby="headingOne" data-parent="#accordion"><div class="card-body">نوع الخدمه: '+type+'<br>التاريخ: '+response[i].created_at+'<br>اسم الدكتور: '+response[i].examinerDoc+'<br>العين: '+response[i].eye+'<br>السعر: '+response[i].owedMoney+'&nbsp;&nbsp;&nbsp;'+status+'<br>الحالة: '+serviceStatus+'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp'+reason+'</div></div></div></div></div>';
                    }
                    $("#patient_data_detailse").html(html);
              },



              error: function () {

                  alert("error");

              }
              });
}
function patient_detailse(id)
{
  $.ajax({
              url: "{{ URL::to('show') }}",
              type: "post",
              dataType: 'json',
            data: {"_token":$('#_token').val(),"id":id},
              success: function(response)
              {

                $("#code").html(response.code);
                $("#name").html(response.name);
                $("#phone").html(response.phone);
                $("#address").html(response.address);
                $("#dob").html(response.DOB);
                $("#parentname").html(response.parentName);
                $("#parentphone").html(response.parentPhone);
                $("#NID").html(response.NID);
                $("#companionName").html(response.companionName);
                $("#companionPhone").html(response.companionPhone);



                 },
              error: function () {

                  alert("error");

              }
              });

}
document.getElementById('from_date').max = new Date(new Date().getTime() - new Date().getTimezoneOffset() * 60000).toISOString().split("T")[0];
document.getElementById('to_date').max = new Date(new Date().getTime() - new Date().getTimezoneOffset() * 60000).toISOString().split("T")[0];
</script>
@stop
