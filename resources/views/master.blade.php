

<html>



<head>

    <meta charset="UTF-8">

    <title>Nahar-Admin</title>

    <meta http-equiv="content-type" content="application/vnd.ms-excel; charset=UTF-8">
    <meta charset="UTF-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="description" content="">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="robots" content="all,follow">



    <title>Nahar-Admin</title>

    <link rel="shortcut icon" href="{{ asset('/img/logo(512).png')}}">



    <!-- global stylesheets -->

    <!-- <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed" rel="stylesheet"> -->

    <link rel="stylesheet" href="{{ asset('/admin/css/bootstrap.min.css')}}">

    <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"> -->

<link rel="stylesheet" href="{{ asset('/font-awesome-4.7.0/css/font-awesome.min.css')}}">
      <!-- <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet"> -->

    <link rel="stylesheet" href="{{ asset('/admin/css/font-icon-style.css')}}">

    <link rel="stylesheet" href="{{ asset('/admin/css/style.default.css')}}" id="theme-stylesheet">



    <!-- Core stylesheets -->

    <link rel="stylesheet" href="{{ asset('/admin/css/ui-elements/card.css')}}">

    <link rel="stylesheet" href="{{ asset('/admin/css/style.css')}}">

    <link rel="stylesheet" href="{{ asset('/css/patient.css')}}">

    <link rel="stylesheet" href="{{ asset('/css/login.css') }}">



    <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>



<!-- Isolated Version of Bootstrap, not needed if your site already uses Bootstrap -->

<!-- <link rel="stylesheet" href="https://formden.com/static/cdn/bootstrap-iso.css" />  -->



<!-- Bootstrap Date-Picker Plugin -->

<!-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>

<script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script> -->









</head>



<body dir="rtl" >



<!--====================================================

                         MAIN NAVBAR

======================================================-->

    <header class="header">

        <nav class="navbar navbar-expand-lg ">



            <div class="container-fluid ">

                <div class="navbar-holder d-flex align-items-center justify-content-between">
                   <div class="col-md-2">
                    <div class="navbar-header">



                        <a id="toggle-btn" href="#" class="menu-btn " >

                            <span style="background:#0222A6;"></span>

                            <span style="background: #0222A6;"></span>

                            <span style="background: #0222A6;"></span>

                        </a>



</div><!-- ./wrapper -->
</div>



                    </div>

                    <div class="col-md-6">

                      <center class="system_name">   <span  class="navbar-brand">

                            <div class="brand-text brand-big hidden-lg-down"><img src="{{ asset('/img/rehab.png')}}" style="width:90px; " alt="Nahar" class="img-fluid"></div>

                            <div class="brand-text brand-small"><img src="{{ asset('/img/rehab.png')}}" alt="Logo" style="width:50px;" class="img-fluid"></div>

                        </span>مركز النهار لجراحات العيون والليزك</center>

                    </div>
<div class="col-md-2">
  <div class="row align-items-center justify-content-center">
اسم المستخدم : {{Auth::user()->userName}}
  </div>
                    <div class="d-flex flex-row-reverse">


                            <a class="btn btn-danger " id="logout" style=" color:white; height:1%; margin-top: 8%;"  href = "{{ url('user_logout') }}"  ><i class="fa fa-sign-out"></i> خروج</a>&nbsp; &nbsp; &nbsp;
                            <a class="btn btn-secondary"  href="#" id="refresh" onClick="refresh()" style=" color:white; height:1%; margin-top: 8%;"><i class="fa fa-refresh" aria-hidden="true"></i> اعادة التحميل</a>


                          </div>
                        </div>



                </div>

                <!-- <div class="pull-right ">

            <div id="logout_button">



            <div>

                <a href = "{{ url('admin_logout') }}" class="btn btn-danger">Logout</a>

              </div>



        </div>

          </div> -->



            </div>

        </nav>

    </header>



<!--====================================================

                        PAGE CONTENT

======================================================-->

    <div class="page-content d-flex align-items-stretch">



        <!--***** SIDE NAVBAR *****-->

          <nav class="side-navbar shrinked">

            <!-- <div class="sidebar-header d-flex align-items-center">

                <div class="avatar"><img src="{{ asset('/img/avatar-1.jpg')}}" alt="..." class="img-fluid rounded-circle"></div>

                <div class="title">

                    <h1 class="h4">Steena Ben</h1>

                </div>

            </div> -->

            <!-- <hr> -->

            <!-- Sidebar Navidation Menus-->

            <ul class="list-unstyled">
             @if(Auth::user()->superAdmin==1)
             <li> <a href="{{ url('/reception') }}"> <i class="fa fa-home"></i>قسم الاستقبال</a></li>
             <li> <a href="{{ url('/receptionist') }}"> <i class="fa fa-user"></i>قسم المرضي</a></li>
              <li> <a href="{{ url('/examination') }}"> <i class="fa fa-stethoscope"></i>قسم الفحوصات</a></li>
               <li> <a href="{{ url('/operation') }}"> <i class="fa fa-user-md"></i>قسم العمليات</a></li>
               <li> <a href="{{ url('/billing') }}"> <i class="fa fa-money"></i> الخزنة</a></li>
               <li> <a href="{{ url('/store') }}"> <i class="fa fa-archive"></i> المخازن</a></li>
               <hr>
               <li> <a href="{{ url('/user') }}"> <i class="fa fa-lock"></i> المديرين</a></li>
                <li> <a href="{{ url('/reports') }}"> <i class="fa fa-map"></i> التقارير</a></li>
               @else
             @foreach($loginroles as $loginrole)
              @if($loginrole->id==1)
                <li> <a href="{{ url('/reception') }}"> <i class="fa fa-home"></i>قسم الاستقبال</a></li>
                   @elseif($loginrole->id==2)
                <li> <a href="{{ url('/receptionist') }}"> <i class="fa fa-user"></i>قسم المرضي</a></li>
                   @elseif($loginrole->id==3)
                   <li> <a href="{{ url('/billing') }}"> <i class="fa fa-money"></i> الخزنة</a></li>

                 @elseif($loginrole->id==4)

                <li> <a href="{{ url('/examination') }}"> <i class="fa fa-stethoscope"></i>قسم الفحوصات</a></li>
                  @elseif($loginrole->id==5)
                <li> <a href="{{ url('/operation') }}"> <i class="fa fa-user-md"></i>قسم العمليات</a></li>
                @elseif($loginrole->id==6)
                <li> <a href="{{ url('/store') }}"> <i class="fa fa-archive"></i> المخازن</a></li>
                @else
                 <li> <a href="{{ url('/user') }}"> <i class="fa fa-lock"></i> المديرين</a></li>
                   <li> <a href="{{ url('/reports') }}"> <i class="fa fa-map"></i> التقارير</a></li>
                @endif
                @endforeach
                @endif

                <!-- <li><a href="#apps" aria-expanded="false" data-toggle="collapse"> <i class="icon-interface-windows"></i>Apps </a>

                    <ul id="apps" class="collapse list-unstyled">

                        <li><a href="calendar.html">Calendar</a></li>

                        <li><a href="email.html">Email</a></li>

                        <li><a href="media.html">Media</a></li>

                        <li><a href="invoice.html">Invoice</a></li>

                    </ul>

                </li>

                <li> <a href="chart.html"> <i class="fa fa-bar-chart"></i>Chart </a></li>

                <!-- <li><a href="#forms" aria-expanded="false" data-toggle="collapse"> <i class="fa fa-building-o"></i>Forms </a>

                    <ul id="forms" class="collapse list-unstyled">

                        <li><a href="basic-form.html">Basic Form</a></li>

                        <li><a href="form-layouts.html">Form Layouts</a></li>

                    </ul>

                </li>

                <li> <a href="maps.html"> <i class="fa fa-map-o"></i>Maps </a></li>

                <li><a href="#pages" aria-expanded="false" data-toggle="collapse"> <i class="fa fa-file-o"></i>Pages </a>

                    <ul id="pages" class="collapse list-unstyled">

                        <li><a href="faq.html">FAQ</a></li>

                        <li><a href="empty.html">Empty</a></li>

                        <li><a href="gallery.html">Gallery</a></li>

                        <li><a href="login.html">Log In</a></li>

                        <li><a href="register.html">Register</a></li>

                        <li><a href="search-result.html">Search Result</a></li>

                        <li><a href="404.html">404</a></li>

                    </ul>

                </li>

                <li> <a href="tables.html"> <i class="icon-grid"></i>Tables </a></li>

                <li><a href="#elements" aria-expanded="false" data-toggle="collapse"> <i class="fa fa-globe"></i>UI Elements </a>

                    <ul id="elements" class="collapse list-unstyled">

                        <li><a href="ui-buttons.html">Buttons</a></li>

                        <li><a href="ui-cards.html">Cards</a></li>

                        <li><a href="ui-progressbars.html">Progress Bar</a></li>

                        <li><a href="ui-timeline.html">Timeline</a></li>

                    </ul>

                </li>

            </ul><span class="heading">Extras</span>

            <ul class="list-unstyled">

                <li> <a href="#"> <i class="icon-picture"></i>Demo </a></li> -->

            </ul>



        </nav>

          @yield('content')

<div>





  </div>

      </div>


<script>



// $(document).ready(function() {

//   $(function() {

//   $('nav a[href^="/' + location.pathname.split("/")[1] + '"]').addClass('active');

// });

// });



</script>



    <!--Global Javascript -->



    <script src="{{ asset('/admin/js/jquery.min.js')}}"></script>

    <script src="{{ asset('/admin/js/popper.min.js')}}"></script>

    <script src="{{ asset('/admin/js/tether.min.js')}}"></script>

    <script src="{{ asset('/admin/js/bootstrap.min.js')}}"></script>

    <script src="{{ asset('/admin/js/jquery.cookie.js')}}"></script>

    <script src="{{ asset('/admin/js/jquery.validate.min.js')}}"></script>

    <script src="{{ asset('/admin/js/chart.min.js')}}"></script>

    <script src="{{ asset('/admin/js/front.js')}}"></script>

    <!-- <script src="{{ asset('/admin/js/admin.js')}}"></script> -->

    <script src="{{ asset('js/patient.js')}}"></script>

    <script src="{{ asset('js/admin.js')}}"></script>

<script>
// $('.date').datepicker({
//
//    format: 'yyyy-mm-dd'
//
//  });
var idleTime = 0;
$(document).ready(function () {
    //Increment the idle time counter every minute.
    var idleInterval = setInterval(timerIncrement, 60000); // 1 minute

    //Zero the idle timer on mouse movement.
    $(this).mousemove(function (e) {
        idleTime = 0;
    });
    $(this).keypress(function (e) {
        idleTime = 0;
    });
});

function timerIncrement() {
    idleTime = idleTime + 1;
    if (idleTime > 14) { // 15 minutes
         window.location="{{ url('user_logout') }}";

    }
}
//var test = 'http://somedomain.com/ijob-css/index.php/search/default/index/area/act?query=&activities_id=13&session_id=14&back=1';

// alert(test.substring(0, test.indexOf('?')));
// alert(window.location.pathname);
function refresh()
{
  location.href=window.location.pathname;
}
</script>

    <!--Core Javascript -->

    <!-- <script src="js/mychart.js"></script> -->



</body>



</html>
