@extends("master")
@section("content")
<div class="container" style="margin-right:0;">
  <hr>
    <h2 class="col-xs-6 " style="text-align:center; color:#243fb2;">المديرين</h2>
    <hr>
<ul class="nav nav-tabs" role="tablist" >
    <li class="nav-item">
      <a class="nav-link active" data-toggle="tab" href="#home">الموظفين</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" data-toggle="tab" href="#menu1">الاطباء</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" data-toggle="tab" href="#menu2">الخدمات</a>
    </li>

  </ul>
  <div class="tab-content">
    <div id="home" class="container tab-pane active"><br>
<section >
      <div class="container panel panel-default panel-body">
<hr>
<div class="row">
<h2 class="col">الموظفين</h2>
<button class="col-xs-2 btn btn-success ml-auto add_admin" data-toggle="modal" data-target="#addmemberModal"><i class="fa fa-plus"></i> اضافة موظف</button>
</div>
<hr>

 @if(Session::has('flash_message'))
    <div class="alert alert-info">
      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        {{ Session::get('flash_message') }}
    </div>
@endif
@if(Session::has('error'))
   <div class="alert alert-danger">
     <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
       {{ Session::get('error') }}
   </div>
@endif
@if($errors->any())
<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <div class="alert alert-danger">
        @foreach($errors->all() as $error)
            <p>{{ $error }}</p>
        @endforeach
    </div>
@endif

<div class="responsive">
<table class="table">
  <col width="100">
  <col width="200">
  <col width="200">
  <col width="150">
  <col width="100">
  <col width="100">

    <thead>
      <tr >

        <th>الاسم</th>
        <th>الصلاحيات</th>
        <th>رقم البطاقة</th>
        <th>الموبايل</th>
        <th>تعديل</th>
        <th>حذف</th>
      </tr>
    </thead>
    <tbody>
      <tr>

        @foreach($users as $user)

                  <td>{{$user->userName}} <br/></td>
                  <td>
                  @foreach($user->roles as $role)
                  {{$role->name}},
                  @endforeach
                  </td>
                  <td>{{$user->NID}}</td>
                  <td>{{$user->phone}}</td>
                  <td><button type="button" class="btn btn-white" data-toggle="modal" data-target="#editmemberModal"  onclick = "edit_user('{{$user->id}}');" id="user_Edit" ><i class="fa fa-edit"></i></button></td>
                <td><button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deletememberModal"  onclick = "delete_user('{{$user->id}}');" id="user_delete" >X</button></td>
              </tr>


        @endforeach

    </tbody>
  </table>

</div>
  <div class="modal fade" id="addmemberModal" role="dialog">
<div class="modal-dialog" >

  <!-- Modal content-->

  <div class="modal-content">
    <div class="modal-header">
      <h4 class="modal-title">اضافة موظف</h4>
      <button type="button" class="close" data-dismiss="modal">&times;</button>

    </div>
    <div class="modal-body">

       <!-- <form class="form-horizontal" enctype="multipart/form-data" action = "" method = ""> -->

<div class="form-group row">
    <label class="control-label col-sm-3" >الاسم :</label>
<div class="col-sm-9">
  <input type="text" class="form-control" id="member_name" required name = "userName" onblur="validateUsername();">
  <span id="member_nameinvalid" style="color:red;"></span>
</div>
</div>
<div class="form-group row">
    <label class="control-label col-sm-3" >الرقم السري :</label>
<div class="col-sm-9">
  <input type="password" class="form-control" id="member_password" required name = "password" onblur="validatepassword();">
  <span id="member_passinvalid" style="color:red;"></span>
</div>
</div>



<div class="form-group row" >
  <label class="control-label col-sm-3" >الصلاحيات :</label>
  <div class="col-sm-9">
    <div class="row">
        <div class="col-sm-6">
    <input type="checkbox" id="1" name="roles"
       value="1" />
<label for="1">الاستقبال</label>
</div>
  <div class="col-sm-6">
<input type="checkbox" id="2" name="roles"
   value="2" />
<label for="2">المرضي</label>
</div>
</div>
<div class="row">
    <div class="col-sm-6">
<input type="checkbox" id="3" name="roles"
   value="3" />
<label for="3">الخزنة</label>
</div>

    <div class="col-sm-6">
<input type="checkbox" id="4" name="roles"
   value="4" />
<label for="4">الفحوصات</label>
</div>
</div>
<div class="row">
    <div class="col-sm-6">
<input type="checkbox" id="5" name="roles"
   value="5" />
<label for="5">العمليات</label>
</div>

    <div class="col-sm-6">
<input type="checkbox" id="6" name="roles"
   value="6" />
<label for="6">المخازن</label>
</div>
</div>
<input type="checkbox" id="7" name="roles"
   value="7" />
<label for="7">المديرين</label>
  <span id="member_rolesinvalid" style="color:red;"></span>
</div>
</div>




  <div class="form-group row">
    <label class="control-label col-sm-3">رقم البطاقة :</label>
    <div class="col-sm-9">
      <input type="emial" class="form-control" id="member_NID" name = "NID" required onblur="validateNID_member()">
      <span id="member_NIDinvalid" style="color:red;"></span>
    </div>

</div>
<div class="form-group row">
  <label class="control-label col-sm-3">الموبايل :</label>
  <div class="col-sm-9">
    <input type="text" class="form-control" id="member_phone" name = "phone" required onblur="validatephone_member()">
    <span id="member_phoneinvalid" style="color:red;"></span>
    <span id="adderror" style="color:red;"></span>
  </div>

</div>



<input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
<div class="form-group">
<div class=" row align-items-center justify-content-center">
  <button type="submit" class="btn btn-white" onclick="addmember()"  id="add_save" >اضافة</button>
</div>
</div>
<!-- </form> -->
</div>
</div>
</div>
</div>

<div class="modal fade" id="editmemberModal" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">تعديل بيانات الموظف</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>

      </div>
      <div class="modal-body">
          <!-- <form class="form-horizontal" enctype="multipart/form-data" action = "user" method = "post"> -->


            <div class="form-group row">
                <label class="control-label col-sm-3" >الاسم :</label>
            <div class="col-sm-9">
              <input type="text" class="form-control" id="oldmember_name" required name = "userName" onblur="oldvalidateUsername();">
              <span id="oldmember_nameinvalid" style="color:red;"></span>
            </div>
            </div>
            <!-- <div class="form-group row">
                <label class="control-label col-sm-3" >الرقم السري :</label>
            <div class="col-sm-9">
              <input type="password" class="form-control" id="oldmember_password" required name = "password" onblur="oldvalidatepassword();">
              <span id="oldmember_passinvalid"></span>
            </div>
            </div> -->
            <div class="form-group row" >
              <label class="control-label col-sm-3" >الصلاحيات :</label>
              <div class="col-sm-9">
                <div class="row">
                    <div class="col-sm-6">
                <input type="checkbox" id="1" name="roles"
                   value="1" />
            <label for="1">الاستقبال</label>
          </div>
              <div class="col-sm-6">
            <input type="checkbox" id="2" name="roles"
               value="2" />
            <label for="2">المرضي</label>
          </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
            <input type="checkbox" id="3" name="roles"
               value="3" />
            <label for="3">الخزنة</label>
          </div>

              <div class="col-sm-6">
            <input type="checkbox" id="4" name="roles"
               value="4" />
            <label for="4">الفحوصات</label>
          </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
            <input type="checkbox" id="5" name="roles"
               value="5" />
            <label for="5">العمليات</label>
          </div>

              <div class="col-sm-6">
            <input type="checkbox" id="6" name="roles"
               value="6" />
            <label for="6">المخازن</label>
          </div>
        </div>
            <input type="checkbox" id="7" name="roles"
               value="7" />
            <label for="7">المديرين</label>
<span id="oldmember_rolesinvalid" style="color:red;"></span>
            </div>
            </div>




              <div class="form-group row">
                <label class="control-label col-sm-3">رقم البطاقة :</label>
                <div class="col-sm-9">
                  <input type="emial" class="form-control" id="oldmember_NID" name = "NID" required onblur="oldvalidateNID_member()">
                   <span id="oldmember_NIDinvalid" style="color:red;"></span>
                </div>

            </div>
            <div class="form-group row">
              <label class="control-label col-sm-3">الموبايل :</label>
              <div class="col-sm-9">
                <input type="text" class="form-control" id="oldmember_phone" name = "phone" required onblur="oldvalidatephone_member()">
                <span id="oldmember_phoneinvalid" style="color:red;"></span>
              </div>

            </div>




  <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
  <input type="hidden" name="id" id="editmember_id">
<div class="form-group">
  <div class=" row align-items-center justify-content-center">
    <button  id="save_update"  type="submit" onclick="edit()"  class="btn btn-white" >حفظ التغيرات</button>
  </div>
</div>
      <!-- </form> -->
    </div>

  </div>
</div>

</div>

<div class="modal fade" id="deletememberModal" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">هل انت متأكد ؟</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>


      </div>
      <div class="modal-body">
       <strong style="color:red;">
           سوف تقوم بحذف جميع بيانات هذا الموظف
       </strong>
      </div>
      <div class="modal-footer">

        <form class="form-horizontal" action = "destroy" method = "post">


          <input type="hidden" name="id" id="member_id">
          <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
          <button type="button" class="btn btn-blue" data-dismiss="modal" onclick="this.form.submit()">نعم</button>
          <button type="button" class="btn btn-blue" data-dismiss="modal">لا</button>
        </form>


      </div>
    </div>

  </div>
</div>
</section>
</div>

<div id="menu1" class="container tab-pane "><br>
  <section >
        <div class="container panel panel-default panel-body">
  <hr>
  <div class="row">
  <h2 class="col">الاطباء</h2>
  <button class="col-xs-2 btn btn-success ml-auto add_admin" data-toggle="modal" data-target="#adddoctorModal"><i class="fa fa-plus"></i> اضافة طبيب</button>
  </div>
  <hr>

   @if(Session::has('flash_message'))
      <div class="alert alert-info">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
          {{ Session::get('flash_message') }}
      </div>
  @endif
  @if($errors->any())
  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
      <div class="alert alert-danger">
          @foreach($errors->all() as $error)
              <p>{{ $error }}</p>
          @endforeach
      </div>
  @endif

  <div class="responsive">
  <table class="table">
    <col width="100">
    <col width="200">
    <col width="200">
    <col width="150">
    <col width="100">
    <col width="100">

      <thead>
        <tr >

          <th>الاسم</th>
          <th>التخصص</th>

          <th>الموبايل</th>
          <th>تعديل</th>
          <th>حذف</th>
        </tr>
      </thead>
      <tbody>
        <tr>

          @foreach($doctors as $doctor)
                     <td>{{$doctor->docName}} <br/></td>
                       <td>
                       @foreach($doctor->types as $type)
                       @if($type->type_id==1)
                       طبيب جراح ,
                       @endif
                       @if($type->type_id==2)
                       طبيب فحص ,
                       @endif
                       @if($type->type_id==3)
                       طبيب تخدير ,
                       @endif
                       @if($type->type_id==4)
                       مساعد ,
                       @endif
                       @if($type->type_id==5)
                      دكتور داتا ,
                       @endif
                       @endforeach
                     </td>
                       <td>{{$doctor->phone}}</td>
                       <td><button type="button" class="btn btn-white" data-toggle="modal" data-target="#editdoctorModal"  onclick = "edit_doctor('{{$doctor->id}}');" id="user_Edit" ><i class="fa fa-edit"></i></button></td>
                     <td><button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deletedoctorModal"  onclick = "delete_doctor('{{$doctor->id}}');" id="user_delete" >X</button></td>
                   </tr>
                       @endforeach


      </tbody>
    </table>

  </div>
    <div class="modal fade" id="adddoctorModal" role="dialog">
  <div class="modal-dialog" >

    <!-- Modal content-->

    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">اضافة طبيب</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>

      </div>
      <div class="modal-body">

         <!-- <form class="form-horizontal" enctype="multipart/form-data" action = "addDoctor" method = "post"> -->

  <div class="form-group row">
      <label class="control-label col-sm-3" >الاسم :</label>
  <div class="col-sm-9">
    <input type="text" class="form-control" id="doctor_name" required name = "docName" onblur="validatedocUsername();">
    <span id="doctor_nameinvalid" style="color:red;"></span>
  </div>
  </div>


  <div class="form-group row" >
    <label class="control-label col-sm-3" >التخصص :</label>
    <div class="col-sm-9">
      <div class="row">
          <div class="col-sm-6">
      <input type="checkbox" id="surgeon" name="type"
         value="surgeon" />
  <label for="surgeon">طبيب جراح</label>
  </div>
    <div class="col-sm-6">
  <input type="checkbox" id="checkUp" name="type"
     value="checkup" />
  <label for="checkup">طبيب فحص</label>
  </div>
  </div>
  <div class="row">
      <div class="col-sm-6">
  <input type="checkbox" id="anesthetist" name="type"
     value="anesthetist" />
  <label for="anesthetist">طبيب تخدير</label>
  </div>

      <div class="col-sm-6">
  <input type="checkbox" id="assistantDoctor" name="type"
     value="assistantDoctor" />
  <label for="assistantDoctor">المساعد</label>
  </div>
  </div>
  <div class="row">
      <div class="col-sm-6">
  <input type="checkbox" id="data" name="type"
     value="data" />
  <label for="data">دكتور داتا</label>
  </div>
</div>
</div>
<span id="doctor_typeinvalid" style="color:red;"></span>
</div>


  <!-- <div class="form-group row" >
    <label class="control-label col-sm-3" >التخصص :</label>
    <div class="col-sm-9">
      <select class="custom-select" name= "type" id="select_doctor" >
       <option  value="surgeon/checkUp" >طبيب فحص / طبيب جراح</option>
       <option  value="anesthetist" >طبيب تخدير</option>
       <option  value="assistantDoctor" >مساعد</option>
       </select>
  </div>
  </div> -->







  <div class="form-group row">
    <label class="control-label col-sm-3">الموبايل :</label>
    <div class="col-sm-9">
      <input type="text" class="form-control" id="doctor_phone" name = "phone" required onblur="validatedocphone_member()">
      <span id="doctor_phoneinvalid" style="color:red;"></span>
    </div>

  </div>



  <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
  <div class="form-group">
  <div class=" row align-items-center justify-content-center">
    <button type="submit" class="btn btn-white" onclick="adddoctor();"   id="add_save" >اضافة</button>
  </div>
  </div>
  <!-- </form> -->
  </div>
  </div>
  </div>
  </div>

  <div class="modal fade" id="editdoctorModal" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title"> تعديل بيانات الطبيب </h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>

        </div>
        <div class="modal-body">
            <!-- <form class="form-horizontal" enctype="multipart/form-data" action = "editDoctor" method = "post"> -->


              <div class="form-group row">
                  <label class="control-label col-sm-3" >الاسم :</label>
              <div class="col-sm-9">
                <input type="text" class="form-control" id="olddoctor_name" required name = "docName" onblur="oldvalidateUsername();">
                <span id="olddoctor_nameinvalid" style="color:red;"></span>
              </div>
              </div>


              <div class="form-group row" >
                <label class="control-label col-sm-3" >التخصص :</label>
                <div class="col-sm-9">
                  <div class="row">
                      <div class="col-sm-6">
                  <input type="checkbox" id="surgeon" name="type"
                     value="surgeon" />
              <label for="surgeon">طبيب جراح</label>
              </div>
                <div class="col-sm-6">
              <input type="checkbox" id="checkUp" name="type"
                 value="checkUp" />
              <label for="checkUp">طبيب فحص</label>
              </div>
              </div>
              <div class="row">
                  <div class="col-sm-6">
              <input type="checkbox" id="anesthetist" name="type"
                 value="anesthetist" />
              <label for="anesthetist">طبيب تخدير</label>
              </div>

                  <div class="col-sm-6">
              <input type="checkbox" id="assistantDoctor" name="type"
                 value="assistantDoctor" />
              <label for="assistantDoctor">المساعد</label>
              </div>
              </div>
              <div class="row">
                  <div class="col-sm-6">
              <input type="checkbox" id="" name="type"
                 value="data" />
              <label for="">دكتور داتا</label>
              </div>
            </div>
          </div>
          <span id="olddoctor_typeinvalid" style="color:red;"></span>
        </div>


                  <!-- <div class="col-sm-6">
              <input type="checkbox" id="6" name="roles"
                 value="6" />
              <label for="6">المخازن</label>
              </div>
              </div>
              <input type="checkbox" id="7" name="roles"
                 value="7" />
              <label for="7">المديرين</label>
                <span id="member_rolesinvalid" style="color:red;"></span>
              </div>
              </div> -->



              <!-- <div class="form-group row" >
                <label class="control-label col-sm-3" >التخصص :</label>
                <div class="col-sm-9">
                  <select class="custom-select" name="type" id="oldselect_doctor" >
                  <option value="none" ></option>
                   <option  value="surgeon/checkUp" >طبيب جراح /طبيب فحص</option>
                   <option  value="anesthetist" >طبيب تخدير</option>
                   <option  value="assistantDoctor" >مساعد</option>
                   </select>
              </div>
              </div> -->







              <div class="form-group row">
                <label class="control-label col-sm-3">الموبايل :</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" id="olddoctor_phone" name = "phone" required onblur="oldvalidatephone_member()">
                  <span id="olddoctor_phoneinvalid" style="color:red;"></span>
                </div>

              </div>




    <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
    <input type="hidden" name="id" id="editdoctor_id">
  <div class="form-group">
    <div class=" row align-items-center justify-content-center">
      <button  id="save_update"  type="submit" onclick="editdoctor();"  class="btn btn-white"data-dismiss="modal" >حفظ التغيرات</button>
    </div>
  </div>
        </form>
      </div>

    </div>
  </div>

  </div>

  <div class="modal fade" id="deletedoctorModal" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">هل انت متأكد ؟</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>


        </div>
        <div class="modal-body">
         <strong style="color:red;">
  سوف تقوم بحذف جميع بيانات هذا الطبيب
         </strong>
        </div>
        <div class="modal-footer">

          <form class="form-horizontal" action = "deleteDoctor" method = "post">


            <input type="hidden" name="id" id="doctor_id">
            <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
            <button type="button" class="btn btn-blue" data-dismiss="modal" onclick="this.form.submit()">نعم</button>
            <button type="button" class="btn btn-blue" data-dismiss="modal">لا</button>
          </form>


        </div>
      </div>

    </div>
  </div>
  </section>

</div>
<div id="menu2" class="container tab-pane "><br>
  <section >
        <div class="container panel panel-default panel-body">
  <hr>
  <div class="row">
  <h2 class="col">الخدمات</h2>

  </div>
  <hr>

   @if(Session::has('flash_message'))
      <div class="alert alert-info">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
          {{ Session::get('flash_message') }}
      </div>
  @endif
  @if($errors->any())
  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
      <div class="alert alert-danger">
          @foreach($errors->all() as $error)
              <p>{{ $error }}</p>
          @endforeach
      </div>
  @endif

  <div class="responsive">
  <table class="table">
    <col width="100">
    <col width="200">
    <col width="200">
    <col width="150">
    <col width="100">
    <col width="100">

      <thead>
        <tr >

          <th>الخدمه</th>
          <th>نسبه المركز</th>
          <th>تعديل</th>
        </tr>
      </thead>
      <tbody>
        <tr>

          @foreach($services as $service)
                    @if($service->name=='checkUp')
                    <td>كشف</td>
                    @endif
                    @if($service->name=='normalExamination')
                    <td>فحص عادى</td>
                    @endif
                    @if($service->name=='lasikExamination')
                    <td>فحص ليزك</td>
                    @endif
                    @if($service->name=='normalOperation')
                    <td>عمليات كبرى وصغرى</td>
                    @endif
                    @if($service->name=='lasikOperation')
                    <td>عمليات ليزك</td>
                    @endif
                    @if($service->name=='repeat')
                    <td>اعادة كشف</td>
                    @endif
                    @if($service->id>6)
                      <td>{{$service->name}}</td>
                    @endif
                    <td>{{$service->commission}}</td>

                    <td><button type="button" class="btn btn-white" data-toggle="modal" data-target="#editServiceModal"  onclick = "edit_service('{{$service->id}}');" id="user_Edit" ><i class="fa fa-edit"></i></button></td>

                </tr>


          @endforeach

      </tbody>
    </table>

  </div>


  <div class="modal fade" id="editServiceModal" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title"> تعديل سعر الخدمه </h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>

        </div>
        <div class="modal-body">
            <form class="form-horizontal" enctype="multipart/form-data" action = "editService" method = "post">


              <div class="form-group row">
                  <label class="control-label col-sm-3" >الخدمه:</label>
              <div class="col-sm-9">
                <label   id="service_name" ></label>

              </div>
              </div>



              <div class="form-group row">
                <label class="control-label col-sm-3">السعر:</label>
                <div class="col-sm-9">
                  <input type="number" class="form-control" id="service_price" name = "commission" required >
                  <!-- <span id="olddoctor_phoneinvalid" style="color:red;"></span> -->
                </div>

              </div>




    <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
    <input type="hidden" name="id" id="editService_id">
  <div class="form-group">
    <div class=" row align-items-center justify-content-center">
      <button  id="save_update"  type="submit" onclick="this.form.submit()"  class="btn btn-white"data-dismiss="modal" >حفظ التغيرات</button>
    </div>
  </div>
        </form>
      </div>

    </div>
  </div>

  </div>

    </div>
      </section>
  </div>




</div>

</div>
<script>
$('.select_type').multiselect({
    enableHTML: true,
    templates: {
        li: '<li><a href="javascript:void(0);"><label class="pl-2 w-100"></label></a></li>',
        button: '<button type="button" class="multiselect w-100 text-left" data-toggle="dropdown">Select..</button>',
    },
    buttonContainer: '<div class="w-100" />',
    buttonClass: 'btn btn-outline-primary',
    selectedClass: 'bg-light',
    onInitialized: function(select, container) {
        // hide radio
        container.find('input[type=radio]').addClass('d-none');
    }
});
    function delete_user (id)
    {

      $("#member_id").val(id);
    }
    function delete_doctor (id)
    {

      $("#doctor_id").val(id);
    }
//     function edit_user(id)
// {
//           $("#editmember_id").val(id);
//           $.ajax({
//             url: "{{ URL::to('update') }}",
//             type: "get",
//             dataType: 'json',
//             data: {"_token":$('#_token').val(),"id":id},
//             success: function(response)
//             {
//                $("#oldmember_name").val(response.userName),
//                $("#oldmember_phone").val(response.phone),
//                $("#oldmember_NID").val(response.NID)
//
//
//                },
//
//
//             error: function () {
//
//                 alert("error");
//
//             }
//             });
//
// }
function edit_user(id)
{
      $("#editmember_id").val(id);
      $.ajax({
        url: "{{ URL::to('update') }}",
        type: "get",
        dataType: 'json',
        data: {"_token":$('#_token').val(),"id":id},
        success: function(response)
        {
           $("#oldmember_name").val(response.userName),
           $("#oldmember_phone").val(response.phone),
           $("#oldmember_NID").val(response.NID)
           // for(var i=0;i<response.roles.length;i++)
           // {
           //   if(response.roles[i].id==$("input[name=roles]").val())
           //   {alert("hi");
           //     // $("input:checked[name=roles]").attr("checked",true);
           //   }
           // }


           },


        error: function () {

            alert("error");

        }
        });

}
function edit_doctor(id)
{
      $("#editdoctor_id").val(id);
      $.ajax({
        url: "{{ URL::to('viewDoctor') }}",
        type: "post",
        dataType: 'json',
        data: {"_token":$('#_token').val(),"id":id},
        success: function(response)
        {
           $("#olddoctor_name").val(response.docName),
           $("#olddoctor_phone").val(response.phone),
           $("#olddoctor_NID").val(response.type)


           },


        error: function () {

            alert("error");

        }
        });

}
function edit_service(id)
{
      $("#editService_id").val(id);
      $.ajax({
        url: "{{ URL::to('editService') }}",
        type: "post",
        dataType: 'json',
        data: {"_token":$('#_token').val(),"id":id},
        success: function(response)
        {
          if(response.name == 'checkUp')
           $("#service_name").html("كشف")
            else if(response.name == 'normalExamination')
            $("#service_name").html("فحص عادي")
            else if(response.name == 'lasikExamination')
             {$("#service_name").html("فحص ليزك")}
            else if(response.name == 'normalOperation')
               $("#service_name").html("عمليات صغري وكبري")
            else if(response.name == 'lasikOperation')
             $("#service_name").html("عمليات ليزك")
             else if(response.name == 'repeat')
              $("#service_name").html("اعادة كشف")
             else {
               $("#service_name").html(response.name)
             }

           $("#service_price").val(response.commission)



           },


        error: function () {

            alert("error");

        }
        });

}
// function edit_user(id)
// {
//       $("#editmember_id").val(id);
//       $.ajax({
//         url: "{{ URL::to('update') }}",
//         type: "get",
//         dataType: 'json',
//         data: {"_token":$('#_token').val(),"id":id},
//         success: function(response)
//         {
//            $("#oldmember_name").val(response.userName),
//            $("#oldmember_phone").val(response.phone),
//            $("#oldmember_NID").val(response.NID)
//
//
//            },
//
//
//         error: function () {
//
//             alert("error");
//
//         }
//         });
//
// }


function edit()
{
  // alert($("#editmember_id").val());
  var arr = [];
$("input:checked[name=roles]").each(function(){
    arr.push($(this).val());
});

  $.ajax({
    url: "{{ URL::to('user') }}",
    type: "post",
    dataType: 'json',
    data: {"_token":$('#_token').val(),"userName":$('#oldmember_name').val(),"NID":$('#oldmember_NID').val(),"phone":$('#oldmember_phone').val(),"roles":arr,"id":$("#editmember_id").val()},
    success: function(response)
    {if(response['errors']){
      if(response['errors'].NID)
      {
        $("#oldmember_NIDinvalid").html(response['errors'].NID);
      }
      if(response['errors'].phone)
      {var html="";
        for(var i=0;i<response['errors'].phone.length;i++){
        html+=''+response["errors"].phone[i]+'<br>'
      }
        $("#oldmember_phoneinvalid").html(html);
      }
      if(response['errors'].roles)
      {
        $("#oldmember_rolesinvalid").html(response['errors'].roles);
      }

      if(response['errors'].userName)
      {
        $("#oldmember_nameinvalid").html(response['errors'].userName);
      }}
  else if(response.success)
  {location.reload();}

arr=[];
// location.reload();
       },


    error: function () {

        alert("error");

    }
    });

}
function adddoctor()
{
  var arr = [];
$("input:checked[name=type]").each(function(){
    arr.push($(this).val());
});

  $.ajax({
    url: "{{ URL::to('addDoctor') }}",
    type: "post",
    dataType: 'json',
    data: {"_token":$('#_token').val(),"docName":$('#doctor_name').val(),"phone":$('#doctor_phone').val(),"type":arr},
    success: function(response)
    {
      if(response['errors'])
      {


      if(response['errors'].phone)
      {var html="";
        for(var i=0;i<response['errors'].phone.length;i++){
        html+=''+response["errors"].phone[i]+'<br>'
      }
        $("#doctor_phoneinvalid").html(html);
      }
      if(response['errors'].type)
      {
        $("#doctor_typeinvalid").html(response['errors'].type);
      }
      if(response['errors'].docName)
      {
        $("#doctor_nameinvalid").html(response['errors'].docName);
      }

    }
      else if(response.success)
      {
        location.reload();
      }
    //  location.reload();




       },


    error: function() {


    }
    });

}

function editdoctor()
{
  var arr = [];
$("input:checked[name=type]").each(function(){
    arr.push($(this).val());
});

  $.ajax({
    url: "{{ URL::to('editDoctor') }}",
    type: "post",
    dataType: 'json',
    data: {"_token":$('#_token').val(),"docName":$('#olddoctor_name').val(),"phone":$('#olddoctor_phone').val(),"type":arr,"id":$("#editdoctor_id").val()},
    success: function(response)
    {
      if(response['errors'])
      {


      if(response['errors'].phone)
      {var html="";
        for(var i=0;i<response['errors'].phone.length;i++){
        html+=''+response["errors"].phone[i]+'<br>'
      }
        $("#olddoctor_phoneinvalid").html(html);
      }
      if(response['errors'].type)
      {
        $("#olddoctor_typeinvalid").html(response['errors'].type);
      }
      if(response['errors'].docName)
      {
        $("#olddoctor_nameinvalid").html(response['errors'].docName);
      }

    }
      else if(response.success)
      {
        location.reload();
      }
    //  location.reload();




       },


    error: function() {


    }
    });

}

function addmember()
{
  var arr = [];
$("input:checked[name=roles]").each(function(){
    arr.push($(this).val());
});

  $.ajax({
    url: "{{ URL::to('user') }}",
    type: "post",
    dataType: 'json',
    data: {"_token":$('#_token').val(),"userName":$('#member_name').val(),"password":$('#member_password').val(),"NID":$('#member_NID').val(),"phone":$('#member_phone').val(),"roles":arr},
    success: function(response)
    {
      if(response['errors'])
      {

      if(response['errors'].NID)
      {
        $("#member_NIDinvalid").html(response['errors'].NID);
      }
      if(response['errors'].phone)
      {var html="";
        for(var i=0;i<response['errors'].phone.length;i++){
        html+=''+response["errors"].phone[i]+'<br>'
      }
        $("#member_phoneinvalid").html(html);
      }
      if(response['errors'].roles)
      {
        $("#member_rolesinvalid").html(response['errors'].roles);
      }
      if(response['errors'].userName)
      {
        $("#member_nameinvalid").html(response['errors'].userName);
      }
      if(response['errors'].password)
      {
        $("#member_passinvalid").html(response['errors'].password);
      }}
      else if(response.success)
      {location.reload();}
    //  location.reload();




       },


    error: function() {


    }
    });

}
  </script>
@stop
