@extends("master")
@section("content")

<div class="container" style="margin-right:0;">
  <hr>
    <h2 class="col-xs-6 " style="text-align:center; color:#243fb2;"> المخازن  </h2>
    <hr>

  @if(Session::has('message'))

  <div class="alert alert-success alert-dismissible fade show" style="margin-top:1%;">
     <button type="button" class="close" data-dismiss="alert">&times;</button>
{{ Session::get('message') }}
</div>
@endif
@if(Session::has('error'))

<div class="alert alert-danger alert-dismissible fade show" style="margin-top:1%;">
   <button type="button" class="close" data-dismiss="alert">&times;</button>
{{ Session::get('error') }}
</div>
@endif
  <div class="row" style="margin-top:3%;">
    <div class="col-md-4">
  <div class="input-group-prepend">
    <select class="custom-select" id="select_store" name="select" onchange="store_data();">
      <option  selected disabled value="none">اختر المخزن</option>
      @foreach($stores as $store)
    <option  value="{{$store->id}}" >{{$store->name}}</option>

      @endforeach
    </select>
     <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
  </div>
</div>
<div class="col-md-4">
  <button class="btn btn-white" data-toggle="modal" data-target="#addstormodale"><i class="fa fa-plus"></i> اضافة مخزن</button>
</div>
<div class="col-md-4">
  <button class="btn btn-white" data-toggle="modal" data-target="#additemmodale"><i class="fa fa-plus"></i> اضافة عنصر</button>
</div>
</div>


                  <div class="modal fade" id="addstormodale" role="dialog" data-backdrop="static" >
                        <div class="modal-dialog formstyle ">
                          <!-- Modal content-->
                              <div class="modal-content formstyle">
                                  <div class="modal-header">
                                    <h4 style="margin:0 auto; color:#3250c9;">اضافة مخزن</h4>
                                    <button type="button" class="close " data-dismiss="modal">&times;</button>
                                  </div>
                                  <div class="modal-body">


                                      <form id="addstore" action="store" method="post">
                                        <table class="table  table-responsive">
                                          <tr>
                                            <td class="label">
                                              اسم المخزن :</td>
                                                <td><input  required  name="name"  type="text" >

                                          </td>
                                          </tr>
                                          <tr>
                                            <td class="label">
                                              وصف المخزن :</td>
                                            <td>    <textarea   name="description" ></textarea>

                                          </td>
                                          </tr>
                                          </table>
                                            <div class="row align-items-center justify-content-center">
                                               <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
                                               <input class="btn btn-white" value="اضافة" type="submit" >
                                            </div>



                                     </form>
                                </div>
                            </div>

                      </div>

            </div>
            <div class="modal fade" id="additemmodale" role="dialog" data-backdrop="static" >
                  <div class="modal-dialog formstyle ">
                    <!-- Modal content-->
                        <div class="modal-content formstyle">
                            <div class="modal-header">
                              <h4 style="margin:0 auto; color:#3250c9;">اضافة عنصر </h4>
                              <button type="button" class="close " data-dismiss="modal">&times;</button>
                            </div>
                            <div class="modal-body">


                                <form id="additem" action="store" method="post">
                                      <table class="table  table-responsive">
                                      <div class="form-group" >

                                    <div class="input-group-prepend">
                                      <tr hidden>
                                   <td class="label">اختر المخزن :</td>
                                    <td>  <select class="custom-select" id="select_store" >

                                        @foreach($stores as $store)
                                      <option  value="{{$store->id}}" >{{$store->name}}</option>
                                        @endforeach
                                      </select></td></tr>
                                    </div>
                                  </div>

                                  <tr>
                                  <td class="label">  اسم العنصر :</td>
                                  <td><input   required  name="name"  type="text"></td>
                                      </tr>

                                      <tr>
                                        <td class="label">    الكمية :</td>
                                      <td>    <input  required  name="quantity"  type="number" class="w"></td>
                                    </tr>
                                        <tr><td class="label">
                                             سعر الشراء :</td>
                                          <td>  <input  required  name="buyPrice"  type="number"  class="w"></td>
                                        </tr>
                                    <tr>
                                          <td class="label">سعر البيع :</td>
                                          <td><input  required  name="price"  type="number"  class="w"></td>
                                      </tr>
                                      <tr><td class="label">
                                        نقطة اعادة الطلب :</td>
                                    <td><input  required  name="limitQuantity"  type="number"  class="w"></td>
                                  </tr>
                                    <tr> <td class="label">
                                      الوصف :</td>
                                        <td>  <textarea    name="description" ></textarea></td>

                                      </tr>
                                       </table>

                                       <div class="row align-items-center justify-content-center">

                                         <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
                                         <input type="hidden" name="id" value="1">
                                           <input class="btn btn-white" value="اضافة" type="submit" >
                                      </div>


                               </form>
                          </div>
                      </div>

                </div>

      </div>

      <div class="modal fade" id="edititemmodale" role="dialog" data-backdrop="static" >
            <div class="modal-dialog formstyle ">
              <!-- Modal content-->
                  <div class="modal-content formstyle">
                      <div class="modal-header">
                        <h4 style="margin:0 auto; color:#3250c9;">تعديل عنصر</h4>
                        <button type="button" class="close " data-dismiss="modal">&times;</button>
                      </div>
                      <div class="modal-body">


                          <form id="edititem" action="update" method="post">
                                <table class="table  table-responsive">
                                <div class="form-group" >

                              <div class="input-group-prepend">
                                <tr hidden>
                             <td class="label">اختر المخزن :</td>
                              <td>  <select class="custom-select" id="select_store" >

                                  @foreach($stores as $store)
                                <option  value="{{$store->id}}" >{{$store->name}}</option>
                                  @endforeach
                                </select></td></tr>
                              </div>
                            </div>

                            <tr>
                            <td class="label">  اسم العنصر :</td>
                            <td><input   required  name="name" id="edit_name"  type="text"></td>
                                </tr>

                                <tr>
                                  <td class="label">    الكمية :</td>
                                <td>    <input  required  name="quantity" id="edit_qty" type="number" class="w"></td>
                              </tr>
                                  <tr><td class="label">
                                       سعر الشراء :</td>
                                    <td>  <input  required  name="buyPrice" id="edit_buyprice"  type="number"  class="w"></td>
                                  </tr>
                              <tr>
                                    <td class="label">سعر البيع :</td>
                                    <td><input  required id="edit_sellprice" name="price"  type="number"  class="w"></td>
                                </tr>
                                <tr><td class="label">
                                  نقطة اعادة الطلب :</td>
                              <td><input  required id="edit_limit" name="limitQuantity"  type="number"  class="w"></td>
                            </tr>
                              <tr> <td class="label">
                                الوصف :</td>
                                  <td>  <textarea id="edit_des"   name="description" ></textarea></td>

                                </tr>
                                 </table>

                                 <div class="row align-items-center justify-content-center">

                                   <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
                                   <input type="hidden" name="id" id="edititemid">
                                     <input class="btn btn-white" value="تعديل" type="submit" >
                                </div>


                         </form>
                    </div>
                </div>

          </div>

</div>


      <div class="modal fade" id="addstoritemmodale" role="dialog" data-backdrop="static" >
            <div class="modal-dialog formstyle ">
              <!-- Modal content-->
                  <div class="modal-content formstyle">
                      <div class="modal-header">
                        <h4>اضافة كمية</h4>
                        <button type="button" class="close " data-dismiss="modal">&times;</button>
                      </div>
                      <div class="modal-body">


                          <form id="additem" action="store" method="post">



                                <fieldset>
                                    <label>الكمية :</label>
                                    <input  required  name="quantity"  type="number">
                                </fieldset>

                                 <div class="row align-items-center justify-content-center">
                                   <input type="hidden" name="item_id" id="addid_item">

                                   <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
                                   <input class="btn btn-white" value="اضافة" type="submit" >
                                </div>


                         </form>
                    </div>
                </div>

          </div>

</div>


      <div class="modal fade" id="transfermodale" role="dialog" data-backdrop="static" >
            <div class="modal-dialog formstyle ">
              <!-- Modal content-->
                  <div class="modal-content formstyle">
                      <div class="modal-header">
                        <h4 style="margin:0 auto;">نقل العنصر</h4>
                        <button type="button" class="close " data-dismiss="modal">&times;</button>
                      </div>
                      <div class="modal-body">


                          <form id="transferitem" action="transform" method="post">

                            <table style="border-style:none;"><tr>

                              <fieldset>
                              <div class="input-group-prepend">
                                <label>اختر المخزن :</label>
                                <select class="custom-select" id="select_store" name="store_id">
                                  <option  selected value="none" disabled>اختر المخزن</option>
                                  @foreach($stores as $store)

                                <option  value="{{$store->id}}" >{{$store->name}}</option>

                                  @endforeach
                                </select></td>
                              </div>
                            </fieldset>
                          </tr>
<tr><td style="border-style:none;"></br></td></tr>

                          <tr>


                                    <td style="border-style:none;" >  <label class="labcol">الكمية :</label></td>
                                      <td style="border-style:none;" ><input  required  name="storeQuantity"  type="number">
                              </td>

                              </tr>
                            </table>
                          </br>
                                 <div class="row align-items-center justify-content-center">
                                   <input type="hidden" name="item_id" id="id_item">
                                   <input type="hidden" name="oldStore_id" id="oldstore">
                                   <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
                                   <input class="btn btn-white" value="نقل" type="submit" >
                                </div>


                         </form>
                    </div>
                </div>

          </div>

</div>
<div class="modal fade" id="deletemodale" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">هل انت متأكد ؟</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>


      </div>
      <div class="modal-body">
       <strong style="color:red;">
           سوف تقوم بحذف جميع بيانات هذا العنصر
       </strong>
      </div>
      <div class="modal-footer">

        <form class="form-horizontal" action = "delete" method = "post">


          <input type="hidden" name="id" id="id_deleteitem">
          <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
          <button type="button" class="btn btn-general btn-white" data-dismiss="modal" onclick="this.form.submit()">نعم</button>
          <button type="button" class="btn btn-danger" data-dismiss="modal">لا</button>
        </form>


      </div>
    </div>

  </div>
</div>

<div id="content">
  <hr>
  <h2 class="col-xs-6 " id="table_title">اسم المخزن</h2>
  <hr>

  <div class="responsive">
  <table class="table table-hover table-responsive">
    <col width="100">
    <col width="100">
    <col width="100">
    <col width="100">
    <col width="100">
    <col width="100">
    <col width="100">
    <col width="100">
    <col width="100">
    <col width="100">

      <thead>
        <tr>
          <th >الاسم</th>
          <th >الكمية</th>
          <th >الوصف</th>
          <th >سعر الشراء</th>
          <th >سعر البيع</th>
          <th >نقطة اعادة الطلب</th>
          <th >تعديل</th>
          <th >اضافة كمية</th>
          <th>نقل العنصر</th>
          <th>حذف العنصر</th>


        </tr>
      </thead>
      <tbody id="store_table">


      </tbody>
    </table>
  </div>
</div>
</div>
<script>
function store_data()
  {
    $("#oldstore").val($('#select_store :selected').val());
    var html="";
    $("#store_table").html("");
    $("#table_title").html("");
    $.ajax({
    url: "{{ URL::to('select') }}",
    type: "post",
    dataType: 'json',
    data: {"id":$('#select_store :selected').val(),"_token":$('#_token').val()},
    success: function(response)
    {
      $("#table_title").html(response.title);
      for(var i=0;i<response.items.length;i++)
      {
        if(response.id == 1)
        {
          edit='<td><button class="btn btn-white" data-toggle="modal" data-target="#edititemmodale" onClick="edit_item('+response.items[i].id+')"><i class="fa fa-edit"></i></button></td>';
          quantity = response.items[i].quantity;
          button3='<td><button class="btn btn-success" data-toggle="modal" data-target="#addstoritemmodale" onClick="transfer_item('+response.items[i].id+')"><i class="fa fa-plus"></i></button></td>';
          button='<td><button class="btn btn-white" data-toggle="modal" data-target="#transfermodale" onClick="transfer_item('+response.items[i].id+')"><i class="fa fa-share-square"></i></button></td>';
          button2='<td><button class="btn btn-danger" data-toggle="modal" data-target="#deletemodale" onClick="delete_item('+response.items[i].id+')"><i class="fa fa-trash"></i></button></td>';
          point= response.items[i].limitQuantity;

        }
        else
        {
            edit='<td style="color:#e13737;">غير مسموح</td>';
            quantity = response.items[i].storeQuantity;
            button3='<td style="color:#e13737;">غير مسموح</td>';
            button='<td><button class="btn btn-white" data-toggle="modal" data-target="#transfermodale" onClick="transfer_item('+response.items[i].id+')"><i class="fa fa-share-square"></i></button></td>';
            button2='<td style="color:#e13737;">غير مسموح</td>';
            point= "-";
        }
        if(quantity<=point)
        {
        html+='<div ><tr style="background: #ffe2e2; color: #cf3c3c;" ><td>'+response.items[i].name+'</td><td >'+quantity+'</td><td>'+response.items[i].description+'</td><td>'+response.items[i].buyPrice+'</td><td>'+response.items[i].price+'</td><td>'+point+'</td>'+edit+''+button3+''+button+''+button2+'</tr></div>'
        }
        else{
      html+='<div ><tr ><td>'+response.items[i].name+'</td><td >'+quantity+'</td><td>'+response.items[i].description+'</td><td>'+response.items[i].buyPrice+'</td><td>'+response.items[i].price+'</td><td>'+point+'</td>'+edit+''+button3+''+button+''+button2+'</tr></div>'
    }}
      $("#store_table").html(html);
    },
    error: function () {

        alert("error");

    }
  });

  }
  function edit_item(id)
  {
    $("#edititemid").val(id);
    $.ajax({
    url: "{{ URL::to('update') }}",
    type: "post",
    dataType: 'json',
    data: {"id":id,"_token":$('#_token').val()},
    success: function(response)
    {
      $("#edit_name").val(response.name);
      $("#edit_buyprice").val(response.buyPrice);
      $("#edit_sellprice").val(response.price);
      $("#edit_qty").val(response.quantity);
      $("#edit_limit").val(response.limitQuantity);
      $("#edit_des").val(response.description);
    },
    error: function () {

        alert("error");

    }
  });
  }
  function transfer_item(id)
  {
    $("#id_item").val(id);
    $("#addid_item").val(id);
  }
  function delete_item(id)
  {
    $("#id_deleteitem").val(id);
  }
</script>
@stop
