@extends("master")
@section("content")
<div class="container" style="margin-right:0;">
<hr>
  <h2 class="col-xs-6 " style="text-align:center; color:#243fb2;">  الخزنة  </h2>
  <hr>
  <form class="form-horizontal" enctype="multipart/form-data" action="billing" method = "get">

    @if(Session::has('error'))

    <div class="alert alert-danger alert-dismissible fade show" style="width: 70%; margin-top: 1%;">
       <button type="button" class="close" data-dismiss="alert">&times;</button>
    {{ Session::get('error') }}
    </div>
    @endif

  <div class="input-group mb-3">



  <div class="input-group-prepend">
    <select class="custom-select" name="select">
      <option  selected value="none">البحث عن طريق</option>
      <option  value="name">اسم المريض</option>
      <option  value="code">كود المريض</option>
      <option  value="NID">رقم البطاقة</option>
      <option  value="phone">رقم الموبايل</option>
    </select>
  </div>

  <input type="text" name="data" class="form-control" aria-label="Text input with dropdown button">
  <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">

  <button type="submit" class="btn btn-white">
    <i class="fa fa-search"></i>  بحث
  </button>

</div>

</form>
<ul class="nav nav-tabs" role="tablist" >

    <li class="nav-item">
      <a class="nav-link active" data-toggle="tab" href="#home">لم يتم الدفع</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" data-toggle="tab" href="#menu1">تم الدفع</a>
    </li>

  </ul>

  <div class="tab-content">
    <div id="home" class="container tab-pane active"><br>
      <table class="table  table-hover paing_table">
        <col width="350">
        <col width="400">
        <col width="350">
        <col width="400">
        <col width="350">
        <col width="350">

          <thead class="thead-light">
              <tr><th>الكود</th><th>الاسم</th><th>التاريخ والوقت</th><th>الخدمة</th><th>نوع الخدمه</th><th>السعر</th><th>المزيد</th></tr>
          </thead>
          <tbody>
            @foreach($services as $service)
                  <tr class="clickable" data-toggle="collapse" data-target="#{{$service->id}}" aria-expanded="false" aria-controls="group-of-rows-1">
                      <td>{{$service->patient->code}}</td>
                    	<td>{{$service->patient->name}}</td>
                      <td>{{$service->patient->created_at}}</td>
                      <td>@if($service->examinationType == "checkUp")
                        كشف
                      @elseif($service->examinationType == "normalExamination")
                        فحص عادي
                      <td>{{$service->examination->diagonistics}}</td>
                      @elseif($service->examinationType == "lasikExamination")
                        فحص ليزك
                        <td>{{$service->examination->investigation}}</td>
                      @elseif($service->examinationType == "normalOperation")
                        عمليات صغرى وكبرى
                        <td>{{$service->operation->operation}}</td>
                      @else
                        عمليات ليزك
                        <td>{{$service->operation->treatment}}</td>
                      @endif</td>

                      <td>{{$service->owedMoney}}</td>

                      <td><a  data-toggle="collapse" href="#{{$service->id}}" class="btn btn-white mb-0"  ><i class="fa fa-plus"></i></a></td>
                  </tr>

              </tbody>

              <tr colspan="12" >
              <tbody style="background:#F8F8F8;"  id="{{$service->id}}" class="collapse ">

    <tr>
       <td></td>
        <td class="label">رقم التليفون :</td>
        <td>{{$service->patient->phone}}</td>
        <td class="label">العنوان :</td>
        <td>{{$service->patient->address}}</td>

      </tr>
      <tr>
        <td></td>
        <td class="label">تاريخ الميلاد :</td>
        <td>{{$service->patient->DOB}}</td>
        <td class="label">عدد العيون :</td>
        <td>{{$service->eye}}</td>

    </tr>



    <form id="money_form" class="form-horizontal" enctype="multipart/form-data" action="billing" method = "post">
      <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
      <input type="hidden" name="id"  value="{{$service->id}}">
      <tr>
      <td></td>
      <td class="label">الطبيب المحول :</td>
      @if($service->transformerDoc=='none')
      <td>لم يتم ادخاله</td>
      <td class="label">نسبته من المبلغ المدفوع :</td>
      <td><input type="number" disabled step=any name="transformerDocMoney" id="transformer{{$service->id}}" value="0" class="doc_com_input"  >
      @else
      <td>{{$service->transformerDoc}}</td>
      <td class="label">نسبته من المبلغ المدفوع :</td>
      <td><input type="number"  step=any name="transformerDocMoney" id="transformer{{$service->id}}" value="0" class="doc_com_input"  >
      @endif
      </tr>
      @if($service->examinationType=='checkUp' || $service->examinationType=='normalExamination' ||$service->examinationType=='lasikExamination')</td>
     <tr>
      <td></td>
      <td class="label">طبيب الفحص :</td>
      <td>{{$service->examinerDoc}}</td>
      <td class="label">نسبته من المبلغ المدفوع :</td>
      <td><input type="number" step=any name="examinerDocMoney" id="examiner{{$service->id}}"  value="0" class="doc_com_input"></td>


     </tr>
      @endif
       @if($service->examinationType=='normalOperation')
       <tr>
      <td></td>
       <td class="label">الطبيب الجراح :</td>
       <td>{{$service->operation->surgeon}}</td>
       <td class="label">نسبته من المبلغ المدفوع :</td>
       <td><input type="number" step=any name="surgeonMoney" class="doc_com_input" id="surgeon{{$service->id}}" value="0" ></td>


      </tr>
      <tr>
      <td></td>
       <td class="label">طبيب التخدير :</td>
       <td>{{$service->operation->anesthetist}}</td>
       <td class="label">نسبته من المبلغ المدفوع :</td>
       <td><input type="number" step=any name="anesthetistMoney" class="doc_com_input" id="anesthetist{{$service->id}}"  value="0" ></td>


     </tr>
     <tr>
       <td></td>
       <td class="label">المساعد :</td>
       <td>{{$service->operation->assistantDoctor}}</td>
       <td class="label">نسبته من المبلغ المدفوع :</td>
      <td><input type="number" step=any name="assistantDoctorMoney" class="doc_com_input" id="assistant{{$service->id}}"  value="0" ></td>


     </tr>
       @endif
       @if($service->examinationType=='lasikOperation')
       <tr>
       <td></td>
       <td class="label">الطبيب الجراح :</td>
       <td>{{$service->operation->surgeon}}</td>
       <td class="label">نسبته من المبلغ المدفوع :</td>
       <td><input type="number" step=any name="surgeonMoney" class="doc_com_input" id="surgeon2{{$service->id}}"  value="0" ></td>


      </tr>

      <tr>
      <td></td>
      <td class="label">دكتور داتا :</td>
      <td>{{$service->operation->dataDoctor}}</td>
      <td class="label">نسبته من المبلغ المدفوع :</td>
      <td><input type="number" step=any name="dataDoctorMoney" class="doc_com_input" id="data{{$service->id}}"  value="0" ></td>


     </tr>
       @endif
       <tr>
       <td></td>
       <td></td>
       <td></td>
       <td class="label">نسبة المركز :</td>
<td><input type="number" step=any name="commission" disabled class="doc_com_input" id="commission{{$service->id}}"  value="{{$service->commission}}" ></td>

      </tr>
      <tr>
        <td></td>
        <td colspan="3"><span id="{{$service->id}}{{$service->patient->id}}" style="color:#e73333; font-weight:bold;"></span></td>

        <td colspan="1"></td>
      </tr>
      <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
      <td><button class="btn btn-white" onclick="return checkcommission({{$service->owedMoney}},{{$service->id}}{{$service->patient->id}},{{$service->id}});">تأكيد الدفع</button></td>

      </tr>
</form>

</tbody>

</tr>
@endforeach
</table>

    </div>
    <div id="menu1" class="tab-pane fade">



      <table class="table  table-hover paing_table">
        <col width="350">
        <col width="400">
        <col width="350">
        <col width="400">
        <col width="350">
        <col width="500">
        <col width="350">
          <thead class="thead-light">
              <tr><th>الكود</th><th>الاسم</th><th>التاريخ والوقت</th><th>الخدمة</th><th>السعر</th><th>معلومات</th><th>المزيد</th></tr>
          </thead>
          <tbody>
            @foreach($services_confirmed as $service)
                  <tr class="clickable" data-toggle="collapse" data-target="#{{$service->id}}" aria-expanded="false" aria-controls="group-of-rows-1">
                      <td>{{$service->patient->code}}</td>
                      <td>{{$service->patient->name}}</td>
                      <td>{{$service->patient->created_at}}</td>
                      <td>@if($service->examinationType == "checkUp")
                        كشف
                      @elseif($service->examinationType == "normalExamination")
                        فحص عادي
                      @elseif($service->examinationType == "lasikExamination")
                        فحص ليزك
                      @elseif($service->examinationType == "normalOperation")
                        عمليات صغرى وكبرى
                      @else
                        عمليات ليزك
                      @endif</td>
                      <td>{{$service->owedMoney}}</td>
                       <td>
                         @if($service->operation)
                          @if($service->serviceStatus == 'rejected' && $service->operation->refund == 1)
                          <h5 class="mb-0" style="color:red;">تم رفض العملية وارجاع المبلغ المدفوع للمريض</h5>

                          @elseif($service->serviceStatus == 'rejected' && $service->operation->refund == 0)
                          <h5 class="mb-0" style="color:red;">تم رفض  اجراء العملية</h5>
                          @else
                          <h5>لايوجد</h5>
                          @endif
                          @endif
                       @if($service->examination)
                          @if($service->serviceStatus == 'rejected' && $service->examination->refund == 1)
                          <h5 class="mb-0" style="color:red;">تم رفض الفحص وارجاع المبلغ المدفوع للمريض</h5>

                          @elseif($service->serviceStatus == 'rejected' && $service->examination->refund == 0)
                          <h5 class="mb-0" style="color:red;">تم رفض  اجراء الفحص</h5>
                          @else
                          <h5>لايوجد</h5>
                          @endif
                          @endif
                       </td>
                      <td><a  data-toggle="collapse" href="#{{$service->id}}" class="btn btn-white mb-0"  ><i class="fa fa-plus"></i></a></td>
                  </tr>

              </tbody>

              <tr colspan="12" >
              <tbody style="background:#F8F8F8;"  id="{{$service->id}}" class="collapse ">

    <tr>
       <td></td>
        <td class="label">رقم التليفون :</td>
        <td>{{$service->patient->phone}}</td>
        <td class="label">العنوان :</td>
        <td>{{$service->patient->address}}</td>
        <td></td>
      </tr>
      <tr>
        <td></td>
        <td class="label">تاريخ الميلاد :</td>
        <td>{{$service->patient->DOB}}</td>
        <td class="label">عدد العيون :</td>
        <td>{{$service->eye}}</td>
        <td></td>
    </tr>




      <tr>
      <td></td>
      <td class="label">الطبيب المحول :</td>
      @if($service->transformerDoc=='none')
      <td>لم يتم ادخاله</td>
      <td class="label">نسبته من المبلغ المدفوع :</td>
      <td>{{$service->transformerDocMoney}}</td>
      @else
      <td>{{$service->transformerDoc}}</td>
      <td class="label">نسبته من المبلغ المدفوع :</td>
      <td>{{$service->transformerDocMoney}}</td>
      @endif
      <td></td>
      </tr>
      @if($service->examinationType=='checkUp' || $service->examinationType=='normalExamination' ||$service->examinationType=='lasikExamination')</td>
     <tr>
      <td></td>
      <td class="label">طبيب الفحص :</td>
      <td>{{$service->examinerDoc}}</td>
      <td class="label">نسبته من المبلغ المدفوع :</td>
      <td>{{$service->examinerDocMoney}}</td>

      <td></td>


     </tr>
      @endif
       @if($service->examinationType=='normalOperation')
       <tr>
      <td></td>
       <td class="label">الطبيب الجراح :</td>
       <td>{{$service->operation->surgeon}}</td>
       <td class="label">نسبته من المبلغ المدفوع :</td>
       <td>{{$service->surgeonMoney}}</td>

       <td></td>
      </tr>
      <tr>
      <td></td>
       <td class="label">طبيب التخدير :</td>
       <td>{{$service->operation->anesthetist}}</td>
       <td class="label">نسبته من المبلغ المدفوع :</td>
       <td>{{$service->anesthetistMoney}}</td>

       <td></td>
     </tr>
     <tr>
       <td></td>
       <td class="label">المساعد :</td>
       <td>{{$service->operation->assistantDoctor}}</td>
       <td class="label">نسبته من المبلغ المدفوع :</td>
      <td>{{$service->assistantDoctorMoney}}</td>

       <td></td>
     </tr>
       @endif
       @if($service->examinationType=='lasikOperation')
       <tr>
       <td></td>
       <td class="label">الطبيب الجراح :</td>
       <td>{{$service->operation->surgeon}}</td>
       <td class="label">نسبته من المبلغ المدفوع :</td>
       <td>{{$service->surgeonMoney}}</td>

       <td></td>

      </tr>
      <tr>
      <td></td>
      <td class="label">دكتور داتا :</td>
      <td>{{$service->operation->dataDoctor}}</td>
      <td class="label">نسبته من المبلغ المدفوع :</td>
       <td>{{$service->dataDoctorMoney}}</td>
       <td></td>


     </tr>
       @endif
       <tr>
       <td></td>
       <td></td>
       <td></td>
       <td class="label">نسبة المركز :</td>
       <td>{{$service->commission}}</td>

       <td></td>
      </tr>
@if($service->operation&&$service->serviceStatus == 'rejected' && $service->operation->refund == 0)
      <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
      <td><form id="money_form" class="form-horizontal" enctype="multipart/form-data" action="billing" method = "post">
        <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
        <input type="hidden" name="operationId"  value="{{$service->operation->id}}">
        <input type="hidden" name="refund"  value="1">
       <button type="submit" class="btn btn-white">ارجاع المبلغ للمريض</button>
     </form></td>
      <td></td>
      </tr>
      @endif

      @if($service->examination&&$service->serviceStatus == 'rejected' && $service->examination->refund == 0)
            <tr>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
            <td><form id="money_form" class="form-horizontal" enctype="multipart/form-data" action="billing" method = "post">
              <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
              <input type="hidden" name="examinationId"  value="{{$service->examination->id}}">
              <input type="hidden" name="refund"  value="1">
             <button type="submit" class="btn btn-white">ارجاع المبلغ للمريض</button>
           </form></td>
            <td></td>
            </tr>
            @endif

      @if(($service->examinationType == "lasikOperation" || $service->examinationType == "normalOperation")&& $service->serviceStatus != 'rejected' )
      @if($service->operation->loss == 0)
      <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
      <td><button class="btn btn-white" data-toggle="modal" onclick="get_id({{$service->id}})" data-target="#salah">اضافة مستهلكات</button></td>
      <td></td>
      </tr>
      @else
      <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
      <td><button class="btn btn-white" data-toggle="modal" onclick="view_items({{$service->id}})" data-target="#items">عرض مستهلكات العملية</button></td>
      <td></td>
      </tr>
@endif
@endif

    </tbody>

    </tr>
    @endforeach
    </table>


  <div class="modal fade" id="salah" role="dialog"  >
        <div class="modal-dialog" style="width: auto; max-width: 960px;" >
          <!-- Modal content-->
              <div class="modal-content formstyle" >
                  <div class="modal-header">
                    <h4>اضافة المستهلكات</h4>
                    <button type="button" class="close " data-dismiss="modal">&times;</button>
                  </div>
                  <div class="modal-body">
                    <div id="table">
                      <div class="row align-items-center justify-content-center" style="margin-bottom:2%;" id="error">

                      </div>

                                <div class="row align-items-center justify-content-center" style="margin-bottom:2%;">
                                  <div class="clo-md-4">
                                <label>اختر المخزن :</label>
                              </div>
                              <div class="col-md-4">
                                <div class="input-group-prepend">
                                  <select class="custom-select" id="select_store" name="select" onchange="store_data();">
                                  <option disabled selected value ="none" id="empty"></option>
                                    @foreach($stores as $store)
                                  <option  value="{{$store->id}}">{{$store->name}}</option>
                                    @endforeach
                                  </select>
                                   <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
                                </div>
                              </div>
                            </div>




                              <table class="table table-striped">


                                  <thead>
                                    <tr>
                                      <th >الاسم</th>
                                      <th >الكمية المتاحة</th>
                                      <th >الكمية المستخدمة</th>
                                      <th >الوحدة</th>
                                      <th >السعر</th>


                                    </tr>
                                  </thead>
                                  <tbody id="store_table">
                                  </tbody>
                                </table>
                              </div>


                             <div class="row align-items-center justify-content-center" id="add">


                               <button class="btn btn-white" onclick="send_items();" >اضافة</button>
                            </div>

                </div>
            </div>

      </div>

</div>




<div class="modal fade" id="items" role="dialog"  >
      <div class="modal-dialog" style="width: auto; max-width: 960px;" >
        <!-- Modal content-->
            <div class="modal-content formstyle" >
                <div class="modal-header">
                  <h4>عرض المستهلكات</h4>
                  <button type="button" class="close " data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                  <div id="table">
                    <div class="row align-items-center justify-content-center" style="margin-bottom:2%;" id="error">

                    </div>






                            <table class="table table-striped">


                                <thead>
                                  <tr>
                                    <th >الاسم</th>
                                    <th >الكمية المستخدمة</th>
                                    <th >السعر</th>


                                  </tr>
                                </thead>
                                <tbody id="view_table">
                                </tbody>
                              </table>
                            </div>


                           <!-- <div class="row align-items-center justify-content-center" id="add">


                             <button class="btn btn-white"  >الغاء</button>
                          </div> -->

              </div>
          </div>

    </div>

</div>

</div>




<script>
var items=[];
var items_sum=0;
var html="";
var qty=[];

function view_items(id)
{var data="";
  $.ajax({
    url: "{{ URL::to('showItems') }}",
    type: "get",
    dataType: 'json',
    data: {"id":id},
    success: function(response)
    {
    //   alert(response.items);
    // alert(response);
    console.log(response.items);
    // console.log(response.items.length);
    // console.log(response.items[0].id);
    // console.log(response.items[0].quantity);
      for(var i=0;i<response.items.length;i++)
      {
        console.log(response.items[i].name);
      html+='<tr><td>'+response.items[i].name+'</td><td>'+response.items[i].quantity+'</td><td>'+response.items[i].price+'</td></tr>';
}
     html+='<tr><td>اجمالي المبلغ</td><td >'+response.total+'</td></tr>';
      $("#view_table").html(html);
      data="";

       },


    error: function () {

        alert("error");

    }
    });
}

function store_data()
{

  $("#store_table").html("");
  $.ajax({
  url: "{{ URL::to('select') }}",
  type: "post",
  dataType: 'json',
  data: {"id":$('#select_store :selected').val(),"_token":$('#_token').val()},
  success: function(response)
  {
    for(var i=0;i<items.length;i++)
    {
      $("#"+items[i].id).val(0);
      $("#qty"+items[i].id).val(0);
    }
    qty=[];
    items=[];
    html="";
   $("#store_table").html(html);
    for(var i=0;i<response.items.length;i++)
    {

    html+='<tr><td>'+response.items[i].name+'</td><td>'+response.items[i].storeQuantity+'</td><td><input value="0" type="number" max='+response.items[i].storeQuantity+' id="qty'+response.items[i].id+
    '"></td><td><input  id="id'+response.items[i].id+'" type="number" value="0"></td><td>'+response.items[i].price+'</td></tr>';

    items.push(response.items[i]);

  }

   html+='<tr><td><button  onClick="summ();" class="btn btn-white"> احسب اجمالي المبلغ</button></td><td ><input value="0" disabled id="sum" class="text-center"></td><td>خصم اجمالي المبلغ من :</td><td><select class="custom-select" name="discount" id="doctor_paying"><option selected disabled value="none"></option><option value="الطبيب الجراح">طبيب جراح</option><option value=" المساعد">مساعد</option><option value="الطبيب المحول">طبيب محول</option></select></td></tr>';
    $("#store_table").html(html);
  //   for(var y=0;y<items.length;y++)
  //   {alert($("#id"+items[y].id).val());
  // }
  },
  error: function () {

      // alert("error");

  }
});

}
function summ()
{ var num;
  var price;
  for(var y=0;y<items.length;y++)
  {
    num=$("#id"+items[y].id).val();
    price=items[y].price;
    items_sum+=num*price;

    // alert(items_sum);
  }

  $("#sum").val(items_sum);
  items_sum=0;

  // alert(items_sum);
}
var service_id=0;
function get_id(id)
{
  $("#error").html("");
  $("select").each(function() { this.selectedIndex = 0 });
  service_id=id;
}
function send_items()
{var id=service_id;
  service_id=0;
  for(var x=0;x<items.length;x++)
  {
    // alert("items_qantity: "+$("#qty"+items[x].id).val());
    qty[x]={
      id:items[x].id,
      quantity:$("#qty"+items[x].id).val()
    };

  }
  // for(var i=0;i<qty.length;i++)
  // {
  //   alert("qty_quantity : "+qty[i].quantity);
  // }
  $.ajax({
  url: "check",
  type: "get",
  dataType: 'json',
  data: {"id":id,"quantity":qty,"total":$("#sum").val(),"store_id":$('#select_store :selected').val(),"discount":$('#doctor_paying :selected').val()},
  success: function(response)
  {
var content="";
if(response.message){
content+='<span style="color:blue;">'+response.message+'</span>';
$("#error").html(content);
}
else{
    for(var e=0;e<response.errors.length;e++)
    {
      content+='<span style="color:red;">'+response.errors[e]+'</span><br>';
    }
    $("#error").html(content);
  }
    content="";

   //   html="";
   //  $("#add").hide();
   //  html+='<div class="align-items-center justify-content-center row"><h4 class="text-center">'+response+'</h4></div>';
   //  $("#table").html(html);
   $( "#salah" ).scrollTop( 50 );
   setTimeout(function() {$('#salah').modal('hide');}, 1500);
   location.reload();
   html="";
   $("#store_table").html(html);

   // $('#salah').modal('hide');
  },
  error: function () {

      alert("error");

  }
  });
  for(var i=0;i<items.length;i++)
  {
    $("#id"+items[i].id).val(0);
    $("#qty"+items[i].id).val(0);
  }
  qty=[];
  items=[];

}

  var sum=0;
function checkcommission(totalmoney,id,serviceid)
{


  var examinerDocMoney=parseFloat($("#examiner"+serviceid).val());
  var transformerDocMoney=parseFloat($("#transformer"+serviceid).val());
  var surgeonMoney=parseFloat($("#surgeon"+serviceid).val());
  var anesthetistMoney=parseFloat($("#anesthetist"+serviceid).val());
  var assistantDoctorMoney=parseFloat($("#assistant"+serviceid).val());
  var commission=parseFloat($("#commission"+serviceid).val());
  var surgeonMoney2=parseFloat($("#surgeon2"+serviceid).val());
  var dataMoney=parseFloat($("#data"+serviceid).val());

  if(examinerDocMoney)
  {
     sum += examinerDocMoney;
  }
  if(transformerDocMoney)
  {
     sum += transformerDocMoney;
  }
  if(surgeonMoney)
  {
     sum += surgeonMoney;
  }
  if(anesthetistMoney)
  {
     sum += anesthetistMoney;
  }
  if(assistantDoctorMoney)
  {
     sum += assistantDoctorMoney;
  }
  if(dataMoney)
  {
     sum += dataMoney;
  }
  if(commission)
  {
     sum += commission;
  }
  if(surgeonMoney2)
  {
     sum += surgeonMoney2;
  }

  if(totalmoney==sum)
{
  return true;
}
else {
$("#"+id).html("النسب غير مساوية للمبلغ المراد دفعه");
return false;
}
}
</script>
@stop
