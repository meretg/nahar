@extends("master")
@section("content")

<div class="container" style="margin-right:0;">
  <hr>
    <h2 class="col-xs-6 " style="text-align:center; color:#243fb2;">قسم التقارير </h2>
    <hr>
    <form class="form-horizontal" enctype="multipart/form-data" action="reports" method = "get">
      <div class="row">
        <!-- <div class="col-md-1">
          </div> -->
          <label style="padding-right:2%;  color:#3250C9;">من:</label>

        <div class="col-md-3">
      <input class="form-control" required type="date" format="dd/MM/yyyy" name="from" id="from_date">
      <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
    </div>
    <!-- <div class="col-md-1" >
        </div> -->
  <label style="padding-right:3%;  color:#3250C9;">الي:</label>

    <div class="col-md-3">

      <input class="form-control" required type="date" format="dd/MM/yyyy" name="to" id="to_date">
      <input hidden value="1" name="filter">
    </div>
    <div class="col-md-1">
    <button type="submit" class="btn btn-white">
      <i class="fa fa-filter"></i> تصفية
    </button>
  </div>
  </div>
    </form>
<ul class="nav nav-tabs" role="tablist" >
    <li class="nav-item">
      <a class="nav-link active" data-toggle="tab" href="#home">الفحوصات</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" data-toggle="tab" href="#menu1">العمليات</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" data-toggle="tab" href="#menu2">الكشوف</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" data-toggle="tab" href="#menu3">الخزنة</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" data-toggle="tab" href="#menu4">حساب الاطباء</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" data-toggle="tab" href="#menu5">المستهلكات</a>
    </li>
  </ul>

    <div class="tab-content">
  <div id="home" class="container tab-pane  active"><br>
    <div class="row">
      <!-- <h4 class="col"></h4> -->
    <button class=" col-xs-6 btn btn-success " id="btnExport" style="margin-bottom:2%;"   onclick="tableToExcel('examinations', 'W3C Example Table')"><i class="fa fa-download"></i> تحميل كملف اكسل</button>
  </div>
    <table class="table  table-hover " id="examinations">
      <!-- <col width="100">
      <col width="100">
      <col width="100"> -->
        <thead >
            <tr><th>نوع الخدمة</th><th>العدد</th><th>عدد العيون</th></tr>
        </thead>
        <tbody>
                <tr>
                  <td>فحص ليزك</td>
                    <td>{{$examinations->lasik}}</td>
                    <td>{{$examinations->lasikEye}}</td>
                  </tr>
                  <tr>
                      <td>فحص عادى</td>

                    <td>{{$examinations->normal}}</td>
                    <td>{{$examinations->normalEye}}</td>


                </tr>


            </tbody>

            </table>

  </div>

  <div id="menu1" class="container tab-pane "><br>
    <div class="row">
      <!-- <h4 class="col"></h4> -->
    <button class=" col-xs-6 btn btn-success " id="btnExport2" style="margin-bottom:2%;"   onclick="tableToExcel('operations', 'W3C Example Table')"><i class="fa fa-download"></i> تحميل كملف اكسل</button>
  </div>
    <table class="table  table-hover " id="operations">
      <!-- <col width="100">
      <col width="100">
      <col width="100"> -->


        <thead class="thead-light">
            <tr><th>نوع الخدمة</th><th>العدد</th><th>عدد العيون</th></tr>
        </thead>
        <tbody>
                <tr >
                  <td>عمليات ليزك</td>
                    <td>{{$operations->lasik}}</td>
                      <td>{{$operations->lasikEye}}</td>
                  </tr>
                  <tr>
                      <td>عمليات عاديه</td>

                    <td>{{$operations->normal}}</td>
                    <td>{{$operations->normalEye}}</td>


                </tr>


            </tbody>

            </table>

  </div>
  <div id="menu2" class="container tab-pane "><br>
    <div class="row">
      <!-- <h4 class="col"></h4> -->
    <button class=" col-xs-6 btn btn-success " id="btnExport2" style="margin-bottom:2%;"   onclick="tableToExcel('normalexamination', 'W3C Example Table')"><i class="fa fa-download"></i> تحميل كملف اكسل</button>
  </div>
    <table class="table  table-hover " id="normalexamination">
      <!-- <col width="100">
      <col width="100">
      <col width="100"> -->



        <thead class="thead-light">
            <tr><th>نوع الخدمة</th><th>العدد</th><th>عدد العيون</th></tr>
        </thead>
        <tbody>
                <tr >
                  <td>كشف</td>
                    <td>{{$checkups->checkup}}</td>
                    <td>{{$checkups->Eye}}</td>
                  </tr>


            </tbody>

            </table>

  </div>
  <div id="menu3" class="container tab-pane "><br>
    <div class="row">
      <!-- <h4 class="col"></h4> -->
    <button class=" col-xs-6 btn btn-success " id="btnExport2" style="margin-bottom:2%;"   onclick="tableToExcel('money', 'W3C Example Table')"><i class="fa fa-download"></i> تحميل كملف اكسل</button>
  </div>
    <table class="table  table-hover " id="money">
      <!-- <col width="100">
      <col width="100">
      <col width="100">
      <col width="100"> -->



        <thead >
            <tr><th>نوع الخدمة</th><th>العدد</th><th>عدد العيون</th><th>المدفوع</th></tr>
        </thead>
        <tbody>
                <tr class="clickable" data-toggle="collapse" data-target="#{{$operations->lasik}}" aria-expanded="false" aria-controls="group-of-rows-1">
                  <td>كشف</td>
                    <td>{{$checkups->checkup}}</td>
                       <td>{{$checkups->Eye}}</td>
                      <td>{{$billings->checkup}}</td>
                  </tr>
                  <tr>
                    <td>فحص ليزك</td>
                      <td>{{$examinations->lasik}}</td>
                      <td>{{$examinations->lasikEye}}</td>
                        <td>{{$billings->lasikExamination}}</td>
                  </tr>
                  <tr>
                    <td>فحص عادى</td>
                      <td>{{$examinations->normal}}</td>
                      <td>{{$examinations->normalEye}}</td>
                        <td>{{$billings->normalExamination}}</td>
                  </tr>
                  <tr>
                    <td>عمليات ليزك</td>
                      <td>{{$operations->lasik}}</td>
                      <td>{{$operations->lasikEye}}</td>
                        <td>{{$billings->lasikOperation}}</td>
                  </tr>
                  <tr>
                    <td>عمليات عاديه</td>
                      <td>{{$operations->normal}}</td>
                      <td>{{$operations->normalEye}}</td>
                        <td>{{$billings->normalOperation}}</td>
                  </tr>



            </tbody>

            </table>

  </div>

  <div id="menu4" class="container tab-pane "><br>
    <div class="row">
      <div class="col-md-10">
    <div class="input-group-prepend">
      <select class="custom-select" id="doctor_type" name="select" onchange="select_doctortype();">
        <option  selected disabled value="none">اختر التخصص</option>

      <option  value="1" >طبيب جراح</option>
      <option  value="2" >طبيب فحص</option>
      <option  value="3" >طبيب تخدير</option>
      <option  value="4" >مساعد</option>
      <option  value="5" >دكتور داتا</option>


      </select>
       <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
    </div>
  </div>
    <button class=" col-xs-2 btn btn-success " id="btnExport4" style="margin-bottom:2%;"   onclick="tableToExcel('Doctors', 'W3C Example Table')"><i class="fa fa-download"></i> تحميل كملف اكسل</button>
  </div>
    <table class="table  table-hover " id="Doctors">
      <!-- <col width="100">
      <col width="100">
      <col width="100"> -->



        <thead class="thead-light">
            <tr><th>اسم الطبيب</th><th>حساب قبل المستهلكات</th><th>المستهلكات</th><th>حساب بعد المستهلكات</th></tr>
        </thead>
        <tbody id="doctors_body">
                  <tr>
                  @foreach($doctors as $doctor)

                            <td>{{$doctor->docName}} <br/></td>
                            <td>
                              {{$doctor->money}}
                            </td>
                            <td> {{$doctor->loss}}</td>
                            <td>{{$doctor->afterLoss}}</td>

                        </tr>

                  @endforeach

            </tbody>

            </table>

  </div>
  <div id="menu5" class="container tab-pane "><br>
    <div class="row">
      <div class="col-md-10">
    <div class="input-group-prepend">
      <select class="custom-select" id="stors_selector" name="storeId" onchange="select_store();">
        <option  selected disabled value="none">اختر المخزن</option>
        @foreach($stores as $store)
      <option  value="{{$store->id}}" >{{$store->name}}</option>

        @endforeach


      </select>
       <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
    </div>
  </div>
    <button class=" col-xs-2 btn btn-success " id="btnExport5" style="margin-bottom:2%;"   onclick="tableToExcel('stors', 'W3C Example Table')"><i class="fa fa-download"></i> تحميل كملف اكسل</button>
  </div>
    <table class="table  table-hover " id="stors">
      <!-- <col width="100">
      <col width="100">
      <col width="100"> -->



        <thead class="thead-light">
            <tr><th>الاسم</th><th>الكمية المستهلكة</th><th>الكمية المتبقية ف المخزن</th></tr>
        </thead>
        <tbody id="stors_body">
                <tr >
                  @foreach($items as $item)
                  <td>{{$item->name}}</td>
                    <td></td>
                    <td></td>
                  </tr>
                  @endforeach


            </tbody>

            </table>

  </div>
</div>
</div>
<script>
var tableToExcel = (function() {
        var uri = 'data:application/vnd.ms-excel;base64,'
          , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><meta http-equiv="content-type" content="application/vnd.ms-excel; charset=UTF-8"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
          , base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))) }
          , format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) }
        return function(table, name) {
          if (!table.nodeType) table = document.getElementById(table)
          var ctx = {worksheet: name || 'Worksheet', table: table.innerHTML}
          window.location.href = uri + base64(format(template, ctx))
        }
      })()
      function select_doctortype()
        {
          var html="";
          $("#doctors_body").html("");
          $.ajax({
          url: "{{ URL::to('reports') }}",
          type: "get",
          dataType: 'json',
          data: {"from":$('#from_date').val(),"to":$('#to_date').val(),"select":$('#doctor_type :selected').val(),"_token":$('#_token').val()},
          success: function(response)
          {

            for(var i=0;i<response.length;i++)
            {
            html+='<div ><tr ><td>'+response[i].docName+'</td><td >'+response[i].money+'</td><td>'+response[i].loss+'</td><td>'+response[i].afterLoss+'</td></tr></div>'
          }
            $("#doctors_body").html(html);
          },
          error: function () {

              alert("error");

          }
        });

        }
        function select_store()
          {
            var html="";
            $("#stors_body").html("");
            $.ajax({
            url: "{{ URL::to('reports') }}",
            type: "get",
            dataType: 'json',
            data: {"storeId":$('#stors_selector :selected').val(),"_token":$('#_token').val()},
            success: function(response)
            {

              for(var i=0;i<response.length;i++)
              {
              html+='<div ><tr ><td>'+response[i].name+'</td><td >'+response[i].storeQuantityLoss+'</td><td>'+response[i].storeQuantity+'</td></tr></div>'
            }
              $("#stors_body").html(html);
            },
            error: function () {

                alert("error");

            }
          });

          }
</script>
@stop
