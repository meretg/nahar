@extends("master")
@section("content")
<div class="container" style="margin-right:0;">
  <hr>
    <h2 class="col-xs-6 " style="text-align:center; color:#243fb2;">  قسم العمليات  </h2>
    <hr>
<ul class="nav nav-tabs" role="tablist" >
    <li class="nav-item">
      <a class="nav-link active" data-toggle="tab" href="#home">لم يتم اجراء العملية</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" data-toggle="tab" href="#menu1">تم اجراء العملية</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" data-toggle="tab" href="#menu2">تم رفض العملية</a>
    </li>

  </ul>
  <div class="tab-content">
    <div id="home" class="container tab-pane active"><br>

      <table class="table  table-hover paing_table">
        <col width="350">
        <col width="400">
        <col width="350">
        <col width="400">
        <col width="350">


          <thead class="thead-light">
              <tr><th>الكود</th><th>الاسم</th><th>التاريخ والوقت</th><th>الخدمة</th><th>المزيد</th></tr>
          </thead>
          <tbody>
            @foreach($services as $service)
                  <tr class="clickable" data-toggle="collapse" data-target="#{{$service->id}}" aria-expanded="false" aria-controls="group-of-rows-1">
                      <td>{{$service->patient->code}}</td>
                      <td>{{$service->patient->name}}</td>
                      <td>{{$service->created_at}}</td>
                      <td>@if($service->examinationType == "checkUp")
                        كشف
                      @elseif($service->examinationType == "normalExamination")
                        فحص عادي
                      @elseif($service->examinationType == "lasikExamination")
                        فحص ليزك
                      @elseif($service->examinationType == "normalOperation")
                        عمليات صغرى وكبرى
                      @else
                        عمليات ليزك
                      @endif</td>


                      <td><a  data-toggle="collapse" href="#{{$service->id}}" class="btn btn-white mb-0"  ><i class="fa fa-plus"></i></a></td>
                  </tr>

              </tbody>
              <tr colspan="12" >
              <tbody style="background:#F8F8F8;"  id="{{$service->id}}" class="collapse ">
                <tr>
                  <td></td>
                  <td class="label">تاريخ الميلاد :</td>
                  <td>{{$service->patient->DOB}}</td>
                  <td class="label">عدد العيون :</td>
                  <td>{{$service->eye}}</td>

              </tr>
              <tr>
              <td></td>
              <td class="label">الطبيب المحول :</td>
              <td>{{$service->transformerDoc}}</td>
              <td></td>
              <td></td>
              </tr>
              @if($service->examinationType=='normalOperation')
              <tr>
             <td></td>
              <td class="label">الطبيب الجراح :</td>
              <td>{{$service->operation->surgeon}}</td>
              <td class="label">طبيب التخدير :</td>
              <td>{{$service->operation->anesthetist}}</td>


             </tr>

            <tr>
              <td></td>
              <td class="label">المساعد :</td>
              <td>{{$service->operation->assistantDoctor}}</td>
              <td class="label">البنج :</td>
              <td>{{$service->operation->anesthesia}}</td>


            </tr>
              @endif
              @if($service->examinationType=='lasikOperation')
              <tr>
              <td></td>
              <td class="label">الطبيب الجراح :</td>
              <td>{{$service->operation->surgeon}}</td>
              <td></td>
              <td></td>
             </tr>
              @endif

                <tr>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <form id="money_form" class="form-horizontal" enctype="multipart/form-data" action="operation" method = "post">

                    <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">

                    <input type="hidden" name="id"  value="{{$service->id}}">

                 <td><button type="submit" class="btn btn-white"> تأكيد اجراء العملية</button></td>

               </form>


                </tr>
                <tr>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td><button class="btn btn-danger danger" data-toggle="modal" data-target="#{{$service->id}}{{$service->operation->id}}">رفض اجراء العملية</button></td>

                </tr>
              </tbody>

                                <div class="modal fade" id="{{$service->id}}{{$service->operation->id}}" role="dialog" data-backdrop="static" >
                                      <div class="modal-dialog formstyle ">
                                        <!-- Modal content-->
                                            <div class="modal-content formstyle">
                                                <div class="modal-header">
                                                  <h4>رفض اجراء العملية</h4>
                                                  <button type="button" class="close " data-dismiss="modal">&times;</button>
                                                </div>
                                                <div class="modal-body">


                                                    <form id="rejectForm" action="operation" method="post">
                                                        <fieldset id="rejectbody">
                                                          <fieldset>
                                                              <label>سبب الرفض :</label>
                                                              <input  required  name="reason"  type="text">
                                                          </fieldset>
                                                           <div class="row align-items-center justify-content-center">
                                                             <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
                                                             <input type="hidden" name="id"  value="{{$service->id}}">
                                                             <input type="hidden" name="operationId"  value="{{$service->operation->id}}">
                                                             <input class="btn btn-white" value="رفض" type="submit" >
                                                          </div>
                                                        </fieldset>

                                                   </form>
                                              </div>
                                          </div>

                                    </div>

                          </div>
              </tr>
              @endforeach
              </table>


  </div>
  <div id="menu1" class="container tab-pane "><br>


    <table class="table  table-hover paing_table">
      <col width="350">
      <col width="400">
      <col width="350">
      <col width="400">
      <col width="350">


        <thead class="thead-light">
            <tr><th>الكود</th><th>الاسم</th><th>التاريخ والوقت</th><th>الخدمة</th><th>المزيد</th></tr>
        </thead>
        <tbody>
          @foreach($services_confirmed as $service)
                <tr class="clickable" data-toggle="collapse" data-target="#{{$service->id}}" aria-expanded="false" aria-controls="group-of-rows-1">
                    <td>{{$service->patient->code}}</td>
                    <td>{{$service->patient->name}}</td>
                    <td>{{$service->created_at}}</td>
                    <td>@if($service->examinationType == "checkUp")
                      كشف
                    @elseif($service->examinationType == "normalExamination")
                      فحص عادي
                    @elseif($service->examinationType == "lasikExamination")
                      فحص ليزك
                    @elseif($service->examinationType == "normalOperation")
                      عمليات صغرى وكبرى
                    @else
                      عمليات ليزك
                    @endif</td>


                    <td><a  data-toggle="collapse" href="#{{$service->id}}" class="btn btn-white mb-0"  ><i class="fa fa-plus"></i></a></td>
                </tr>

            </tbody>
            <tr colspan="12" >
            <tbody style="background:#F8F8F8;"  id="{{$service->id}}" class="collapse ">
              <tr>
                <td></td>
                <td class="label">تاريخ الميلاد :</td>
                <td>{{$service->patient->DOB}}</td>
                <td class="label">عدد العيون :</td>
                <td>{{$service->eye}}</td>

            </tr>
            <tr>
            <td></td>
            <td class="label">الطبيب المحول :</td>
            <td>{{$service->transformerDoc}}</td>
            <td></td>
            <td></td>
            </tr>
            @if($service->examinationType=='normalOperation')
            <tr>
           <td></td>
            <td class="label">الطبيب الجراح :</td>
            <td>{{$service->operation->surgeon}}</td>
            <td class="label">طبيب التخدير :</td>
            <td>{{$service->operation->anesthetist}}</td>


           </tr>

          <tr>
            <td></td>
            <td class="label">المساعد :</td>
            <td>{{$service->operation->assistantDoctor}}</td>
            <td class="label">البنج :</td>
            <td>{{$service->operation->anesthesia}}</td>


          </tr>
            @endif
            @if($service->examinationType=='lasikOperation')
            <tr>
            <td></td>
            <td class="label">الطبيب الجراح :</td>
            <td>{{$service->operation->surgeon}}</td>
            <td></td>
            <td></td>
           </tr>
            @endif


            </tbody>


            </tr>
            @endforeach
            </table>


  </div>
  <div id="menu2" class="container tab-pane "><br>

    <table class="table  table-hover paing_table">
      <col width="350">
      <col width="400">
      <col width="350">
      <col width="400">
      <col width="350">


        <thead class="thead-light">
            <tr><th>الكود</th><th>الاسم</th><th>التاريخ والوقت</th><th>الخدمة</th><th>المزيد</th></tr>
        </thead>
        <tbody>
          @foreach($services_rejected as $service)
                <tr class="clickable" data-toggle="collapse" data-target="#{{$service->id}}" aria-expanded="false" aria-controls="group-of-rows-1">
                    <td>{{$service->patient->code}}</td>
                    <td>{{$service->patient->name}}</td>
                    <td>{{$service->created_at}}</td>
                    <td>@if($service->examinationType == "checkUp")
                      كشف
                    @elseif($service->examinationType == "normalExamination")
                      فحص عادي
                    @elseif($service->examinationType == "lasikExamination")
                      فحص ليزك
                    @elseif($service->examinationType == "normalOperation")
                      عمليات صغرى وكبرى
                    @else
                      عمليات ليزك
                    @endif</td>


                    <td><a  data-toggle="collapse" href="#{{$service->id}}" class="btn btn-white mb-0"  ><i class="fa fa-plus"></i></a></td>
                </tr>

            </tbody>
            <tr colspan="12" >
            <tbody style="background:#F8F8F8;"  id="{{$service->id}}" class="collapse ">
              <tr>
                <td></td>
                <td class="label">تاريخ الميلاد :</td>
                <td>{{$service->patient->DOB}}</td>
                <td class="label">عدد العيون :</td>
                <td>{{$service->eye}}</td>

            </tr>
            <tr>
            <td></td>
            <td class="label">الطبيب المحول :</td>
            <td>{{$service->transformerDoc}}</td>
            <td></td>
            <td></td>
            </tr>
            @if($service->examinationType=='normalOperation')
            <tr>
           <td></td>
            <td class="label">الطبيب الجراح :</td>
            <td>{{$service->operation->surgeon}}</td>
            <td class="label">طبيب التخدير :</td>
            <td>{{$service->operation->anesthetist}}</td>


           </tr>

          <tr>
            <td></td>
            <td class="label">المساعد :</td>
            <td>{{$service->operation->assistantDoctor}}</td>
            <td class="label">البنج :</td>
            <td>{{$service->operation->anesthesia}}</td>


          </tr>
            @endif
            @if($service->examinationType=='lasikOperation')
            <tr>
            <td></td>
            <td class="label">الطبيب الجراح :</td>
            <td>{{$service->operation->surgeon}}</td>
            <td></td>
            <td></td>
           </tr>
            @endif
            <tr>
            <td></td>
            <td class="label">سبب الرفض :</td>
            <td>{{$service->operation->reason}}</td>
            <td></td>
            <td></td>
            </tr>

            </tbody>


            </tr>
            @endforeach
            </table>

  </div>
</div>
</div>
@stop
