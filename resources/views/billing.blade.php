@extends("master")
@section("content")


<div class="container" style="margin-right:0;">
  <hr>
    <h2 class="col-xs-6 " style="text-align:center; color:#243fb2;">  الخزنة  </h2>
    <hr>
<ul class="nav nav-tabs" role="tablist" >
    <li class="nav-item">
      <a class="nav-link active" data-toggle="tab" href="#home">لم يتم الدفع</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" data-toggle="tab" href="#menu1">تم الدفع</a>
    </li>

  </ul>


   <div class="tab-content">
    <div id="home" class="container tab-pane active"><br>
      @foreach($services as $service)

      <div id="accordion"><div class="card">
        <div class="card-header" data-toggle="collapse" data-target="'#'+{{$service->patient->id}}" aria-expanded="true" aria-controls="collapseOne" id="headingtwo">
         <div class="row">
           <div class="col-2">
            <h5 class="mb-0">{{$service->patient}}</h5>
          </div>
          <div class="col-2">
           <h5 class="mb-0">{{$service->patient->name}}</h5>
         </div>
        <div class="col-2">
         <h5 class="mb-0">{{$service->created_at}}</h5>
      </div>
      <div class="col-2">
       <h5 class="mb-0">{{$service->examinationType}}</h5>
     </div>
     
     <div class="col-2">
      <h5 class="mb-0">{{$service->owedMoney}}</h5>
    </div>
        </div>
      </div>
      <div class="row">
        <div class="col-6">
          <label class="label_name">رقم التليفون : </label>
         <label class="mb-0">{{$service->patient->phone}}</label>
       </div>
       <div class="col-6">
         <label class="label_name">تارخ الميلاد : </label>
        <label class="mb-0">{{$service->patient->DOB}}</label>
      </div>
      </div>
      <div class="row">
        <div class="col-6">
          <label class="label_name">العنوان : </label>
         <label class="mb-0">{{$service->patient->address}}</label>
       </div>
       <div class="col-6">
         <label class="label_name">عدد العيون : </label>
        <label class="mb-0">{{$service->eye}}</label>
      </div>
      </div>
      <div class="row">
        <div class="col-9">
           <label class="mb-0 label_name">الطبيب المحول : </label>
         <label class="mb-0">{{$service->transformerDoc}}</label>
         <input type="number" name="transformerDocMoney">
       </div>
      </div>
      <div class="row">
        <div class="col-9">
           <label class="mb-0 label_name">طبيب الفحص : </label>
         <label class="mb-0">{{$service->examinerDoc}}</label>
         <input type="number" name="examinerDocMoney">
       </div>
      </div>
      <div class="row">
        <div class="col-9">
           <label class="mb-0 label_name">نسبة امركز : </label>
         <input type="number" name="commission">
       </div>
      </div>
      <div class="row">

          <button type="submit" class="btn btn-blue">تأكيد الدفع</button>


      </div>
      <div class="row">
      <!-- <form class="form-horizontal" enctype="multipart/form-data" action="" method = "">
        <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
        <input type="hidden" name="id"  value="{{$patient->id}}">
        <div id="{{$patient->id}}" class="collapse" aria-labelledby="headingtwo" data-parent="#accordion"> -->
        <!-- <div class="row">
          <div class="col-6">
            <label class="label_name">رقم التليفون : </label>
           <label class="mb-0">{{$patient}}</label>
         </div>
         <div class="col-6">
           <label class="label_name">تارخ الميلاد : </label>
          <label class="mb-0">{{$patient->DOB}}</label>
        </div>
        </div>
        <div class="row">
          <div class="col-6">
            <label class="label_name">العنوان : </label>
           <label class="mb-0">{{$patient->address}}</label>
         </div>
         <div class="col-6">
           <label class="label_name">عدد العيون : </label>
          <label class="mb-0">{{$patient->services[0]->eye}}</label>
        </div>
        </div>
        <div class="row">
          <div class="col-9">
             <label class="mb-0 label_name">الطبيب المحول : </label>
           <label class="mb-0">{{$patient->services[0]->transformerDoc}}</label>
           <input type="number" name="transformerDocMoney">
         </div>
        </div>
        <div class="row">
          <div class="col-9">
             <label class="mb-0 label_name">طبيب الفحص : </label>
           <label class="mb-0">{{$patient->services[0]->examinerDoc}}</label>
           <input type="number" name="examinerDocMoney">
         </div>
        </div> -->
        <div class="row">
          <div class="col-9">
             <label class="mb-0 label_name">نسبة امركز : </label>
           <input type="number" name="commission">
         </div>
        </div>
        <div class="row">

            <button type="submit" class="btn btn-blue">تأكيد الدفع</button>


        </div>
      </div>
    </div>
  </div>
<!-- </form> -->

  @endforeach
    </div>
    <div id="menu1" class="container tab-pane fade"><br>
      <h3>Menu 1</h3>
      <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
    </div>

  </div>
</div>
</div>
@stop
