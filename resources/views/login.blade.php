<html>

<head>
    <meta charset="UTF-8">
    <title>Nahar-Admin</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">

    <title>Nahar-Admin</title>
    <link rel="shortcut icon" href="{{ asset('/img/logo(512).png')}}">

    <!-- global stylesheets -->
    <!-- <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed" rel="stylesheet"> -->
    <link rel="stylesheet" href="{{ asset('/admin/css/bootstrap.min.css')}}">
    <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"> -->
    <link rel="stylesheet" href="{{ asset('/font-awesome-4.7.0/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{ asset('/admin/css/font-icon-style.css')}}">
    <link rel="stylesheet" href="{{ asset('/admin/css/style.default.css')}}" id="theme-stylesheet">

    <!-- Core stylesheets -->
    <link rel="stylesheet" href="{{ asset('/admin/css/ui-elements/card.css')}}">
    <link rel="stylesheet" href="{{ asset('/admin/css/style.css')}}">
    <link rel="stylesheet" href="{{ asset('/css/patient.css')}}">
    <link rel="stylesheet" href="{{ asset('/css/login.css') }}">

    <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>

<!-- Isolated Version of Bootstrap, not needed if your site already uses Bootstrap -->
<!-- <link rel="stylesheet" href="https://formden.com/static/cdn/bootstrap-iso.css" /> -->

<!-- Bootstrap Date-Picker Plugin -->
<!-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script> -->




</head>

<body dir="rtl" >

<section class="hero-area">
  <div class="overlay"></div>
  <div class="container">
    <div class="row">
      <div class="col-md-12 ">
          <div class="contact-h-cont">
            <center><img src="{{ asset('/img/rehab.png')}}" style="width:90px; " alt="Nahar" class="img-fluid"></center>
            <h3 class="text-center">مركز النهار لجراحات العيون والليزك</h3><br>
            @if(Session::has('flash_message'))
               <div class="alert alert-info">
                   {{ Session::get('flash_message') }}
               </div>
            @endif
            @if(Session::has('error'))
               <div class="alert alert-danger">
                      {{ Session::get('error') }}
               </div>
            @endif
            <form class="form-horizontal" enctype="multipart/form-data"  action="user_login" method = "post">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <div class="form-group">
                <label for="username">اسم المستخدم</label>
                <input type="text" class="form-control" id="username"  name="userName">
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">كلمة السر</label>
                <input class="form-control" type="password"  id="example-password-input" name="password">
              </div>
              <div class="row align-items-center justify-content-center">
              <button type="submit"  class="btn btn-white" role="button"><i fa fa-right-arrow></i>دخول</button>
            </div>
            </form>
          </div>
      </div>
    </div>
  </div>
</section>
<script src="{{ asset('/admin/js/jquery.min.js')}}"></script>
<script src="{{ asset('/admin/js/popper.min.js')}}"></script>
<script src="{{ asset('/admin/js/tether.min.js')}}"></script>
<script src="{{ asset('/admin/js/bootstrap.min.js')}}"></script>
<script src="{{ asset('/admin/js/jquery.cookie.js')}}"></script>
<script src="{{ asset('/admin/js/jquery.validate.min.js')}}"></script>
<script src="{{ asset('/admin/js/chart.min.js')}}"></script>
<script src="{{ asset('/admin/js/front.js')}}"></script>
<!-- <script src="{{ asset('/admin/js/admin.js')}}"></script> -->
<script src="{{ asset('js/patient.js')}}"></script>

<!--Core Javascript -->
<!-- <script src="js/mychart.js"></script> -->

</body>

</html>
