
@extends("master")
@section("content")

        <div class="content-inner form-cont"  >
            <div class="row">
                <div class="col-md-12">


<form class="form-horizontal" enctype="multipart/form-data" action="patient" method = "post">
          <div class="card form" id="form2">
              <h2 class="col-xs-6 " style="text-align:center; color:#243fb2;"> أضافة خدمات  </h2>
              <div class="">
                @if(Session::has('error'))

   <div class="alert alert-danger alert-dismissible fade show">
      <button type="button" class="close" data-dismiss="alert">&times;</button>
       {{ Session::get('error') }}

   </div>
@endif
<div class="card-header">
  @if(Session::has('message'))

  <div class="alert alert-success alert-dismissible fade show">
     <button type="button" class="close" data-dismiss="alert">&times;</button>
{{ Session::get('message') }}
</div>
@endif
              </div>
              <br>
              <div class="row">
                  <div class="col-md-6">



                      <div class="form-group ">
                          <label for="example-text-input" class=" col-form-label padding padding">كود المريض </label>
                          <div class="col-9">
                              <input  class="form-control" required type="text" name="code" value={{$code}} id="patient_code" onblur="check_patient_code();">
                              <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
                          </div>
                          <span class="padding" id="invalid_patient_code"></span>
                      </div>
                      <div class="form-group ">
                          <label for="example-url-input"  class=" col-form-label padding">الطبيب المحول </label>
                          <div class="col-9">
                              <input class="form-control" type="text" name="transformerDoc" id="doctor_transducer" onblur="check_doctor_transducer_name();">
                          </div>
                          <span  class="padding" id="invalid_doctor_transducer"></span>
                      </div>

                      <div class="form-group " id="checkup_doctor">
                          <label for="example-tel-input"  class=" col-form-label padding">طبيب الفحص </label>
                          <div class="col-9">
                              <!-- <input class="form-control" type="text" name="examinerDoc" id="examination_doctor" onblur="check_examination_doctor_name();"> -->
                              <select class="form-control" style="width: 72%;" id="surgeon" name="examinerDoc" >
                                  <option selected value="none">
                                    اختر اسم الطبيب
                                  </option>
                                  @foreach($surgeonDoctors as $surgeonDoctor)
                                  <option value="{{$surgeonDoctor->docName}}">{{$surgeonDoctor->docName}}</option>
                                  @endforeach
                              </select>
                          </div>
                          <span  class="padding" id="invalid_examination_doctor"></span>
                      </div>




                      <div class="form-group padding ">
                          <label for="exampleSelect1">نوع الفحص </label>
                          <select class="form-control" style="width: 72%;" id="basic_form" name="examinationType" onchange="show_hidden();">
                              <option selected value="checkup">كشف</option>
                              <option value="repeat">اعادة كشف</option>
                              <option value="normalExamination">فحص عادي</option>
                              <option value="lasikExamination">فحص ليزك</option>
                              <option value="normalOperation">عمليات صغري و كبري</option>
                              <option value="lasikOperation">عمليات ليزك</option>
                          </select>
                          <span></span>
                      </div>
                      <div class="form-group padding " id="lasikoperation_price" style="display:none;">
                          <label for="exampleSelect1">عمليات ليزك </label>
                          <select class="form-control" style="width: 72%;" id="basic_form" name="treatment" onchange="">
                             <option selected value="crossLiniking">cross Liniking</option>
                             <option value="proscan+CustomLasik">proscan+CustomLasik </option>
                             <option value="proscan+supracor"> proscan+supracor </option>
                             <option value="proscan+crossLiniking">proscan+crossLiniking</option>
                             <option value="CustomLasik+supracor">CustomLasik+supracor</option>
                             <option value="CustomLasik+crossLiniking"> CustomLasik+crossLiniking </option>
                             <option value="supracor+crossLiniking">supracor+crossLiniking </option>
                             <option value="proscan_OU">proscan_OU </option>
                             <option value="TouchUP_OU"> TouchUP_OU </option>
                             <option value="CustomLasik_OU">CustomLasik_OU </option>
                             <option value="supracor_OU">supracor_OU </option>
                             <option value="CustomLasik">CustomLasik </option>
                             <option value="supracor">supracor</option>
                             <option value="crossLiniking_OU">crossLiniking_OU</option>
                             <option value="proscan">proscan</option>
                             <option value="TouchUP">TouchUP</option>
                         </select>
                          <span></span>
                      </div>
                      <div class="form-group padding " id="normalcheckup_price" style="display:none;">
                          <label for="exampleSelect1">فحص عادي </label>
                          <select class="form-control" style="width: 72%;" id="basic_form" name="diagonistics" onchange="">
                              <option selected value="جلسه_ياج_ليزر_مسح_عدسه_yag_OU">جلسة ياج ليزر (مسح عدسة) yag(ou)</option>
                              <option value="جلسه_ياج_ليزر_مسح_عدسه_yag">جلسة ياج ليزر (مسح عدسة) yag </option>
                              <option value="اشعه_تلفزيونيه_US_OU">اشعة تليفزيونية U.S(ou)</option>
                              <option value="اشعه_تلفزيونيه_US">اشعة تليفزيونية U.S</option>
                              <option value="مقاس_عدسه_OU">مقاس عدسة Biometry(ou)</option>
                              <option value="مقاس_عدسه">مقاس عدسة Biometry</option>
                              <option value="كشف">كشف</option>
                              <option value="اعاده_كشف">اعادة كشف</option>
                              <option value="ازاله_جسم_غريب">في حالة ازالة جسم غريب في حجرة الكشف</option>
                              <option value="ازاله_جسم_غريب_حجره_الكشف_OU">(ou)في حالة ازالة جسم غريب في حجرة الكشف</option>
                              <option value="فحص_فلورسين_للعين_FFAA">فحص فلوروسين للعين FFA</option>
                              <option value="فحص_فلورسين_للعينين_FFAA">فحص فلوروسين للعينين FFA </option>
                              <option value="فحص_للعين_OCT">فحص للعين OCT</option>
                              <option value="فحص_للعينين_OCT">فحص للعينين OCT</option>
                              <option value="فحص_عين_فلورسين_OCT_عين">فحص عين فلوروسين و OCT لعين واحدة</option>
                              <option value="فحص_عين_عنين_OCT_فلورستين_للعنين">فحص عين او عينين OCT و فلوروسين للعينين</option>
                              <option value="فحص_عين_عينين_فلورسين_OCT_لعينين">فحص عنين OCT و فلوروسين للعينين</option>
                          </select>
                          <span></span>
                      </div>
                      <div class="form-group padding " id="lasikcheckup_price" style="display:none;">
                          <label for="exampleSelect1">فحص ليزك </label>
                          <select class="form-control" style="width: 72%;" id="basic_form" name="" onchange="">
                              <option selected value="Pantcam">pantcam</option>
                              <option value="Pantcam_OU">pantcam(ou)</option>
                              <option value="orb_scan">orb-scan</option>
                              <option value="orb_scan_OU">orb-scan(ou)</option>

                          </select>
                          <span></span>
                      </div>
                      <div class="form-group padding " id="eyes_num_div">
                          <label for="exampleSelect1">عدد العيون </label>
                          <select class="form-control" style="width: 72%;" required id="eyes_number" name="eye" >

                              <option value="OD">OD</option>
                              <option value="OS">OS</option>
                              <option value="OU">OU</option>
                          </select>
                          <span></span>
                      </div>

                  </div>
                  <div class="col-md-6">
                  <div id="Facilities_info" style="display:none;">
                <div class="form-group ">
                    <label  class=" col-form-label padding "> اسم المرافق </label>
                    <div class="col-9">
                        <input class="form-control"  type="text" name="companionName" id="Facilities_name" onblur="check_Facilities_name();">

                    </div>
                    <span class="padding" id="invalid_Facilities_name"></span>
                </div>
                <div class="form-group ">
                    <label class=" col-form-label padding padding">رقم موبايل المرافق</label>
                    <div class="col-9">
                        <input class="form-control"  type="text" name="companionPhone" id="Facilities_num" onblur="check_Facilities_phone();">

                    </div>
                    <span class="padding" id="invalid_Facilities_num"></span>
                </div>
                </div>
              </div>


</div></div>

<div class="inbox-body row justify-content-center" dir="ltr" id="normalcheck" style="display:none;" >
<div class="card-header">
    <center style="color:#1a4db6;font-size:35px;" > فحص عادي  </center>
</div>

<table class="table table-inbox ">

<thead>



              <th class="tdstyle ">Diagonistics </th>

                      <th class="view-message dont-show ">OD

                </th>
                <th class="view-message dont-show ">OS


                  </th>
                <th class="view-message dont-show ">OU


                </th>


      </thead>
      <tbody>


              <td class="tdstyle" >جلسة ياج ليزر (مسح عدسة) yag(ou)

              </td>

                <td class="view-message dont-show  "><input type="checkbox" name="eye"  value="OD" disabled>
                </td>
                <td class="view-message dont-show "><input type="checkbox" name="eye" value="OS" class="mail-checkbox" disabled>

                </td>
                <td class="view-message dont-show "><input type="checkbox" name="eye"  value="OU" class="mail-checkbox" >

                </td>



          <tr  class="">
            <td class="tdstyle" >جلسة ياج ليزر (مسح عدسة) yag
            </td>

            <td class="view-message dont-show  "><input type="checkbox" name="eye"  value="OD">
            </td>
            <td class="view-message dont-show "><input type="checkbox" name="eye" value="OS" class="mail-checkbox">

            </td>
            <td class="view-message dont-show "><input type="checkbox" name="eye"  value="OU" class="mail-checkbox" >

            </td>


            </tr>

          <tr class="">

                       <td  class="tdstyle" > اشعة تليفزيونية U.S(ou)</td>
                         <td class="view-message dont-show  "><input type="checkbox" name="eye"  value="OD" disabled>
                         </td>
                         <td class="view-message dont-show "><input type="checkbox" name="eye" value="OS" class="mail-checkbox" disabled>

                         </td>
                         <td class="view-message dont-show "><input type="checkbox" name="eye"  value="OU" class="mail-checkbox">

                         </td>



              </tr>
          <tr class="">

                  <td class="tdstyle" >اشعة تليفزيونية U.S</td>

                  <td class="view-message dont-show  "><input type="checkbox" name="eye"  value="OD">
                  </td>
                  <td class="view-message dont-show "><input type="checkbox" name="eye" value="OS" class="mail-checkbox">

                  </td>
                  <td class="view-message dont-show "><input type="checkbox" name="eye"  value="OU" class="mail-checkbox" >

                  </td>
            </tr>

                  <tr class="">

                          <td class="tdstyle">مقاس عدسة Biometry(ou)</td>

                          <td class="view-message dont-show  "><input type="checkbox" name="eye"  value="OD" disabled>
                          </td>
                          <td class="view-message dont-show "><input type="checkbox" name="eye" value="OS" class="mail-checkbox" disabled>

                          </td>
                          <td class="view-message dont-show "><input type="checkbox" name="eye"  value="OU" class="mail-checkbox" >

                          </td>
                    </tr>

                    <tr class="">

                                <td class="tdstyle" > مقاس عدسة Biometry </td>  <td class="view-message dont-show  "><input type="checkbox" name="eye"  value="OD">
                                  </td>
                                  <td class="view-message dont-show "><input type="checkbox" name="eye" value="OS" class="mail-checkbox" >

                                  </td>
                                  <td class="view-message dont-show "><input type="checkbox" name="eye"  value="OU" class="mail-checkbox" >

                                  </td>

                        </tr>
                        <!-- <tr class="">

                                    <td class="tdstyle">كشف</td><td class="view-message dont-show  "><input type="checkbox" name="eye"  value="OD">
                                      </td>
                                      <td class="view-message dont-show "><input type="checkbox" name="eye" value="OS" class="mail-checkbox" >

                                      </td>
                                      <td class="view-message dont-show "><input type="checkbox" name="eye"  value="OU" class="mail-checkbox" >

                                      </td>

                            </tr> -->

                            <!-- <tr class="">

                                        <td class="tdstyle" > اعادة كشف </td><td class="view-message dont-show  "><input type="checkbox" name="eye"  value="OD">
                                          </td>
                                          <td class="view-message dont-show "><input type="checkbox" name="eye" value="OS" class="mail-checkbox" >

                                          </td>
                                          <td class="view-message dont-show "><input type="checkbox" name="eye"  value="OU" class="mail-checkbox" >

                                          </td>

                                </tr> -->
                                <tr class="">

                                            <td class="tdstyle" >  في حالة ازالة جسم غريب في حجرة الكشف </td><td class="view-message dont-show  "><input type="checkbox" name="eye"  value="OD">
                                              </td>
                                              <td class="view-message dont-show "><input type="checkbox" name="eye" value="OS" class="mail-checkbox" >

                                              </td>
                                              <td class="view-message dont-show "><input type="checkbox" name="eye"  value="OU" class="mail-checkbox" >

                                              </td>

                                    </tr>
                                    <tr class="">

                                                <td class="tdstyle " >  (ou)في حالة ازالة جسم غريب في حجرة الكشف </td><td class="view-message dont-show  "><input type="checkbox" name="eye"  value="OD" disabled>
                                                  </td>
                                                  <td class="view-message dont-show "><input type="checkbox" name="eye" value="OS" class="mail-checkbox" disabled>

                                                  </td>
                                                  <td class="view-message dont-show "><input type="checkbox" name="eye"  value="OU" class="mail-checkbox" >

                                                  </td>

                                        </tr>
                                        <tr class="">

                                                    <td class="tdstyle" > فحص فلوروسين للعين FFA </td><td class="view-message dont-show  "><input type="checkbox" name="eye"  value="OD">
                                                      </td>
                                                      <td class="view-message dont-show "><input type="checkbox" name="eye" value="OS" class="mail-checkbox" >

                                                      </td>
                                                      <td class="view-message dont-show "><input type="checkbox" name="eye"  value="OU" class="mail-checkbox" disabled >

                                                      </td>

                                            </tr>
                                            <tr class="">

                                                        <td class="tdstyle" > فحص فلوروسين للعينين FFA </td><td class="view-message dont-show  "><input type="checkbox" name="eye"  value="OD" disabled>
                                                          </td>
                                                          <td class="view-message dont-show "><input type="checkbox" name="eye" value="OS" class="mail-checkbox" disabled>

                                                          </td>
                                                          <td class="view-message dont-show "><input type="checkbox" name="eye"  value="OU" class="mail-checkbox"  >

                                                          </td>

                                                </tr>
                                              </tr>
                                              <tr class="">

                                                          <td class="tdstyle" >فحص للعين OCT </td><td class="view-message dont-show  "><input type="checkbox" name="eye"  value="OD" >
                                                            </td>
                                                            <td class="view-message dont-show "><input type="checkbox" name="eye" value="OS" class="mail-checkbox">

                                                            </td>
                                                            <td class="view-message dont-show "><input type="checkbox" name="eye"  value="OU" class="mail-checkbox" disabled>

                                                            </td>

                                                  </tr>
                                                </tr>
                                                <tr class="">

                                                            <td class="tdstyle" >فحص للعينين OCT</td><td class="view-message dont-show  "><input type="checkbox" name="eye"  value="OD" disabled>
                                                              </td>
                                                              <td class="view-message dont-show "><input type="checkbox" name="eye" value="OS" class="mail-checkbox" disabled>

                                                              </td>
                                                              <td class="view-message dont-show "><input type="checkbox" name="eye"  value="OU" class="mail-checkbox" >

                                                              </td>

                                                    </tr>
                                                    <tr class="">

                                                                <td class="tdstyle" >فحص عين فلوروسين و OCT لعين واحدة</td><td class="view-message dont-show  "><input type="checkbox" name="eye"  value="OD" >
                                                                  </td>
                                                                  <td class="view-message dont-show "><input type="checkbox" name="eye" value="OS" class="mail-checkbox" >

                                                                  </td>
                                                                  <td class="view-message dont-show "><input type="checkbox" name="eye"  value="OU" class="mail-checkbox" disabled>

                                                                  </td>

                                                        </tr>
                                                        <tr class="">

                                                                    <td class="tdstyle" >فحص عين او عينين فلوروسين و OCT  للعينين</td><td class="view-message dont-show  "><input type="checkbox" name="eye"  value="OD">
                                                                      </td>
                                                                      <td class="view-message dont-show "><input type="checkbox" name="eye" value="OS" class="mail-checkbox" >

                                                                      </td>
                                                                      <td class="view-message dont-show "><input type="checkbox" name="eye"  value="OU" class="mail-checkbox" >

                                                                      </td>

                                                            </tr>
                                                            <tr class="">

                                                                        <td class="tdstyle" >فحص عنين OCT و فلوروسين للعينين</td><td class="view-message dont-show  "><input type="checkbox" name="eye"  value="OD" disabled>
                                                                          </td>
                                                                          <td class="view-message dont-show "><input type="checkbox" name="eye" value="OS" class="mail-checkbox" disabled>

                                                                          </td>
                                                                          <td class="view-message dont-show "><input type="checkbox" name="eye"  value="OU" class="mail-checkbox" >

                                                                          </td>

                                                                </tr>



      </tbody>
</table>

    </div>

    <div class="inbox-body " id="operation" dir="ltr" style="display:none; ">
      <div class="row justify-content-center">
    <div class="card-header">
      <center style="color:#3250C9;font-size:35px;" >عمليات صغري وكبري</center>
    </div>
  </div>
    <div class="row" dir="rtl">
        <div class="col-md-6">
          <div class="form-group ">
              <label for="example-text-input" class=" col-form-label padding padding">الطبيب الجراح </label>
              <div class="col-9">
                  <!-- <input class="form-control" name="surgeon_op" type="text" value="" id="surgeon" onblur="check_surgeonname();"> -->
                  <select class="form-control" style="width: 72%;" id="surgeon" name="surgeon_op" >
                      <option selected value="none">
                        اختر اسم الطبيب
                      </option>
                      @foreach($surgeonDoctors as $surgeonDoctor)
                      <option value="{{$surgeonDoctor->docName}}">{{$surgeonDoctor->docName}}</option>
                      @endforeach
                  </select>
              </div>
            <span class="padding" id="invalid_surgeon"></span>
          </div>
            <div class="form-group ">
                <label for="example-text-input" class=" col-form-label padding padding">طبيب التخدير </label>
                <div class="col-9">
                    <!-- <input class="form-control" name="anesthetist" type="text" value="" id="anesthetist"onblur="check_anesthetist();"> -->
                    <select class="form-control" style="width: 72%;" id="surgeon" name="anesthetist" >
                        <option selected value="none">
                          اختر اسم الطبيب
                        </option>
                        @foreach($anesthetistDoctors as $anesthetistDoctor)
                        <option value="{{$anesthetistDoctor->docName}}">{{$anesthetistDoctor->docName}}</option>
                        @endforeach
                    </select>
                </div>
                <span class="padding" id="invalid_anesthetist"></span>
            </div>
            <div class="form-group ">
                <label for="example-text-input" class=" col-form-label padding padding" > المساعد </label>
                <div class="col-9">
                    <!-- <input class="form-control" name="assistantDoctor" type="text" value="" id="assistantDoctor"onblur="check_assistantDoctor();"> -->
                    <select class="form-control" style="width: 72%;" id="surgeon" name="assistantDoctor" >
                        <option selected value="none">
                          اختر اسم المساعد
                        </option>
                        @foreach($assistantDoctors as $assistantDoctor)
                        <option value="{{$assistantDoctor->docName}}">{{$assistantDoctor->docName}}</option>
                        @endforeach
                    </select>
                </div>
                <span class="padding" id="invalid_assistantDoctor"></span>
            </div>
          </div></div>
<table class="table table-inbox ">
<tbody>

<tr class="">


<td class="tdstyle">العين  </td>

              <td class="view-message dont-show">OD
                  <input type="checkbox" name="eye_op" value="OD" id="eye_od" class="mail-checkbox" onclick="checkeye();">
        </td>
        <td class="view-message dont-show">OS
            <input type="checkbox"  name="eye_op" value="OS" id="eye_os" class="mail-checkbox" onclick="checkeye();">

        </td>
        <td class="view-message dont-show">OU
            <input type="checkbox" name="eye_op" value="OU" id="eye_ou" class="mail-checkbox" onclick="checkeye();">

        </td>
       <td><span id="eye_span"></span></td>

</tr>

<tr class="">


          <td class="tdstyle">البنج </td>
                <td class="view-message dont-show">General
                    <input type="checkbox" name="anesthesia" value="general" id="general" class="mail-checkbox" onclick="checkanesthesia();">
          </td>
          <td class="view-message dont-show">Local
              <input type="checkbox" name="anesthesia"  value="local" id="local" class="mail-checkbox" onclick="checkanesthesia();">

          </td>
 <td><span id="anesthesia_span"></span></td>

  </tr>


</tbody>
</table>


<div class="form-group padding" dir="rtl" >
<label  for="example-text-input" class=" col-form-label  ">العمليات </label>
<select class="form-control"style="width: 35%;" name="operation"  id="operationSelect"onchange="selectoperation();">
  <option selected value="none">اختر عملية</option>
  <option value='مياه_بيضا_عدسه_هندى_phaco_زرع_العدسه(CLE)'>مياه بيضاء عدسه هندى phacoوزرع العدسه(CLE)</option>
  <option value='مياه_بيضا_عدسه_امريكى_phaco'>phaco-مياه بيضاء عدسة امريكي</option>
  <option value='مياه_بيضا_جراحه_Extra_cap(ازاله_عدسه+مياه_بيضاء)'>(ازاله عدسه+مياه بيضاءextra cap-مياه بيضاء جراحة</option>
  <option value='مياه_بيضا_جراحه_بدون_زرع_عدسه'>مياه بيضاء جراحه بدون زرع عدسه</option>
  <option value='مياه_بيضا_عدسه(بوش_اندلومب)'>مياه بيضاء عدسه(بوش اندلومب)</option>
  <option value=phacoبدون_عدسه>phaco بدون عدسه</option>
  <option value='مياه_بيضا_عدسه_ارتيزان'>مياه بيضاء -عدسه ارتيزان</option>
  <option value='تركيب_عدسه_لاصقه_للاطفال'>تركيب عدسه لاصقه للاطفال</option>
  <option value='مياه_بيضا-زرقا_phaco_trabe'>مياه بيضاء +زرقاء</option>
  <option value='مياه_زرقا_فقط'>مياه زرقاء فقط</option>

  <option value='عدسه_هندى'>عدسه هندى</option>
  <option value='عدسه_امريكى'>عدسه امريكى</option>
  <option value="مياه_زرقا_فقط">مياه زرقاء فقط</option>
  <option value= 'الظفره(ptery_gium)'>الظفره(ptery_gium) </option>
  <option value='حقن_افاستين_بالحقنه(AV_astin)_عين'>حقن افاستينبالحقنه|(AV astin)</option>
  <option value='حقن_افاستين_بالحقنه_OU'>حقن افاستين بالحقنه(AV astin)(ou)</option>
  <option value='حقن_كيناكورت_عين'>حقن كيناكورت عين</option>
  <option value='حقن_كيناكورت_OU'>حقن كيناكورت ou</option>
  <option value='حقن_ليوسنتيس_عين'> حقن ليوسنتيس عين</option>
  <option value='حقن_ليوسنتيس_OU'>حقن ليوسنتيس  ou </option>
  <option value= 'حقن_الايليا_عين'>حقن الايليا عين</option>
  <option value='حقن_الايليا_OU'>حقن الايليا ou</option>
  <option value="كيس_دهنى">كيس دهني</option>
  <option value='ازاله_حسنه/ازاله_وحمه'> ازاله حسنه/ازاله وحمه </option>
  <option value='زرع_قرنيه(Keratoplasty)'>زرع قرنيه </option>
  <option value="ارتخاء_الجفن">ارتخاء جفن </option>
  <option value='ارتخاء_الجفن_OU'>ارتخاء جفن ou</option>
  <option value='squintحول(عين_اتنين)(تجميل)'> حول (عين_اتنين)(تجميل)</option>
  <option value="كيس_دمعى">كيس دمعي DCR </option>
  <option value="تسليك_قناه_دمعيه(عين_عنين)(probing)">Probing تسليك قناة دمعيه </option>
  <option value="فحص_تحت_مخدر_عام(قياس_ضغط_عين_تحت_تخدير)">'فحص_تحت_مخدر_عام(قياس_ضغط_عين_تحت_تخدير) </option>
  <option value='ازاله_كيس_بالملتحمه/ازاله_غده'>ازاله كيس بالملتحمه/ازاله ازاله_غده</option>
  <option value="تفريغ_عين"> تفريغ عين</option>
  <option value="شبكيه">Victrectamy شبكية</option>
  <option value="شبكيه_من_غير_سيليكون_وبرفلوكاريون">شبكية من غير سيليكون وبرفلوكاريون</option>
  <option value="ازاله_سيليكون"> ازالة سيليكون</option>
  <option value="صبغ_القرنيه_التاتون">صبغ القرنيه التاتون </option>
  <option value="زانس_لازمه">زانس لازمه</option>
  <option value='خياطه_غرز_بالجفن'>خياطه غرز بالجفن</option>
  <option value='عمليه_شعره(ازاله_شعره)'>عمليه شعره(ازاله شعره)</option>
  <option value="ازاله_غرز">ازالة غرز </option>
  <option value="اصابه_بالعين">اصابة بالعين </option>
  <option value='مياه_بيضا_+ازاله_سيليكون'>مياه بيضاء+ازاله سيلكون </option>
  <option value="ازاله_جسم_غريب_بالقرنيه">ازاله جسم غريب بالقرنيه </option>
  <option value='مياه_بيضاء_للاطفال_بدون_زرع_عدسه'>مياه بيضاء للاطفال بدون زرع عدسه</option>
  <option value='زرع_عدسه_ثانويه_صلبه'>زرع عدسه ثانويه صلبه</option>
  <option value='زرع_عدسه_ثانويه_ثلاثيه'>زرع عدسه ثانويه ثلاثيه</option>
  <option value='زرع_عدسه_ذكيه'>زرع عدسه ذكيه</option>
  <option value='زرع_عدسه_ثانويه_ارتيزان(من_غير_phaco)'>زرع عدسه ثانويه ارتيزان</option>
  <option value='تعديل_عدسه'>تعديل عدسه</option>
  <option value='ازاله_جسم_غريب'>ازاله جسم غريب</option>
  <option value='مسح_عدسه_بالجراحه'>مسح عدسه بالجراحه</option>


  <option value='phaco+AV_astin'>phaco+AV astin</option>


  <span class="padding" id="invalid_selectoperation"></span>

</select>
</div>



</div>


<div class="inbox-body " id="lasikoperation" dir="ltr" style="display:none;">
  <div class="row justify-content-center">
<div class="card-header">
    <center style="color:#3250C9;font-size:35px;" > عمليات ليزك</center>
</div>
</div>
<div class="row" dir="rtl">
    <div class="col-md-6">
        <div class="form-group ">
            <label for="example-text-input" class=" col-form-label padding padding">الطبيب الجراح </label>
            <div class="col-9">
              <select class="form-control" style="width: 72%;" id="surgeon" name="surgeon_la" >
                  <option selected value="none">
                    اختر اسم الطبيب
                  </option>
                  @foreach($surgeonDoctors as $surgeonDoctor)
                  <option value="{{$surgeonDoctor->docName}}">{{$surgeonDoctor->docName}}</option>
                  @endforeach
              </select>
            </div>
            <span class="padding" id="invalid_surgeon"></span>
        </div>
      </div>
      <div class="col-md-6">
          <div class="form-group ">
              <label for="example-text-input" class=" col-form-label padding padding">دكتور داتا</label>
              <div class="col-9">
                <select class="form-control" style="width: 72%;" id="surgeon" name="dataDoctor" >
                    <option selected value="none">
                      اختر اسم الطبيب
                    </option>
                    @foreach($dataDoctors as $dataDoctor)
                    <option value="{{$dataDoctor->docName}}">{{$dataDoctor->docName}}</option>
                    @endforeach
                </select>
              </div>
              <span class="padding" id="invalid_surgeon"></span>
          </div>
        </div>
      <div class="col-md-12" dir="ltr" >

        <table  class="table" >
        	<thead >
        		<tr >
        			<th class="tdstyle">Treatment</th>
        			<th class="tdstyle">OD</th>
              <th class="tdstyle">OS</th>
              <th class="tdstyle">OU</th>
              <th class="tdstyle"></th>
        		</tr>
        	</thead>
        	<tbody>
            <tr>
              <td id="opimizedLasik_row">
              cross Liniking
            <input value="crossLiniking" type="hidden" id="opimizedLasik">
              </td>
              <td>
                <input type="checkbox"  name="eye" value="OD" id="OpimizedLasik_od"  maxlength="254" onclick="opimized_lasik();"/>


              </td>

              <td>
                <input type="checkbox"  name="eye" value="OS" id="OpimizedLasik_os"   maxlength="30" onclick="opimized_lasik();" />
              </td>
              <td>
                <input type="checkbox"  name="eye" value="OU"  id="OpimizedLasik_ou"  maxlength="30" onclick="opimized_lasik();"/>
              </td>
              <td><span id="OpimizedLasik_span"></span></td>
            </tr>
            <tr>
              <td>
              proscan+CustomLasik
                <input value="proscan+CustomLasik" type="hidden" id="lasik_PRK">
              </td>
              <td>
                <input type="checkbox" name="eye" value="OD" id="PRK_od" maxlength="100" onclick="PRK();" />
              </td>
              <td>
                <input type="checkbox" name="eye" value="OS" id="PRK_os"   maxlength="30" onclick="PRK();"/>
              </td>
              <td>
                <input type="checkbox" name="eye" value="OU" id="PRK_ou" maxlength="30" onclick="PRK();"/>
              </td>
              <td><span id="PRK_span"></span></td>
            </tr>
            <tr>
              <td>
              proscan+supracor
              <input value="proscan+supracor" type="hidden" id="custumlasik">
              </td>
              <td>
                <input type="checkbox" name="eye" value="OD" id="custum_lasik_od" maxlength="100" onclick="custum_lasik();" maxlength="100" />
              </td>
              <td>
                <input type="checkbox" name="eye" value="OS" id="custum_lasik_os" maxlength="100" onclick="custum_lasik();"  maxlength="30" />
              </td>
              <td>
                <input type="checkbox" name="eye" value="OU" id="custum_lasik_ou" maxlength="100" onclick="custum_lasik();"  maxlength="30" />
              </td>
              <td><span id="custumlasik_span"></span></td>
            </tr>
            <tr>
              <td>
                proscan+crossLiniking
                <input value="proscan+crossLiniking" type="hidden" id="supracor">
              </td>
              <td>
                <input type="checkbox" name="eye" value="OD" id="Supracor_od" maxlength="100" onclick="Supracor();" maxlength="30" />
              </td>
              <td>
                <input type="checkbox" name="eye" value="OS" id="Supracor_os" maxlength="100" onclick="Supracor();" maxlength="30" />
              </td>
              <td>
                <input type="checkbox" name="eye" value="OU" id="Supracor_ou" maxlength="100" onclick="Supracor();" maxlength="30" />
              </td>
              <td><span id="supracor_span"></span></td>
            </tr>
            <tr>
              <td>
            CustomLasik+supracor
              <input value="CustomLasik+supracor" type="hidden" id="ultralasik">
              </td>
              <td>
                <input type="checkbox" name="eye" value="OD" id="ultra_lasik_od" maxlength="100" onclick="ultra_lasik();"  maxlength="30" />
              </td>
              <td>
                <input type="checkbox" name="eye" value="OS" id="ultra_lasik_os" maxlength="100" onclick="ultra_lasik();" maxlength="30" />
              </td>
              <td>
                <input type="checkbox" name="eye" value="OU" id="ultra_lasik_ou" maxlength="100" onclick="ultra_lasik();" maxlength="30" />
              </td>
              <td><span id="ultralasik_span"></span></td>
            </tr>
            <tr>
              <td>
              CustomLasik+crossLiniking
              <input value="CustomLasik+crossLiniking" type="hidden" id="ptkk">
              </td>
              <td>
                <input type="checkbox" name="eye" value="OD" id="PTK_od" maxlength="100" onclick="PTK();" maxlength="30" />
              </td>
              <td>
                <input type="checkbox" name="eye" value="OS" id="PTK_os" maxlength="100" onclick="PTK();" maxlength="30" />
              </td>
              <td>
                <input type="checkbox" name="eye" value="OU" id="PTK_ou" maxlength="100" onclick="PTK();" maxlength="30" />
              </td>
              <td><span id="ptkk_span"></span></td>
            </tr>
            <tr>
              <td>
                supracor+crossLiniking
                <input value="supracor+crossLiniking" type="hidden" id="CrossLinking">
              </td>
              <td>
                <input type="checkbox" name="eye" value="OD" id="Cross_Linking_od" maxlength="100" onclick="Cross_Linking();" maxlength="30" />
              </td>
              <td>
                <input type="checkbox" name="eye" value="OS" id="Cross_Linking_os" maxlength="100" onclick="Cross_Linking();" maxlength="30" />
              </td>
              <td>
                <input type="checkbox" name="eye" value="OU" id="Cross_Linking_ou" maxlength="100" onclick="Cross_Linking();" maxlength="30" />
              </td>
              <td><span id="CrossLinking_span"></span></td>
            </tr>
            <tr>
              <td>
              proscan_OU
              <input value="proscan_OU" type="hidden" id="keratoplasty">
              </td>
              <td>
                <input type="checkbox" name="eye" value="OD" id="Keratoplasty_od" maxlength="100" onclick="Keratoplasty();"  maxlength="30" />
              </td>
              <td>
                <input type="checkbox" name="eye" value="OS" id="Keratoplasty_os" maxlength="100" onclick="Keratoplasty();" maxlength="30" />
              </td>
              <td>
                <input type="checkbox" name="eye" value="OU" id="Keratoplasty_ou" maxlength="100" onclick="Keratoplasty();" maxlength="30" />
              </td>
              <td><span id="keratoplasty_span"></span></td>


            </tr>
            <tr>
              <td>
              TouchUP_OU
              <input value="TouchUP_OU" type="hidden" id="keratoplasty">
              </td>
              <td>
                <input type="checkbox" name="eye" value="OD" id="Keratoplasty_od" maxlength="100" onclick="Keratoplasty();"  maxlength="30" />
              </td>
              <td>
                <input type="checkbox" name="eye" value="OS" id="Keratoplasty_os" maxlength="100" onclick="Keratoplasty();" maxlength="30" />
              </td>
              <td>
                <input type="checkbox" name="eye" value="OU" id="Keratoplasty_ou" maxlength="100" onclick="Keratoplasty();" maxlength="30" />
              </td>
              <td><span id="keratoplasty_span"></span></td>


            </tr>
            <tr>
              <td>
              CustomLasik_OU
              <input value="CustomLasik_OU" type="hidden" id="keratoplasty">
              </td>
              <td>
                <input type="checkbox" name="eye" value="OD" id="Keratoplasty_od" maxlength="100" onclick="Keratoplasty();"  maxlength="30" />
              </td>
              <td>
                <input type="checkbox" name="eye" value="OS" id="Keratoplasty_os" maxlength="100" onclick="Keratoplasty();" maxlength="30" />
              </td>
              <td>
                <input type="checkbox" name="eye" value="OU" id="Keratoplasty_ou" maxlength="100" onclick="Keratoplasty();" maxlength="30" />
              </td>
              <td><span id="keratoplasty_span"></span></td>


            </tr>
            <tr>
              <td>
              supracor_OU
              <input value="supracor_OU" type="hidden" id="keratoplasty">
              </td>
              <td>
                <input type="checkbox" name="eye" value="OD" id="Keratoplasty_od" maxlength="100" onclick="Keratoplasty();"  maxlength="30" />
              </td>
              <td>
                <input type="checkbox" name="eye" value="OS" id="Keratoplasty_os" maxlength="100" onclick="Keratoplasty();" maxlength="30" />
              </td>
              <td>
                <input type="checkbox" name="eye" value="OU" id="Keratoplasty_ou" maxlength="100" onclick="Keratoplasty();" maxlength="30" />
              </td>
              <td><span id="keratoplasty_span"></span></td>


            </tr>
            <tr>
              <td>
              CustomLasik
              <input value="CustomLasik" type="hidden" id="keratoplasty">
              </td>
              <td>
                <input type="checkbox" name="eye" value="OD" id="Keratoplasty_od" maxlength="100" onclick="Keratoplasty();"  maxlength="30" />
              </td>
              <td>
                <input type="checkbox" name="eye" value="OS" id="Keratoplasty_os" maxlength="100" onclick="Keratoplasty();" maxlength="30" />
              </td>
              <td>
                <input type="checkbox" name="eye" value="OU" id="Keratoplasty_ou" maxlength="100" onclick="Keratoplasty();" maxlength="30" />
              </td>
              <td><span id="keratoplasty_span"></span></td>


            </tr>
            <tr>
              <td>
              supracor
              <input value="supracor" type="hidden" id="keratoplasty">
              </td>
              <td>
                <input type="checkbox" name="eye" value="OD" id="Keratoplasty_od" maxlength="100" onclick="Keratoplasty();"  maxlength="30" />
              </td>
              <td>
                <input type="checkbox" name="eye" value="OS" id="Keratoplasty_os" maxlength="100" onclick="Keratoplasty();" maxlength="30" />
              </td>
              <td>
                <input type="checkbox" name="eye" value="OU" id="Keratoplasty_ou" maxlength="100" onclick="Keratoplasty();" maxlength="30" />
              </td>
              <td><span id="keratoplasty_span"></span></td>


            </tr>
            <tr>
              <td>
              crossLiniking_OU
              <input value="crossLiniking_OU" type="hidden" id="keratoplasty">
              </td>
              <td>
                <input type="checkbox" name="eye" value="OD" id="Keratoplasty_od" maxlength="100" onclick="Keratoplasty();"  maxlength="30" />
              </td>
              <td>
                <input type="checkbox" name="eye" value="OS" id="Keratoplasty_os" maxlength="100" onclick="Keratoplasty();" maxlength="30" />
              </td>
              <td>
                <input type="checkbox" name="eye" value="OU" id="Keratoplasty_ou" maxlength="100" onclick="Keratoplasty();" maxlength="30" />
              </td>
              <td><span id="keratoplasty_span"></span></td>


            </tr>
            <tr>
              <td>
              proscan
              <input value="proscan" type="hidden" id="keratoplasty">
              </td>
              <td>
                <input type="checkbox" name="eye" value="OD" id="Keratoplasty_od" maxlength="100" onclick="Keratoplasty();"  maxlength="30" />
              </td>
              <td>
                <input type="checkbox" name="eye" value="OS" id="Keratoplasty_os" maxlength="100" onclick="Keratoplasty();" maxlength="30" />
              </td>
              <td>
                <input type="checkbox" name="eye" value="OU" id="Keratoplasty_ou" maxlength="100" onclick="Keratoplasty();" maxlength="30" />
              </td>
              <td><span id="keratoplasty_span"></span></td>


            </tr>
            <tr>
              <td>
              TouchUP
              <input value="TouchUP" type="hidden" id="keratoplasty">
              </td>
              <td>
                <input type="checkbox" name="eye" value="OD" id="Keratoplasty_od" maxlength="100" onclick="Keratoplasty();"  maxlength="30" />
              </td>
              <td>
                <input type="checkbox" name="eye" value="OS" id="Keratoplasty_os" maxlength="100" onclick="Keratoplasty();" maxlength="30" />
              </td>
              <td>
                <input type="checkbox" name="eye" value="OU" id="Keratoplasty_ou" maxlength="100" onclick="Keratoplasty();" maxlength="30" />
              </td>
              <td><span id="keratoplasty_span"></span></td>


            </tr>
        	</tbody>
        </table>

      </div></div></div>

      <div class=" " id="lasikcheckup" dir="ltr"style="display:none;">
        <div class="row justify-content-center">
      <div class="card-header">
          <center style="color:#3250C9;font-size:35px;" >فحص ليزك</center>
      </div>
    </div>

            <div class="col-md-12" dir="ltr"data-pattern="priority-columns" >

              <table  class="table" >
              	<thead >
              		<tr >
              			<th class="tdstyle" >Investigation</th>
              			<th  class="tdstyle">OD</th>
                    <th  class="tdstyle">OS</th>
                    <th  class="tdstyle">OU</th>
                    <th  class="tdstyle"></th>

              		</tr>
              	</thead>
              	<tbody>
              		<tr>
              			<td>
              		Lasik assessment
                  <input value="lasikAssessment" type="hidden" class="beeb" id="lasikassessment">
              			</td>
              			<td>
              				<input type="checkbox" name="eye" value="OD" class="eye_class" id="Lasik_assessment_od"  onclick="Lasik_assessment();" maxlength="254" />
              			</td>
                    <td>
                      <input type="checkbox" name="eye" value="OS" class="eye_class" id="Lasik_assessment_os"  onclick="Lasik_assessment();"  maxlength="30" />
                    </td>
                    <td>
                      <input type="checkbox" name="eye" value="OU" class="eye_class" id="Lasik_assessment_ou"  onclick="Lasik_assessment();"  maxlength="30" />
                    </td>
                    <td><span id="lasikassessment_span"></span></td>
              		</tr>
              		<tr>
              			<td >
              				Pentacam
                      <input value="pentacam" type="hidden"  class="beeb" id="pentacam">
              			</td>
              			<td>
              				<input type="checkbox" name="eye" value="OD" class="eye_class" id="Pentacam_od"  onclick="Pentacam();"  maxlength="100" />
              			</td>
                    <td>
                      <input type="checkbox" name="eye" value="OS" class="eye_class" id="Pentacam_os"  onclick="Pentacam();"  maxlength="30" />
                    </td>
                    <td>
                      <input type="checkbox" name="eye" value="OU" class="eye_class" id="Pentacam_ou"  onclick="Pentacam();"  maxlength="30" />
                    </td>
                      <td><span id="pentacam_span"></span></td>
              		</tr>
              		<tr>
              			<td >
              		Aberrometry
                  <input value="aberrometry" type="hidden" class="beeb" id="aberrometry">
              			</td>
              			<td>
              				<input type="checkbox" name="eye" value="OD" class="eye_class" id="Aberrometry_od"  onclick="Aberrometry();" maxlength="100" />
              			</td>
                    <td>
                      <input type="checkbox" name="eye" value="OS" class="eye_class" id="Aberrometry_os"  onclick="Aberrometry();"  maxlength="30" />
                    </td>
                    <td>
                      <input type="checkbox" name="eye" value="OU" class="eye_class" id="Aberrometry_ou"  onclick="Aberrometry();"  maxlength="30" />
                    </td>
                    <td><span id="aberrometry_span"></span></td>
              		</tr>
              		<tr>
              			<td >
              				Topography
                      <input value="topography" type="hidden" class="beeb" id="topography">
              			</td>
                    <td>
                      <input type="checkbox" name="eye" value="OD" class="eye_class" id="Topography_od"  onclick="Topography();" maxlength="30" />
                    </td>
              			<td>
              				<input type="checkbox" name="eye" value="OS" class="eye_class" id="s"  onclick="Topography();"  maxlength="30" />
              			</td>
                    <td>
                      <input type="checkbox" name="eye" value="OU" class="eye_class" id="Topography_ou"  onclick="Topography();"  maxlength="30" />
                    </td>
                    <td><span id="Topography_span"></span></td>
              		</tr>
              		<tr>
              			<td >
              		Wave Front
                  <input value="waveFront" type="hidden" class="beeb" id="wavefront">
              			</td>
                    <td>
                      <input type="checkbox" name="eye" value="OD" class="eye_class" id="Wave_Front_od"  onclick="Wave_Front();"  maxlength="30" />
                    </td>
                    <td>
                      <input type="checkbox" name="eye" value="OS" class="eye_class" id="Wave_Front_os"  onclick="Wave_Front();" maxlength="30" />
                    </td>
                    <td>
                      <input type="checkbox" name="eye" value="OU" class="eye_class" id="Wave_Front_ou"  onclick="Wave_Front();" maxlength="30" />
                    </td>
                      <td><span id="wavefront_span"></span></td>
              		</tr>


              	</tbody>
              </table>

            </div></div>
              <div class="col-md-6 padding" >
            <div class="form-group ">
            <label for="example-week-input"  class=" col-form-label padding">المبلغ المستحق </label>
            <div class="col-9">
            <input class="form-control " type="text" name="owedMoney" required id="money" onblur="check_money();">
            </div>
            <span id="invalid_money"></span>
            </div></div>
            <div class="row justify-content-center align-items-center buttons">
              <div class="col-md-2 " >
            <button type="submit" class=" btn btn-general btn-white mr-2" >تسجيل</button>
      </div>
       <div  class="col-md-2 " >
            <button type="reset" class=" btn btn-danger danger">الغاء </button>
</div>
</div>
          </div>



</div>


</form>
</div>
</div>
</div>

    @stop
