<section >
      <div class="container panel panel-default panel-body">
<hr>
<div class="row">
<h2 class="col">الاطباء</h2>
<button class="col-xs-2 btn btn-success ml-auto add_admin" data-toggle="modal" data-target="#adddoctorModal"><i class="fa fa-plus"></i> اضافة طبيب</button>
</div>
<hr>

 @if(Session::has('flash_message'))
    <div class="alert alert-info">
      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        {{ Session::get('flash_message') }}
    </div>
@endif
@if($errors->any())
<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <div class="alert alert-danger">
        @foreach($errors->all() as $error)
            <p>{{ $error }}</p>
        @endforeach
    </div>
@endif

<div class="responsive">
<table class="table">
  <col width="100">
  <col width="200">
  <col width="200">
  <col width="150">
  <col width="100">
  <col width="100">

    <thead>
      <tr >

        <th>الاسم</th>
        <th>التخصص</th>
        <th>رقم البطاقة</th>
        <th>الموبايل</th>
        <th>تعديل</th>
        <th>حذف</th>
      </tr>
    </thead>
    <tbody>
      <tr>

        @foreach($users as $user)

                  <td>{{$user->userName}} <br/></td>
                  <td>{{$role->name}}</td>
                  <td>{{$user->NID}}</td>
                  <td>{{$user->phone}}</td>
                  <td><button type="button" class="btn btn-white" data-toggle="modal" data-target="#editdoctorModal"  onclick = "edit_doctor('{{$user->id}}');" id="user_Edit" ><i class="fa fa-edit"></i></button></td>
                <td><button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deletedoctorModal"  onclick = "delete_doctor('{{$user->id}}');" id="user_delete" >X</button></td>
              </tr>


        @endforeach

    </tbody>
  </table>

</div>
  <div class="modal fade" id="adddoctorModal" role="dialog">
<div class="modal-dialog" >

  <!-- Modal content-->

  <div class="modal-content">
    <div class="modal-header">
      <h4 class="modal-title">اضافة طبيب</h4>
      <button type="button" class="close" data-dismiss="modal">&times;</button>

    </div>
    <div class="modal-body">

       <form class="form-horizontal" enctype="multipart/form-data" action = "" method = "">

<div class="form-group row">
    <label class="control-label col-sm-3" >الاسم :</label>
<div class="col-sm-9">
  <input type="text" class="form-control" id="doctor_name" required name = "userName" onblur="validatedocUsername();">
  <span id="doctor_nameinvalid" style="color:red;"></span>
</div>
</div>
<div class="form-group row">
    <label class="control-label col-sm-3" >الرقم السري :</label>
<div class="col-sm-9">
  <input type="password" class="form-control" id="doctor_password" required name = "password" onblur="validatedocpassword();">
  <span id="doctor_passinvalid" style="color:red;"></span>
</div>
</div>



<div class="form-group row" >
  <label class="control-label col-sm-3" >التخصص :</label>
  <div class="col-sm-9">
    <select class="custom-select" id="select_doctor" >
     <option  value="" >طبيب جراح</option>
     <option  value="" >طبيب داتا</option>
     <option  value="" >مساعد</option>
     </select>
</div>
</div>




  <div class="form-group row">
    <label class="control-label col-sm-3">رقم البطاقة :</label>
    <div class="col-sm-9">
      <input type="emial" class="form-control" id="doctor_NID" name = "NID" required onblur="validatedocNID_member()">
      <span id="doctor_NIDinvalid" style="color:red;"></span>
    </div>

</div>
<div class="form-group row">
  <label class="control-label col-sm-3">الموبايل :</label>
  <div class="col-sm-9">
    <input type="text" class="form-control" id="doctor_phone" name = "phone" required onblur="validatedocphone_member()">
    <span id="doctor_phoneinvalid" style="color:red;"></span>
  </div>

</div>



<input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
<div class="form-group">
<div class=" row align-items-center justify-content-center">
  <button type="submit" class="btn btn-white" onclick="return validate_adddoc();"  id="add_save" >اضافة</button>
</div>
</div>
</form>
</div>
</div>
</div>
</div>

<div class="modal fade" id="editdoctorModal" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title"> تعديل بيانات الطبيب </h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>

      </div>
      <div class="modal-body">
          <form class="form-horizontal" enctype="multipart/form-data" action = "" method = "">


            <div class="form-group row">
                <label class="control-label col-sm-3" >الاسم :</label>
            <div class="col-sm-9">
              <input type="text" class="form-control" id="olddoctor_name" required name = "userName" onblur="oldvalidateUsername();">
              <span id="olddoctor_nameinvalid" style="color:red;"></span>
            </div>
            </div>
            <!-- <div class="form-group row">
                <label class="control-label col-sm-3" >الرقم السري :</label>
            <div class="col-sm-9">
              <input type="password" class="form-control" id="oldmember_password" required name = "password" onblur="oldvalidatepassword();">
              <span id="oldmember_passinvalid"></span>
            </div>
            </div> -->
            <div class="form-group row" >
              <label class="control-label col-sm-3" >التخصص :</label>
              <div class="col-sm-9">
                <select class="custom-select" id="oldselect_doctor" >
                 <option  value="" >طبيب جراح</option>
                 <option  value="" >طبيب داتا</option>
                 <option  value="" >مساعد</option>
                 </select>
            </div>
            </div>




              <div class="form-group row">
                <label class="control-label col-sm-3">رقم البطاقة :</label>
                <div class="col-sm-9">
                  <input type="emial" class="form-control" id="olddoctor_NID" name = "NID" required onblur="oldvalidateNID_member()">
                   <span id="olddoctor_NIDinvalid" style="color:red;"></span>
                </div>

            </div>
            <div class="form-group row">
              <label class="control-label col-sm-3">الموبايل :</label>
              <div class="col-sm-9">
                <input type="text" class="form-control" id="olddoctor_phone" name = "phone" required onblur="oldvalidatephone_member()">
                <span id="olddoctor_phoneinvalid" style="color:red;"></span>
              </div>

            </div>




  <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
  <input type="hidden" name="id" id="editdoctor_id">
<div class="form-group">
  <div class=" row align-items-center justify-content-center">
    <button  id="save_update"  type="submit" onclick="this.form.submit()"  class="btn btn-white"data-dismiss="modal" >حفظ التغيرات</button>
  </div>
</div>
      <!-- </form> -->
    </div>

  </div>
</div>

</div>

<div class="modal fade" id="deletedoctorModal" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">هل انت متأكد ؟</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>


      </div>
      <div class="modal-body">
       <strong style="color:red;">
سوف تقوم بحذف جميع بيانات هذا الطبيب
       </strong>
      </div>
      <div class="modal-footer">

        <form class="form-horizontal" action = "destroy" method = "post">


          <input type="hidden" name="id" id="doctor_id">
          <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
          <button type="button" class="btn btn-blue" data-dismiss="modal" onclick="this.form.submit()">نعم</button>
          <button type="button" class="btn btn-blue" data-dismiss="modal">لا</button>
        </form>


      </div>
    </div>

  </div>
</div>
</section>
