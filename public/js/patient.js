
function show_hidden()
{
  if($('#basic_form :selected').val() === "normalExamination"){
    $("#operation").hide();
    $("#lasikoperation").hide();
    $("#lasikcheckup").hide();
    $("#lasikoperation_price").hide();
    $("#lasikcheckup_price").hide();
    $("#normalcheckup_price").show();
  $("#normalcheck").show();
  $("#Facilities_info").show();
  $("#eyes_num_div").hide();
  $("#money").prop('disabled', false);
  // $('html,body').animate({ scrollTop:200 }, 'slow');
}
else if ($('#basic_form :selected').val() === "repeat")
 {
  $("#money").val(0);
  $("#money").prop('disabled', true);
 }
  else if ($('#basic_form :selected').val() === "lasikExamination") {
    $("#normalcheck").hide();
    $("#operation").hide();
    $("#lasikoperation").hide();
    $("#lasikcheckup").show();
    $("#Facilities_info").show();
    $("#lasikoperation_price").hide();
    $("#lasikcheckup_price").show();
    $("#normalcheckup_price").hide();
    $("#eyes_num_div").hide();
    $("#money").prop('disabled', false);
    // $('html,body').animate({ scrollTop:200 }, 'slow');
  }
  else if ($('#basic_form :selected').val() === "normalOperation") {
    $("#normalcheck").hide();
    $("#operation").show();
    $("#lasikoperation").hide();
    $("#lasikcheckup").hide();
    $("#checkup_doctor").hide();
    $("#Facilities_info").show();
    $("#eyes_num_div").hide();
    $("#lasikoperation_price").hide();
    $("#lasikcheckup_price").hide();
    $("#normalcheckup_price").hide();
    $("#money").prop('disabled', false);
    // $('html,body').animate({ scrollTop:200 }, 'slow');
  }
  else if ($('#basic_form :selected').val() === "lasikOperation") {
    $("#normalcheck").hide();
    $("#operation").hide();
    $("#lasikoperation").show();
    $("#lasikoperation_price").show();
    $("#lasikcheckup_price").hide();
    $("#normalcheckup_price").hide();
    $("#lasikcheckup").hide();
    $("#checkup_doctor").hide();
    $("#Facilities_info").show();
    $("#eyes_num_div").hide();
    $("#money").prop('disabled', false);
    // $('html,body').animate({ scrollTop:200 }, 'slow');
  }
  else if ($('#basic_form :selected').val() === "checkup") {
    $("#normalcheck").hide();
    $("#operation").hide();
    $("#lasikoperation").hide();
    $("#lasikcheckup").hide();
    $("#Facilities_info").hide();
    $("#lasikoperation_price").hide();
    $("#lasikcheckup_price").hide();
    $("#normalcheckup_price").hide();
    $("#money").prop('disabled', false);
    // $('html,body').animate({ scrollTop:200 }, 'slow');
  }
}
function enable_disable()
{
  if($("#diagonistics_od").is(":checked"))
  {
    $(".od").prop('disabled', false);
    $(".os").prop('disabled', true);
    $(".ou").prop('disabled', true);
    $(".public").prop('disabled', false);

  }
  if($("#diagonistics_os").is(":checked"))
  {
    $(".od").prop('disabled', true);
    $(".os").prop('disabled', false);
    $(".ou").prop('disabled', true);
    $(".public").prop('disabled', false);
  }
  if($("#diagonistics_ou").is(":checked"))
  {
    $(".od").prop('disabled', true);
    $(".os").prop('disabled', true);
    $(".ou").prop('disabled', false);
    $(".public").prop('disabled', false);
  }
}

function selectoperation()
{
  if($('#operationSelect:selected').val() === "none"){
    $("#invalid_selectoperation").html(" اختار عملية");
    return false;
}

}





// function octcheck()
// {
//   if($("#oct_Macula").is(":checked")||$("#oct_Glaucoma").is(":checked")||$("#oct_Anerior").is(":checked"))
//   {
//    $('#oct').prop('checked', true);
//   }
// }
function check_patient_code()
{
  var pattern=new RegExp(/[ء-ي]/);
  if($("#patient_codes").val()==="")
  {
    $("#invalid_patient_code").html("يجب ادخال كود المريض ");
    return false;
  }
  else if(pattern.test($("#patient_code").val()))
  {
    $("#invalid_patient_code").html("الكود يجب الا يحتوي علي حروف عربي");
      return false;
  }
  else {
    $("#invalid_patient_code").html("");
    return true;
  }
}
function check_phone()
{
   var pattern=new RegExp(/[^0-9]/);

   if(pattern.test($("#patient_phone").val()))
   {
      $("#invalid_patient_phone").html('رقم غير صحيح');
return false;
   }
   else if($("#patient_phone").val().length !== 11)
  {
     $("#invalid_patient_phone").html('رقم التليفون يجب ان يكون 11 رقم ');
return false;
  }

  else{
     $("#invalid_patient_phone").hide();
     return true;
  }
}
function check_id()
{
   var pattern=new RegExp(/[^0-9]/);

   if(pattern.test($("#patient_id").val()))
   {
      $("#invalid_patient_id").html('رقم غير صحيح');
return false;
   }
   else if($("#patient_id").val().length !== 14)
  {
     $("#invalid_patient_id").html('رقم البطاقة يجب ان يكون 14 رقم ');
return false;
  }

  else{
     $("#invalid_patient_id").hide();
     return true;
  }
}


function check_eye_num()
{
   var pattern=new RegExp(/[^0-9]/);
   if($("#eyes_number").val() > 2)
    {
       $("#invalid_eyes_number").html('يجب ادخال رقم لا يزيد عن 2');
        return false;
    }

  else{
     $("#invalid_eyes_number").hide();
     return true;
  }
}

function check_name()
{
  var pattern=new RegExp(/[^a-zA-Zء-ي\s]/);
if($("#patient_name").val()==="")
    {
      $("#invalid_patient_name").html("يجب ادخال الاسم ");
    return false;

    }
  else if($("#patient_name").val().length < 2)
  {
    $("#invalid_patient_name").html('الاسم لا يقل عن حرفان ');
    return false;
  }

else if($("#patient_name").val().length > 50)
{
  $("#invalid_patient_name").html('الاسم لا يزيد عن 50 حرف');
    return false;
}


else if(pattern.test($("#patient_name").val()))
{
  $("#invalid_patient_name").html("الاسم يجب الا يحتوي علي ارقام ");
    return false;
}
  else
    {
    $("#invalid_patient_name").eq(0).hide();
    return true;
    }

}

function check_surgeonname()
{
    var pattern=new RegExp(/[^a-zA-Zء-ي\s]/);
if($("#surgeon").val()==="")
    {
      $("#invalid_surgeon").html("يجب ادخال الاسم ");
    return false;

    }
  else if($("#surgeon").val().length < 2)
  {
    $("#invalid_surgeon").html('الاسم لا يقل عن حرفان ');
    return false;
  }

else if($("#surgeon").val().length > 50)
{
  $("#invalid_surgeon").html('الاسم لا يزيد عن 50 حرف');
    return false;
}


else if(pattern.test($("#surgeon").val()))
{
  $("#invalid_surgeon").html("الاسم يجب الا يحتوي علي ارقام ");
    return false;
}
  else
    {
    $("#invalid_surgeon").eq(0).hide();
    return true;
    }

}
function check_anesthetist()
{
    var pattern=new RegExp(/[^a-zA-Zء-ي\s]/);
if($("#anesthetist").val()==="")
    {
      $("#invalid_anesthetist").html("يجب ادخال الاسم ");
    return false;

    }
  else if($("#anesthetist").val().length < 2)
  {
    $("#invalid_anesthetist").html('الاسم لا يقل عن حرفان ');
    return false;
  }

else if($("#anesthetist").val().length > 50)
{
  $("#invalid_anesthetist").html('الاسم لا يزيد عن 50 حرف');
    return false;
}


else if(pattern.test($("#anesthetist").val()))
{
  $("#invalid_anesthetist").html("الاسم يجب الا يحتوي علي ارقام ");
    return false;
}
  else
    {
    $("#invalid_anesthetist").eq(0).hide();
    return true;
    }

}

function check_assistantDoctor()
{
    var pattern=new RegExp(/[^a-zA-Zء-ي\s]/);
if($("#assistantDoctor").val()==="")
    {
      $("#invalid_assistantDoctor").html("يجب ادخال الاسم ");
    return false;

    }
  else if($("#assistantDoctor").val().length < 2)
  {
    $("#invalid_assistantDoctor").html('الاسم لا يقل عن حرفان ');
    return false;
  }

else if($("#assistantDoctor").val().length > 50)
{
  $("#invalid_assistantDoctor").html('الاسم لا يزيد عن 50 حرف');
    return false;
}


else if(pattern.test($("#assistantDoctor").val()))
{
  $("#invalid_assistantDoctor").html("الاسم يجب الا يحتوي علي ارقام ");
    return false;
}
  else
    {
    $("#invalid_assistantDoctor").eq(0).hide();
    return true;
    }

}
function check_dob(){

var date= $( "#patient_dob" ).val();

       var dob = new Date(date);
       var today = new Date();

      if ( today.getFullYear()- dob.getFullYear()<18)
      {
        $("#parent_info").show();
      }
      else
      {
        $("#parent_info").hide();
      }
}
function check_parent_name()
{
  var pattern=new RegExp(/[^a-zA-Zء-ي\s]/);
if($("#parent_name").val()==="")
    {
      $("#invalid_parent_name").html("يجب ادخال الاسم ");
    return false;

    }
  else if($("#parent_name").val().length < 2)
  {
    $("#invalid_parent_name").html('الاسم لا يقل عن حرفان ');
    return false;
  }

else if($("#parent_name").val().length > 50)
{
  $("#invalid_parent_name").html('الاسم لا يزيد عن 50 حرف');
    return false;
}


else if(pattern.test($("#parent_name").val()))
{
  $("#invalid_patient_name").html("الاسم يجب الا يحتوي علي ارقام ");
    return false;
}
  else
    {
    $("#invalid_parent_name").eq(0).hide();
    return true;
    }

}
///
function check_Facilities_name()
{
    var pattern=new RegExp(/[^a-zA-Zء-ي\s]/);
if($("#Facilities_name").val()==="")
    {
      $("#invalid_Facilities_name").html("يجب ادخال الاسم ");
    return false;

    }
  else if($("#Facilities_name").val().length < 2)
  {
    $("#invalid_Facilities_name").html('الاسم لا يقل عن حرفان ');
    return false;
  }

else if($("#Facilities_name").val().length > 50)
{
  $("#invalid_Facilities_name").html('الاسم لا يزيد عن 50 حرف');
    return false;
}


else if(pattern.test($("#Facilities_name").val()))
{
  $("#invalid_Facilities_name").html("الاسم يجب الا يحتوي علي ارقام ");
    return false;
}
  else
    {
    $("#invalid_Facilities_name").eq(0).hide();
    return true;
    }

}
function check_parent_phone()
{
   var pattern=new RegExp(/[^0-9]/);

   if(pattern.test($("#parent_phone").val()))
   {
      $("#invalid_parent_phone").html('رقم غير صحيح');
return false;
   }
   else if($("#parent_phone").val().length !== 11)
  {
     $("#invalid_parent_phone").html('رقم التليفون يجب ان يكون 11 رقم ');
return false;
  }

  else{
     $("#invalid_parent_phone").hide();
     return true;
  }
}
///
function check_Facilities_phone()
{
   var pattern=new RegExp(/[^0-9]/);

   if(pattern.test($("#Facilities_num").val()))
   {
      $("#invalid_Facilities_num").html('رقم غير صحيح');
return false;
   }
   else if($("#Facilities_num").val().length !== 11)
  {
     $("#invalid_Facilities_num").html('رقم التليفون يجب ان يكون 11 رقم ');
return false;
  }

  else{
     $("#invalid_Facilities_num").hide();
     return true;
  }
}

function check_examination_doctor_name()
{
    var pattern=new RegExp(/[^a-zA-Zء-ي\s]/);

   if($("#examination_doctor").val().length < 2)
  {
    $("#invalid_examination_doctor").html('الاسم لا يقل عن حرفان ');
    return false;
  }

else if($("#examination_doctor").val().length > 50)
{
  $("#invalid_examination_doctor").html('الاسم لا يزيد عن 50 حرف');
    return false;
}


else if(pattern.test($("#examination_doctor").val()))
{
  $("#invalid_examination_doctor").html("الاسم يجب الا يحتوي علي ارقام ");
    return false;
}
  else
    {
    $("#invalid_examination_doctor").eq(0).hide();
    return true;
    }

}
function check_doctor_transducer_name()
{
      var pattern=new RegExp(/[^a-zA-Zء-ي\s]/);


 if($("#doctor_transducer").val().length > 50)
{
  $("#invalid_doctor_transducer").html('الاسم لايزيد عن 50 حرف');
    return false;
}


else if(pattern.test($("#doctor_transducer").val()))
{
  $("#invalid_doctor_transducer").html("الاسم يجب الا يحتوي علي ارقام ");
    return false;
}
  else
    {
    $("#invalid_doctor_transducer").eq(0).hide();
    return true;
    }

}
function check_money()
{
  var pattern=new RegExp(/[^0-9]/);
  if($("#money").val()==="")
      {
        $("#invalid_money").html("يجب ادخال المبلغ المستحق");
      return false;

      }
  else if(pattern.test($("#money").val()))
  {
    $("#invalid_money").html("المبلغ المستحق يجب الا يحتوي علي حروف");
      return false;
  }
    else
      {
      $("#invalid_money").eq(0).hide();
      return true;
      }
}

function opimized_lasik()
{
  $("#lasik_PRK").attr("name","");
  $("#custumlasik").attr("name","");
  $("#supracor").attr("name","");
  $("#ultralasik").attr("name","");
  $("#CrossLinking").attr("name","");
  $("#ptkk").attr("name","");
  $("#keratoplasty").attr("name","");
  if($("#OpimizedLasik_od").is(":checked")||$("#OpimizedLasik_os").is(":checked")||$("#OpimizedLasik_ou").is(":checked"))
  {
    $("#opimizedLasik").attr('name', 'treatment');


  }
  if(($("#OpimizedLasik_od").is(":checked")&&$("#OpimizedLasik_os").is(":checked"))||($("#OpimizedLasik_od").is(":checked")&&$("#OpimizedLasik_ou").is(":checked"))||($("#OpimizedLasik_ou").is(":checked")&&$("#OpimizedLasik_os").is(":checked")))
  {
    $("#OpimizedLasik_span").html("لا يمكن اختيار اكثر من نوع");
    return false;
  }
  else {
    $("#OpimizedLasik_span").html("");
    return true;
  }

}
function PRK()
{
  $("#opimizedLasik").attr("name","");
  $("#custumlasik").attr("name","");
  $("#supracor").attr("name","");
  $("#ultralasik").attr("name","");
  $("#CrossLinking").attr("name","");
  $("#ptkk").attr("name","");
  $("#keratoplasty").attr("name","");
  if($("#PRK_od").is(":checked")||$("#PRK_os").is(":checked")||$("#PRK_ou").is(":checked"))
  {
    $("#lasik_PRK").attr("name","treatment");
  }
  if(($("#PRK_od").is(":checked")&&$("#PRK_os").is(":checked"))||($("#PRK_od").is(":checked")&&$("#PRK_ou").is(":checked"))||($("#PRK_ou").is(":checked")&&$("#PRK_os").is(":checked")))
  {
    $("#PRK_span").html("لا يمكن اختيار اكثر من نوع");
    return false;
  }
  else {
    $("#PRK_span").html("");
    return true;
  }
}


function custum_lasik()
{
  $("#opimizedLasik").attr("name","");
$("#lasik_PRK").attr("name","");
$("#supracor").attr("name","");
$("#ultralasik").attr("name","");
$("#CrossLinking").attr("name","");
$("#ptkk").attr("name","");
$("#keratoplasty").attr("name","");
  if($("#custum_lasik_od").is(":checked")||$("#custum_lasik_os").is(":checked")||$("#custum_lasik_ou").is(":checked"))
  {
    $("#custumlasik").attr("name","treatment");
  }
  if(($("#custum_lasik_od").is(":checked")&&$("#custum_lasik_os").is(":checked"))||($("#custum_lasik_od").is(":checked")&&$("#custum_lasik_ou").is(":checked"))||($("#custum_lasik_ou").is(":checked")&&$("#custum_lasik_os").is(":checked")))
  {
    $("#custumlasik_span").html("لا يمكن اختيار اكثر من نوع");
    return false;
  }
  else {
    $("#custumlasik_span").html("");
    return true;
  }
}



function Supracor()
{
  $("#opimizedLasik").attr("name","");
$("#lasik_PRK").attr("name","");
$("#custumlasik").attr("name","");
$("#ultralasik").attr("name","");
$("#CrossLinking").attr("name","");
$("#ptkk").attr("name","");
$("#keratoplasty").attr("name","");
  if($("#Supracor_od").is(":checked")||$("#Supracor_os").is(":checked")||$("#Supracor_ou").is(":checked"))
  {
    $("#supracor").attr("name","treatment");
  }
  if(($("#Supracor_od").is(":checked")&&$("#Supracor_os").is(":checked"))||($("#Supracor_od").is(":checked")&&$("#Supracor_ou").is(":checked"))||($("#Supracor_ou").is(":checked")&&$("#Supracor_os").is(":checked")))
  {
    $("#supracor_span").html("لا يمكن اختيار اكثر من نوع");
    return false;
  }
  else {
    $("#supracor_span").html("");
    return true;
  }

}



function ultra_lasik()
{
  $("#opimizedLasik").attr("name","");
$("#lasik_PRK").attr("name","");
$("#custumlasik").attr("name","");
$("#supracor").attr("name","");
$("#CrossLinking").attr("name","");
$("#ptkk").attr("name","");
$("#keratoplasty").attr("name","");
  if($("#ultra_lasik_od").is(":checked")||$("#ultra_lasik_os").is(":checked")||$("#ultra_lasik_ou").is(":checked"))
  {
    $("#ultralasik").attr("name","treatment");
  }
  if(($("#ultra_lasik_od").is(":checked")&&$("#ultra_lasik_os").is(":checked"))||($("#ultra_lasik_od").is(":checked")&&$("#ultra_lasik_ou").is(":checked"))||($("#ultra_lasik_ou").is(":checked")&&$("#ultra_lasik_os").is(":checked")))
  {
    $("#ultralasik_span").html("لا يمكن اختيار اكثر من نوع");
    return false;
  }
  else {
    $("#ultralasik_span").html("");
    return true;
  }
}


function Cross_Linking()
{
  $("#opimizedLasik").attr("name","");
$("#lasik_PRK").attr("name","");
$("#custumlasik").attr("name","");
$("#supracor").attr("name","");
$("#ultralasik").attr("name","");
$("#ptkk").attr("name","");
$("#keratoplasty").attr("name","");
  if($("#Cross_Linking_od").is(":checked")||$("#Cross_Linking_os").is(":checked")||$("#Cross_Linking_ou").is(":checked"))
  {
    $("#CrossLinking").attr("name","treatment");
  }
  if(($("#Cross_Linking_od").is(":checked")&&$("#Cross_Linking_os").is(":checked"))||($("#Cross_Linking_od").is(":checked")&&$("#Cross_Linking_ou").is(":checked"))||($("#Cross_Linking_ou").is(":checked")&&$("#Cross_Linking_os").is(":checked")))
  {
    $("#CrossLinking_span").html("لا يمكن اختيار اكثر من نوع");
    return false;
  }
  else {
    $("#CrossLinking_span").html("");
    return true;
  }
}


function PTK()
{
  $("#opimizedLasik").attr("name","");
$("#lasik_PRK").attr("name","");
$("#custumlasik").attr("name","");
$("#supracor").attr("name","");
$("#ultralasik").attr("name","");
$("#CrossLinking").attr("name","");
$("#keratoplasty").attr("name","");
  if($("#PTK_od").is(":checked")||$("#PTK_os").is(":checked")||$("#PTK_ou").is(":checked"))
  {
    $("#ptkk").attr("name","treatment");
  }
  if(($("#PTK_od").is(":checked")&&$("#PTK_os").is(":checked"))||($("#PTK_od").is(":checked")&&$("#PTK_ou").is(":checked"))||($("#PTK_ou").is(":checked")&&$("#PTK_os").is(":checked")))
  {
    $("#ptkk_span").html("لا يمكن اختيار اكثر من نوع");
    return false;
  }
  else {
    $("#ptkk_span").html("");
    return true;
  }
}
function Keratoplasty()
{
  $("#opimizedLasik").attr("name","");
$("#lasik_PRK").attr("name","");
$("#custumlasik").attr("name","");
$("#supracor").attr("name","");
$("#ultralasik").attr("name","");
$("#CrossLinking").attr("name","");
$("#ptkk").attr("name","");
  if($("#Keratoplasty_od").is(":checked")||$("#Keratoplasty_os").is(":checked")||$("#Keratoplasty_ou").is(":checked"))
  {
    $("#keratoplasty").attr("name","treatment");
  }
  if(($("#Keratoplasty_od").is(":checked")&&$("#Keratoplasty_os").is(":checked"))||($("#Keratoplasty_od").is(":checked")&&$("#Keratoplasty_ou").is(":checked"))||($("#Keratoplasty_ou").is(":checked")&&$("#Keratoplasty_os").is(":checked")))
  {
    $("#keratoplasty_span").html("لا يمكن اختيار اكثر من نوع");
    return false;
  }
  else {
    $("#keratoplasty_span").html("");
    return true;
  }
}

function Lasik_assessment()
{
  $("#pentacam").attr("name","");
  $("#aberrometry").attr("name","");
  $("#Wave_Front").attr("name","");
  $("#topography").attr("name","");
  if($("#Lasik_assessment_od").is(":checked")||$("#Lasik_assessment_os").is(":checked")||$("#Lasik_assessment_ou").is(":checked"))
  {
    $("#lasikassessment").attr("name","investigation");
  }
  if(($("#Lasik_assessment_od").is(":checked")&&$("#Lasik_assessment_os").is(":checked"))||($("#Lasik_assessment_od").is(":checked")&&$("#Lasik_assessment_ou").is(":checked"))||($("#Lasik_assessment_ou").is(":checked")&&$("#Lasik_assessment_os").is(":checked")))
  {
    $("#lasikassessment_span").html("لا يمكن اختيار اكثر من نوع");
    return false;
  }
  else {
    $("#lasikassessment_span").html("");
    return true;
  }
}

function Pentacam()
{
  $("#lasikassessment").attr("name","");
  $("#aberrometry").attr("name","");
  $("#Wave_Front").attr("name","");
  $("#topography").attr("name","");
  if($("#Pentacam_od").is(":checked")||$("#Pentacam_os").is(":checked")||$("#Pentacam_ou").is(":checked"))
  {
    $("#pentacam").attr("name","investigation");
  }
  if(($("#Pentacam_od").is(":checked")&&$("#Pentacam_os").is(":checked"))||($("#Pentacam_od").is(":checked")&&$("#Pentacam_ou").is(":checked"))||($("#Pentacam_ou").is(":checked")&&$("#Pentacam_os").is(":checked")))
  {
    $("#pentacam_span").html("لا يمكن اختيار اكثر من نوع");
    return false;
  }
  else {
    $("#pentacam_span").html("");
    return true;
  }
}

function Aberrometry()
{
$("#lasikassessment").attr("name","");
$("#pentacam").attr("name","");
$("#Wave_Front").attr("name","");
$("#topography").attr("name","");
  if($("#Aberrometry_od").is(":checked")||$("#Aberrometry_os").is(":checked")||$("#Aberrometry_ou").is(":checked"))
  {
    $("#aberrometry").attr("name","investigation");
  }
  if(($("#Aberrometry_od").is(":checked")&&$("#Aberrometry_os").is(":checked"))||($("#Aberrometry_od").is(":checked")&&$("#Aberrometry_ou").is(":checked"))||($("#Aberrometry_ou").is(":checked")&&$("#Aberrometry_os").is(":checked")))
  {
    $("#aberrometry_span").html("لا يمكن اختيار اكثر من نوع");
    return false;
  }
  else {
    $("#aberrometry_span").html("");
    return true;
  }
}

function Topography()
{
  $("#lasikassessment").attr("name","");
  $("#pentacam").attr("name","");
  $("#aberrometry").attr("name","");
  $("#Wave_Front").attr("name","");
  if($("#Topography_od").is(":checked")||$("#Topography_os").is(":checked")||$("#Topography_ou").is(":checked"))
  {
    $("#topography").attr("name","investigation");
  }
  if(($("#Topography_od").is(":checked")&&$("#Topography_os").is(":checked"))||($("#Topography_od").is(":checked")&&$("#Topography_ou").is(":checked"))||($("#Topography_ou").is(":checked")&&$("#Topography_os").is(":checked")))
  {
    $("#Topography_span").html("لا يمكن اختيار اكثر من نوع");
    return false;
  }
  else {
    $("#Topography_span").html("");
    return true;
  }
}

function Wave_Front()
{
  $("#pentacam").attr("name","");
  $("#aberrometry").attr("name","");
  $("#lasikassessment").attr("name","");
  $("#topography").attr("name","");
  if($("#Wave_Front_od").is(":checked")||$("#Wave_Front_os").is(":checked")||$("#Wave_Front_ou").is(":checked"))
  {
    $("#wavefront").attr("name","investigation");
  }
  if(($("#Wave_Front_od").is(":checked")&&$("#Wave_Front_os").is(":checked"))||($("#Wave_Front_od").is(":checked")&&$("#Wave_Front_ou").is(":checked"))||($("#Wave_Front_ou").is(":checked")&&$("#Wave_Front_os").is(":checked")))
  {
    $("#wavefront_span").html("لا يمكن اختيار اكثر من نوع");
    return false;
  }
  else {
    $("#wavefront_span").html("");
    return true;
  }
}
function checkeye()
{
  if(($("#eye_od").is(":checked")&&$("#eye_os").is(":checked"))||($("#eye_od").is(":checked")&&$("#eye_ou").is(":checked"))||($("#eye_ou").is(":checked")&&$("#eye_os").is(":checked")))
  {
    $("#eye_span").html("لا يمكن اختيار اكثر من نوع");
    return false;
  }
  else {
    $("#eye_span").html("");
    return true;
  }
}
function checkanesthesia()
{
  if($("#general").is(":checked")&&$("#local").is(":checked"))
  {
    $("#anesthesia_span").html("لا يمكن اختيار اكثر من نوع");
    return false;
  }
  else {
    $("#anesthesia_span").html("");
    return true;
  }
}
