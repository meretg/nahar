function validateUsername() {

    var username =document.getElementById("member_name").value;
    //var illegalChars =  /"[^a-zA-Z0-9\s\u0600-\u06ff\u0750-\u077f\ufb50-\ufc3f\ufe70-\ufefc]"/; // allow letters, numbers, and underscores
     var illegalChars = /\W/;

    if (username ==="") {
        document.getElementById("member_nameinvalid").innerHTML="برجاء ادخال اسم الموظف";
        return false;
      }
    else if ((username.length > 20)) {
        document.getElementById("member_nameinvalid").innerHTML="اسم الموظف طويل جدا";
        return false;}

    else if (illegalChars.test(username.value))
     {
        document.getElementById("member_nameinvalid").innerHTML="اسم الموظف يجب الا يحتوي علي رموز او ارقام";
        return false;

    }
     else
     {

        document.getElementById("member_nameinvalid").innerHTML="";
        return true;

    }

}
function validatepassword()
{
  var password=document.getElementById("member_password").value;
  if (password.length < 8)
  {
       document.getElementById("member_passinvalid").innerHTML="الرقم السري يجب الا يقل عن 8 حروف";
       return false;
   }
  else if (password.search(/[a-z]/i) < 0)
  {
      document.getElementById("member_passinvalid").innerHTML="الرقم السري يجب احتوائه علي الاقل علي حرف ";
      return false;
   }
   else if (password.search(/[0-9]/) < 0)
   {
       document.getElementById("member_passinvalid").innerHTML="الرقم السري يجب احتوائه علي الاقل علي رقم ";
       return false;
   }
   else
   document.getElementById("member_passinvalid").innerHTML="";
   return true;
}
function validatephone_member()
{
   var pattern=new RegExp(/[^0-9]/);

   if(pattern.test($("#member_phone").val()))
   {
      $("#member_phoneinvalid").html('رقم غير صحيح');
return false;
   }
   else if($("#member_phone").val().length !== 11)
  {
     $("#member_phoneinvalid").html('رقم التليفون يجب ان يكون 11 رقم ');
return false;
  }

  else{
     $("#invalid_patient_phone").hide();
     return true;
  }
}
function validateNID_member()
{
   var pattern=new RegExp(/[^0-9]/);

   if(pattern.test($("#member_NID").val()))
   {
      $("#member_NIDinvalid").html('رقم غير صحيح');
return false;
   }
   else if($("#member_NID").val().length !== 14)
  {
     $("#member_NIDinvalid").html('رقم البطاقة يجب ان يكون 14 رقم ');
return false;
  }

  else{
     $("#invalid_patient_id").hide();
     return true;
  }
}
function validate_add()
{
  if(validateNID_member()==true && validatephone_member()==true && validatepassword()==true && validateUsername()==true )
  {
  return true;
}
  else
  {
  return false;
}
}
function oldvalidateUsername() {

    var username =document.getElementById("oldmember_name").value;
    //var illegalChars =  /"[^a-zA-Z0-9\s\u0600-\u06ff\u0750-\u077f\ufb50-\ufc3f\ufe70-\ufefc]"/; // allow letters, numbers, and underscores
     var illegalChars = /\W/;

    if (username ==="") {
        document.getElementById("oldmember_nameinvalid").innerHTML="برجاء ادخال اسم الموظف";
        return false;
      }
    else if ((username.length > 20)) {
        document.getElementById("oldmember_nameinvalid").innerHTML="اسم الموظف طويل جدا";
        return false;}

    else if (illegalChars.test(username.value))
     {
        document.getElementById("oldmember_nameinvalid").innerHTML="اسم الموظف يجب الا يحتوي علي رموز او ارقام";
        return false;

    }
     else
     {

        document.getElementById("oldmember_nameinvalid").innerHTML="";
        return true;

    }

}
function oldvalidatepassword()
{
  var password=document.getElementById("oldmember_password").value;
  if (password.length < 8)
  {
       document.getElementById("oldmember_passinvalid").innerHTML="الرقم السري يجب الا يقل عن 8 حروف";
       return false;
   }
  else if (password.search(/[a-z]/i) < 0)
  {
      document.getElementById("oldmember_passinvalid").innerHTML="الرقم السري يجب احتوائه علي الاقل علي حرف ";
      return false;
   }
   else if (password.search(/[0-9]/) < 0)
   {
       document.getElementById("oldmember_passinvalid").innerHTML="الرقم السري يجب احتوائه علي الاقل علي رقم ";
       return false;
   }
   else
   document.getElementById("oldmember_passinvalid").innerHTML="";
   return true;
}
function oldvalidatephone_member()
{
   var pattern=new RegExp(/[^0-9]/);

     if(pattern.test($("#oldmember_phone").val()))
   {
      $("#oldmember_phoneinvalid").html('رقم غير صحيح');
return false;
   }
   else if($("#oldmember_phone").val().length !== 11)
  {
     $("#oldmember_phoneinvalid").html('رقم التليفون يجب ان يكون 11 رقم ');
return false;
  }

  else{
     $("#oldinvalid_patient_phone").hide();
     return true;
  }
}
function oldvalidateNID_member()
{
   var pattern=new RegExp(/[^0-9]/);

   if(pattern.test($("#oldmember_NID").val()))
   {
      $("#oldmember_NIDinvalid").html('رقم غير صحيح');
return false;
   }
   else if($("#oldmember_NID").val().length !== 14)
  {
     $("#oldmember_NIDinvalid").html('رقم البطاقة يجب ان يكون 14 رقم ');
return false;
  }

  else{
     $("#oldinvalid_patient_id").hide();
     return true;
  }
}
function oldvalidate_add()
{
  if(oldvalidateNID_member()==true && oldvalidatephone_member()==true && oldvalidatepassword()==true && oldvalidateUsername()==true )
  {
  return true;
}
  else
  {
  return false;
}
}



function validatedocUsername() {

    var username =document.getElementById("doctor_name").value;
    //var illegalChars =  /"[^a-zA-Z0-9\s\u0600-\u06ff\u0750-\u077f\ufb50-\ufc3f\ufe70-\ufefc]"/; // allow letters, numbers, and underscores
     var illegalChars = /\W/;

    if (username ==="") {
        document.getElementById("doctor_nameinvalid").innerHTML="برجاء ادخال اسم الطبيب";
        return false;
      }
    else if ((username.length > 20)) {
        document.getElementById("doctor_nameinvalid").innerHTML="اسم الطبيب طويل جدا";
        return false;}

    else if (illegalChars.test(username.value))
     {
        document.getElementById("doctor_nameinvalid").innerHTML="اسم الطبيب يجب الا يحتوي علي رموز او ارقام";
        return false;

    }
     else
     {

        document.getElementById("doctor_nameinvalid").innerHTML="";
        return true;

    }

}
function validatedocpassword()
{
  var password=document.getElementById("doctor_password").value;
  if (password.length < 8)
  {
       document.getElementById("doctor_passinvalid").innerHTML="الرقم السري يجب الا يقل عن 8 حروف";
       return false;
   }
  else if (password.search(/[a-z]/i) < 0)
  {
      document.getElementById("doctor_passinvalid").innerHTML="الرقم السري يجب احتوائه علي الاقل علي حرف ";
      return false;
   }
   else if (password.search(/[0-9]/) < 0)
   {
       document.getElementById("doctor_passinvalid").innerHTML="الرقم السري يجب احتوائه علي الاقل علي رقم ";
       return false;
   }
   else
   document.getElementById("doctor_passinvalid").innerHTML="";
   return true;
}
function validatedocphone_member()
{
   var pattern=new RegExp(/[^0-9]/);

   if(pattern.test($("#doctor_phone").val()))
   {
      $("#doctor_phoneinvalid").html('رقم غير صحيح');
return false;
   }
   else if($("#doctor_phone").val().length !== 11)
  {
     $("#doctor_phoneinvalid").html('رقم التليفون يجب ان يكون 11 رقم ');
return false;
  }

  else{
     $("#doctor_phoneinvalid").hide();
     return true;
  }
}
function validatedocNID_member()
{
   var pattern=new RegExp(/[^0-9]/);

   if(pattern.test($("#doctor_NID").val()))
   {
      $("#doctor_NIDinvalid").html('رقم غير صحيح');
return false;
   }
   else if($("#doctor_NID").val().length !== 14)
  {
     $("#doctor_NIDinvalid").html('رقم البطاقة يجب ان يكون 14 رقم ');
return false;
  }

  else{
      $("#doctor_NIDinvalid").hide();
     return true;
  }
}
function validate_adddoc()
{
  if(validatedocNID_member()==true && validatedocphone_member()==true && validatedocpassword()==true && validatedocUsername()==true )
  {
  return true;
}
  else
  {
  return false;
}
}
function oldvalidateUsername() {

    var username =document.getElementById("oldmember_name").value;
    //var illegalChars =  /"[^a-zA-Z0-9\s\u0600-\u06ff\u0750-\u077f\ufb50-\ufc3f\ufe70-\ufefc]"/; // allow letters, numbers, and underscores
     var illegalChars = /\W/;

    if (username ==="") {
        document.getElementById("oldmember_nameinvalid").innerHTML="برجاء ادخال اسم الموظف";
        return false;
      }
    else if ((username.length > 20)) {
        document.getElementById("oldmember_nameinvalid").innerHTML="اسم الموظف طويل جدا";
        return false;}

    else if (illegalChars.test(username.value))
     {
        document.getElementById("oldmember_nameinvalid").innerHTML="اسم الموظف يجب الا يحتوي علي رموز او ارقام";
        return false;

    }
     else
     {

        document.getElementById("oldmember_nameinvalid").innerHTML="";
        return true;

    }

}
function oldvalidatepassword()
{
  var password=document.getElementById("oldmember_password").value;
  if (password.length < 8)
  {
       document.getElementById("oldmember_passinvalid").innerHTML="الرقم السري يجب الا يقل عن 8 حروف";
       return false;
   }
  else if (password.search(/[a-z]/i) < 0)
  {
      document.getElementById("oldmember_passinvalid").innerHTML="الرقم السري يجب احتوائه علي الاقل علي حرف ";
      return false;
   }
   else if (password.search(/[0-9]/) < 0)
   {
       document.getElementById("oldmember_passinvalid").innerHTML="الرقم السري يجب احتوائه علي الاقل علي رقم ";
       return false;
   }
   else
   document.getElementById("oldmember_passinvalid").innerHTML="";
   return true;
}
function oldvalidatephone_member()
{
   var pattern=new RegExp(/[^0-9]/);

     if(pattern.test($("#oldmember_phone").val()))
   {
      $("#oldmember_phoneinvalid").html('رقم غير صحيح');
return false;
   }
   else if($("#oldmember_phone").val().length !== 11)
  {
     $("#oldmember_phoneinvalid").html('رقم التليفون يجب ان يكون 11 رقم ');
return false;
  }

  else{
     $("#oldinvalid_patient_phone").hide();
     return true;
  }
}
function oldvalidateNID_member()
{
   var pattern=new RegExp(/[^0-9]/);

   if(pattern.test($("#oldmember_NID").val()))
   {
      $("#oldmember_NIDinvalid").html('رقم غير صحيح');
return false;
   }
   else if($("#oldmember_NID").val().length !== 14)
  {
     $("#oldmember_NIDinvalid").html('رقم البطاقة يجب ان يكون 14 رقم ');
return false;
  }

  else{
     $("#oldinvalid_patient_id").hide();
     return true;
  }
}
function oldvalidate_add()
{
  if(oldvalidateNID_member()==true && oldvalidatephone_member()==true && oldvalidatepassword()==true && oldvalidateUsername()==true )
  {
  return true;
}
  else
  {
  return false;
}
}
