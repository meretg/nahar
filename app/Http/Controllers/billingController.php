<?php

namespace App\Http\Controllers;
use App\patient;
use App\operation;
use App\examination;
use App\service;
use App\serviceName;
use App\code;
use App\store;
use App\permission;
use App\item;
use App\role;
use App\storeItem;
use App\Doctor;
use Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\receptionistController;
use Illuminate\Support\Facades\Session;

class billingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $x=0;
       $loginroles=array();
      if(!Auth::check()){
        Session::flash('message', 'تسجيل الدخول');
        return redirect ('login');
      }
     $permissions=permission::where('user_id',Auth::user()->id)->get();
    foreach ($permissions as $permission) {

   $role=role::findOrFail($permission->role_id);
     $loginroles[$x++]=$role;
      }
      $stores=store::where('id','>','1')->get();
      $i=0;
      $services=array();
      $services_confirmed=array();
        $services_array=service::all();
        $select=\Request::get('select');
        $data=\Request::get('data');
        if($select=="none"){
          Session::flash('error', 'يجب اختيار طريقه للبحث');
          return redirect ()->back();
        }
        elseif($select){
          $reception= new receptionistController();
           $patients=$reception->search($select,$data);
           foreach ($patients as $patient){
         $services_array=$patient->services;
         foreach ($services_array as $service)
         {
           $reception= new receptionistController();
           $patient=patient::findOrFail($service->patient_id);
           $examination=examination::where('service_id',$service->id)->first();
           $operation=operation::where('service_id',$service->id)->first();
           $service->age=$reception->date($patient->DOB);
           $service->patient=$patient;
           $service->operation=$operation;
           $service->examination=$examination;

           if($service->status=='pending'){
               $services[$i++]=$service;
           }
           if($service->status=='confirmed'&&$service->examinationType!='repeat'){
               $services_confirmed[$i++]=$service;
           }

         }
           }
          }

        else{
          foreach ($services_array as $service)
          {
            $reception= new receptionistController();
            $patient=patient::findOrFail($service->patient_id);
            $examination=examination::where('service_id',$service->id)->first();
            $operation=operation::where('service_id',$service->id)->first();
            $service->age=$reception->date($patient->DOB);
            $service->patient=$patient;
            $service->operation=$operation;
            $service->examination=$examination;

            if($service->status=='pending'){
                $services[$i++]=$service;
            }
            if($service->status=='confirmed'&&$service->examinationType!='repeat'){
                $services_confirmed[$i++]=$service;
            }

          }

        }


      // $services_confirmed=$this->sort($services_confirmed);


        return view('bill', [
            'services' => $services,
            'services_confirmed' => $services_confirmed,
            'stores' => $stores,
            'loginroles' => $loginroles,
        ]);
       //return json_encode($services);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
     // public function search($select,$data)
     // {
     //   $i=0;
     //   switch ($select) {
     // case "code":
     //     $patients_array=patient::where('code', 'like', '%'.$data.'%')->get();
     //     break;
     // case "name":
     //   $patients_array=patient::where('name','like', '%'.$data.'%')->get();
     //     break;
     // case "phone":
     //   $patients_array=patient::where('phone','like', '%'.$data.'%')->get();
     //     break;
     // case "NID":
     //     $patients_array=patient::where('NID','like', '%'.$data.'%')->get();
     //         break;
     //     default:
     //     Session::flash('error', 'يجب اختيار طريقه للبحث');
     //     return redirect ()->back();
     //       }
     //       foreach ($patients_array as $patient)
     //       {
     //         $patient->age=$this->date($patient->DOB);
     //         $patient->services=service::where('patient_id',$patient->id)->get();
     //         $patient->status = $this->getStatus($patient->services);
     //         $patient->total=$this->total($patient->services);
     //         $patients[$i++]=$patient;
     //       }
     //
     //      return $patients;
     //
     // }

    public function sort($services)
    {
      $i=0;
      $x=0;
      $servie_refunded=array();
      $servie_notrefunded=array();
      foreach($services as $service){

          if($service->operation->refund=='1'){
            $servie_refunded[$i++]=$service;
          }
          else{
            $servie_notrefunded[$x++]=$service;
          }


      }
       //$servie_refunded->merge($servie_notrefunded);
       return (array_merge($servie_notrefunded,$servie_refunded));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function returnMoney($operation){
      $service=service::findOrFail($operation->service_id);
      if($service->transformerDoc!='none'){
      $doctor=Doctor::where('docName',$service->transformerDoc)->first();
      $doctor->money-=$service->transformerDocMoney;
      $doctor->save();
    }
      if($service->examinerDoc!='none'){
      $doctor=Doctor::where('docName',$service->examinerDoc)->first();
      $doctor->money-=$service->examinerDocMoney;
      $doctor->save();
    }
      if($operation->surgeon!='none'){
        $doctor=Doctor::where('docName',$operation->surgeon)->first();
        $doctor->money-=$service->surgeonMoney;
        $doctor->save();
      }
      if($operation->assistantDoctor!='none'){
      $doctor=Doctor::where('docName',$operation->assistantDoctor)->first();
      $doctor->money-=$service->assistantDoctorMoney;
      $doctor->save();
      //return json_encode($doctor);
    }
    if($operation->anesthetist!='none'){
      $doctor=Doctor::where('docName',$operation->anesthetist)->first();
      $doctor->money-=$service->anesthetistMoney;
      $doctor->save();
    }
    if($operation->dataDoctor!='none'){
    $doctor=Doctor::where('docName',$operation->dataDoctor)->first();
    $doctor->money-=$service->dataDoctorMoney;
    $doctor->save();
  }
    }
    public function store(Request $request)
    {
        $i=0;
        $services=array();
        $services_confirmed=array();

        if($request->refund){
          if($request->operationId){
            $operation = operation::findOrFail($request->operationId);
            $operation->refund=1;
            $this->returnMoney($operation);
            $result = $operation->save();
          }
          elseif($request->examinationId){
            $examination = examination::findOrFail($request->examinationId);
            $examination->refund=1;
            $this->returnMoney($examination);
            $result = $examination->save();
          }

        if($result)
        {
            Session::flash('success', 'تم بنجاح');
            return redirect ()->back();
        }
        Session::flash('error', 'لم يتم اتمام العملية');
        return redirect ()->back();
      }

      $service=service::findOrFail($request->id);
      $operation=operation::where('service_id',$service->id)->first();


      // $sum=$request->transformerDocMoney+$request->commission+$request->examinerDocMoney
      // +$request->surgeonMoney+$request->anesthetistMoney+$request->assistantDoctorMoney;
      //
      // if($service->owedMoney!=$sum){
      //   Session::flash('error', 'النسب غير مساوية للمبلغ المراد دفع');
      //   return redirect ()->back();
      // }


      if($service->transformerDoc!='none'){
        $service->transformerDocMoney=$request->transformerDocMoney;
        $doctor=Doctor::where('docName',$service->transformerDoc)->first();
        $doctor->money+=$request->transformerDocMoney;
        $doctor->save();
      }

      //$service->commission=$request->commission;

    if($service->examinationType=='checkUp'||
    $service->examinationType=='normalExamination'||
    $service->examinationType=='lasikExamination'){


      if($service->examinerDoc!='none'){
        $service->examinerDocMoney=$request->examinerDocMoney;
        $doctor=Doctor::where('docName',$service->examinerDoc)->first();
        $doctor->money+=$request->examinerDocMoney;
        $doctor->save();
      }

      $service->save();

    }
      if($service->examinationType=='lasikOperation'){

         $service->surgeonMoney=$request->surgeonMoney;
          $service->dataDoctorMoney=$request->dataDoctorMoney;
         if($operation->surgeon!='none'){
           $doctor=Doctor::where('docName',$operation->surgeon)->first();
           $doctor->money+=$request->surgeonMoney;
           $doctor->save();

         }
         if($operation->dataDoctor!='none'){
           $doctor=Doctor::where('docName',$operation->dataDoctor)->first();
           $doctor->money+=$request->dataDoctorMoney;
           $doctor->save();

         }



         $service->save();


      }
      if($service->examinationType=='normalOperation'){

         $service->surgeonMoney=$request->surgeonMoney;
         if($operation->surgeon!='none'){
           $doctor=Doctor::where('docName',$operation->surgeon)->first();
           $doctor->money+=$request->surgeonMoney;
           $doctor->save();
         }


         $service->anesthetistMoney=$request->anesthetistMoney;
         if($operation->anesthetist!='none'){
         $doctor=Doctor::where('docName',$operation->anesthetist)->first();
         $doctor->money+=$request->anesthetistMoney;
         $doctor->save();
       }
         $service->assistantDoctorMoney=$request->assistantDoctorMoney;
          if($operation->assistantDoctor!='none'){
         $doctor=Doctor::where('docName',$operation->assistantDoctor)->first();
         $doctor->money+=$request->assistantDoctorMoney;
         $doctor->save();
       }
         $service->save();


      }
      $service->status='confirmed';
      $service->save();
      // $services_array=service::all();
      // foreach ($services_array as $service)
      // {
      //   $operation=operation::where('service_id',$service->id)->first();
      //   $service->operation=$operation;
      //
      //   if($service->status=='pending'){
      //       $services[$i++]=$service;
      //   }
      //   if($service->status=='confirmed'){
      //       $services_confirmed[$i++]=$service;
      //   }
      //
      // }

      return redirect()->action('billingController@index');

    //  return view('bill', [
    //        'services' => $services,
    //          'services_confirmed' => $services_confirmed,
    //    ]);
     //return json_encode($services_confirmed);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function check(Request $request){
       $i=0;
      $operation=operation::where('service_id',$request->id)->first();
      $store=storeItem::where('store_id',$request->store_id)->first();
      $errors=array();
      $response=new \stdClass;
        $operation->items=$request->quantity;
      $items=json_encode($request->quantity);

      foreach(json_decode($items) as $object){

       $item=item::findOrFail($object->id);
       if($store->storeQuantity < $object->quantity&& $object->quantity!=0){
          $errors[$i++]='الكميه المدخله فى  ' .$item->name. ' اكثر من الموجوده فى المخزن' ;

         }
       }
       if(sizeof($errors)>0){
         $response->errors=$errors;
         return json_encode($response);
       }
      else{
         $message=$this->saveItems($items,$request->id,$request->total,$request->store_id,$request->discount);
         $response->message=$message;
         return json_encode($response);
       }


     }
    public function saveItems($items,$id,$total,$store_id,$discount)
    {
      $service=service::findOrFail($id);
      $operation=operation::where('service_id',$id)->first();
      $storeiet=storeItem::where('store_id',$store_id)->get();
      $message='تم اضافة مستلكات للعملية وانقاصها من المخزن';
       $operation->items=$items;
       $operation->loss=$total;
       $operation->discount=$discount;
       $operation->storeIdLoss=$store_id;
       $operation->save();
       switch ($discount) {
         case 'الطبيب الجراح':
         $doctor=Doctor::where('docName',$operation->surgeon)->first();
         $doctor->loss+=$total;
         $doctor->save();
        $service->surgeonMoney-=$total;
        $service->commission+=$total;
         $service->save();
           break;
           case 'المساعد':
           $doctor=Doctor::where('docName',$operation->assistantDoctor)->first();
           $doctor->loss+=$total;
           $doctor->save();
           $service->assistantDoctorMoney-=$total;
            $service->commission+=$total;
            $service->save();
             break;
             case 'الطبيب المحول':
             $doctor=Doctor::where('docName',$service->transformerDoc)->first();
             $doctor->loss+=$total;
             $doctor->save();
             $service->transformerDocMoney-=$total;
              $service->commission+=$total;
              $service->save();
               break;
               case 'طبيب التخدير':
               $doctor=Doctor::where('docName',$operation->transformerDoc)->first();
               $doctor->loss+=$total;
               $doctor->save();
                $service->anesthetistMoney-=$total;
                $service->commission+=$total;
                 $service->save();
                 break;

       }
       foreach(json_decode($items) as $object){
          $store=storeItem::where('store_id','=',$store_id)->where('item_id','=',$object->id)->first();
          //return $store;
          $store->storeQuantityLoss+=$object->quantity;
          $store->storeQuantity-=$object->quantity;
          $store->save();
          //$item->save();
        }
       return $message;
        }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function showItems(Request $request)
       {

         $i=0;
         $items=array();
         $response=new \stdClass;
           $operation=operation::where('service_id',$request->id)->first();
           $objects=json_decode($operation->items);
           $total=$operation->loss;

           foreach($objects as $item){
             if($item->quantity>0){
               $object=item::findOrFail($item->id);
               $object->quantity=$item->quantity;
               //$item->item=$object;
               $items[$i++]=$object;
             }
           }
   $response->items=$items;
   $response->total=$total;
     return json_encode ($response);
       }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
