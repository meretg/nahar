<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\patient;
use App\operation;
use App\examination;
use App\service;
use App\serviceName;
use App\code;
use App\User;
use App\role;
use App\permission;
use Storage;
use App\Doctor;
use App\type;
use Auth;
use  Carbon;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use View;
use Redirect;
use Validator;


class receptionistController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $z=0;
      $loginroles=array();
     if(!Auth::check()){
       return redirect ('login');
     }

    $permissions=permission::where('user_id',Auth::user()->id)->get();
   foreach ($permissions as $permission) {

  $role=role::findOrFail($permission->role_id);
    $loginroles[$z++]=$role;
     }
      $i=0;
      $patients=array();
        $patients_array=patient::all();
        $select=\Request::get('select');
        $data=\Request::get('data');
        $from=\Request::get('from');
        $to=\Request::get('to');
        $filter=\Request::get('filter');
        foreach ($patients_array as $patient)
        {
          $patient->age=$this->date($patient->DOB);
          $patient->services=service::where('patient_id',$patient->id)->get();
          $patient->status = $this->getStatus($patient->services);
          $patient->total=$this->total($patient->services);
          $patients[$i++]=$patient;
        }
        if($filter==1){
           $patients=$this->filter($from,$to);
        }
        // else{
        //   Session::flash('error', 'برجاء ادخال التاريخ الاخر');
        //   return redirect ()->back();
        // }
        elseif($select){
           $patients=$this->search($select,$data);
           // return json_encode($patients);
          }
        elseif($select=="none"){
          Session::flash('error', 'يجب اختيار طريقه للبحث');
          return redirect ()->back();
        }

        //else{
          return view('patient', [
             'patients' => $patients,
             'loginroles' => $loginroles,
         ]);
        //}


    //

    }
    public function filter($from, $to)
    {
        $i=0;
      $patients_array=array();
      $patients=array();
      $patients_array=patient::whereBetween('created_at',[$from, $to])->get();
      $services=service::whereBetween('created_at',[$from, $to])->get();
      foreach ($services as $service) {
        $x=0;
       $patient=patient::findOrFail($service->patient_id);
       $patients_array[$x++]=$patient;
      }
      foreach ($patients_array as $patient)
      {
        $patient->age=$this->date($patient->DOB);
        $patient->services=service::where('patient_id',$patient->id)->get();
        $patient->status = $this->getStatus($patient->services);
        $patient->total=$this->total($patient->services);
        $patients[$i++]=$patient;
      }
      return $patients;
    }
    public function search($select,$data)
    {
      $i=0;
      $patients=array();
      $patients_array=array();
      switch ($select) {
    case "code":
        $patients_array=patient::where('code', 'like', '%'.$data.'%')->get();
        break;
    case "name":
      $patients_array=patient::where('name','like', '%'.$data.'%')->get();
        break;
    case "phone":
      $patients_array=patient::where('phone','like', '%'.$data.'%')->get();
        break;
    case "NID":
        $patients_array=patient::where('NID','like', '%'.$data.'%')->get();
            break;
    case"none":
        Session::flash('error', 'يجب اختيار طريقه للبحث');
        return redirect ()->back();
          }
          foreach ($patients_array as $patient)
          {
            $patient->age=$this->date($patient->DOB);
            $patient->services=service::where('patient_id',$patient->id)->get();
            $patient->status = $this->getStatus($patient->services);
            $patient->total=$this->total($patient->services);
            $patients[$i++]=$patient;
          }

         return $patients;

    }


    public function getStatus($patientServices)
    {
      $status = 'confirmed';
        foreach ($patientServices as $service)
        {
            if ($service->status == 'pending')
            {
              $status = 'pending';
              break;
            }
        }
        return $status;
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
      public function total($patientServices){
        $total=0;
        foreach($patientServices as $service){
          $refund=operation::where('service_id',$service->id)->pluck('refund')->max();
          if($service->status=='confirmed'){
            $total+=$service->owedMoney;

          }
          if($refund == '1'){

              $total-=$service->owedMoney;
          }

        }
 return $total;
      }
   public function date($DOB)
   {
     $mytime = Carbon\Carbon::now();
     $patient_date= Carbon\Carbon::createFromFormat('Y-m-d',$DOB)->year;
     $age=$mytime->year-$patient_date;
     return json_encode($age);
   }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $validator = Validator::make($request->all(), [
       'code' => 'required|unique:patients,code',
        'unique'=> 'unique:patients,unique',
        'phone'=> 'required:patients,phone',
     ]);
//      $validator->after(function($validator) {
//
//      // $validator->errors()->merge('كود المريض تم تسجيله من قبل');
//      // $validator->errors()->merge( 'رقم البطاقه موجود من قب');
//
// });
     if ($validator->fails()){


        return redirect()->back()->withErrors($validator);
      // Session::flash('error',$validator );
      //  return redirect ()->back();

        }


      $patient= new patient;
      $patient->name=$request->name;
      $patient->code=$request->code;
      $patient->NID=$request->NID;
      $patient->phone=$request->phone;
      $patient->address=$request->address;
      $patient->DOB=$request->DOB;
      if($this->date($request->DOB)<18){
        $patient->parentName=$request->parentName;
        $patient->parentPhone=$request->parentPhone;
      }

      $patient->save();
      $service= new service;
      if($request->examinerDoc){
        $service->examinerDoc=$request->examinerDoc;
      }


      $service->owedMoney=$request->owedMoney;



      if($request->transformerDoc){
        $service->transformerDoc=$request->transformerDoc;
        $check=Doctor::where('docName',$request->transformerDoc)->first();
        if(!$check){
          $newDoctor=new Doctor;
          $newDoctor->docName=$request->transformerDoc;
          $newDoctor->save();
        }

      }
      $service->patient_id=$patient->id;
      $service->examinationType=$request->examinationType;
      $service->eye=$request->eye;
      $service->save();

      if($request->examinationType=='normalExamination'){
        $examination= new examination;

        $patient->companionName=$request->companionName;
        $patient->companionPhone=$request->companionPhone;
        $patient->save();
        if(!$request->other){
          $examination->diagonistics=$request->diagonistics;



        }
        else{
            $examination->other=$request->other;
        }


        $examination->service_id=$service->id;

        $examination->save();
        $commission=serviceName::where('name',$examination->diagonistics)->first();
      }

      if($request->examinationType=='lasikExamination'){
        $examination= new examination;
        //$patient->examinerDoc=$request->examinerDoc_la;
        $patient->companionName=$request->companionName;
        $patient->companionPhone=$request->companionPhone;
        $patient->save();
        $examination->investigation=$request->investigation;

        $examination->service_id=$service->id;
        $examination->save();
        $commission=serviceName::where('name',$examination->investigation)->first();
      }

      if($request->examinationType=='normalOperation'){
        $operation= new operation;
        $patient->companionName=$request->companionName;
        $patient->companionPhone=$request->companionPhone;
        $patient->save();
        $operation->surgeon=$request->surgeon_op;
        $operation->anesthetist=$request->anesthetist;
        $operation->assistantDoctor=$request->assistantDoctor;

        $operation->anesthesia=$request->anesthesia;
        $operation->operation=$request->operation;
        $operation->service_id=$service->id;
        $operation->save();
        $commission=serviceName::where('name',$operation->operation)->first();


      }
      if($request->examinationType=='lasikOperation'){
        $patient->companionName=$request->companionName;
        $patient->companionPhone=$request->companionPhone;
        $patient->save();
        $operation= new operation;

        $operation->surgeon=$request->surgeon_la;
        $operation->dataDoctor=$request->dataDoctor;
        $operation->treatment=$request->treatment;
        $operation->service_id=$service->id;
        $operation->save();
        $commission=serviceName::where('name',$operation->treatment)->first();


      }
      if($request->examinationType=='checkup'){
        $commission=serviceName::where('name','checkUp')->first();
      }
      if($request->examinationType=='repeat'){
        $commission=serviceName::where('name','repeat')->first();
      }
      $service->commission=$commission->commission;
      $service->save();
        Session::flash('message', 'تم تسجيل المريض بنجاح');
        return redirect ()->back();
      //  return json_encode($commission);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $services=service::where('patient_id',$request->id)->get();

        foreach($services as $service){
        if($service->examinationType=='normalExamination'){
          $examination=examination::where('service_id',$service->id)->first();
          $service->details=$examination;
        }
        if($service->examinationType=='lasikExamination'){
          $examination=examination::where('service_id',$service->id)->first();
          $service->details=$examination;

        }
        if($service->examinationType=='normalOperation'){
          $operation=operation::where('service_id',$service->id)->first();
          $service->details=$operation;

        }
        if($service->examinationType=='lasikOperation'){
          $operation=operation::where('service_id',$service->id)->first();
          $service->details=$operation;

        }
      }
        return json_encode($services);
      //  Response::json(array('services'=>$services,'patient'=>$patient));
    }
  public function view(Request $request){
      $patient=patient::findOrFail($request->id);

      return json_encode($patient);
  }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function send(Request $request)
     {
       $surgeonDoctors=Doctor::whereIn('id',type::where('type_id',1)->pluck('doctor_id'))->get();
       $anesthetistDoctors=Doctor::whereIn('id',type::where('type_id',3)->pluck('doctor_id'))->get();
       $assistantDoctors=Doctor::whereIn('id',type::where('type_id',4)->pluck('doctor_id'))->get();
       $checkupDoctors=Doctor::whereIn('id',type::where('type_id',2)->pluck('doctor_id'))->get();
       $dataDoctors=Doctor::whereIn('id',type::where('type_id',5)->pluck('doctor_id'))->get();

     $id=$request->session()->put('patient_id', $request->id);
     $patient=patient::findOrFail($request->id);
    // $code=$request->session()->put('code', $request->id);
    $code=$patient->code;
     return view('adddetails', ['code' =>$code,
     'surgeonDoctors'=>$surgeonDoctors,
     'assistantDoctors'=>$assistantDoctors,
     'anesthetistDoctors'=>$anesthetistDoctors,
     'checkupDoctors'=>$checkupDoctors,
     'dataDoctors'=>$dataDoctors,]);
          //return view('adddetails')->with($code);
     }


    public function edit(Request $request)
    {
      $id=$request->session()->pull('patient_id');
      $patient=patient::findOrFail($id);
      $service= new service;

      if($request->examinerDoc){
        $service->examinerDoc=$request->examinerDoc;
      }
      if($request->examinationType=='repeat'){
        $service->status='confirmed';

      }
      $service->owedMoney=$request->owedMoney;

      if($request->transformerDoc){
        $service->transformerDoc=$request->transformerDoc;
        $check=Doctor::where('docName',$request->transformerDoc)->first();
        if(!$check){
          $newDoctor=new Doctor;
          $newDoctor->docName=$request->transformerDoc;
          $newDoctor->save();
        }

      }

      $service->patient_id=$patient->id;
      $service->examinationType=$request->examinationType;
      $service->eye=$request->eye;
      $service->save();
      if($request->examinationType=='normalExamination'){
        $examination= new examination;
        //$patient->examinerDoc=$request->examinerDoc_n;
        $patient->companionName=$request->companionName;
        $patient->companionPhone=$request->companionPhone;
        $patient->save();
        if(!$request->other){
          $examination->diagonistics=$request->diagonistics;
        }
        else{
            $examination->other=$request->other;
        }
        $examination->service_id=$service->id;
        $examination->save();
        $commission=serviceName::where('name',$examination->diagonistics)->first();
      }

      if($request->examinationType=='lasikExamination'){
        $code=new code;
        $code->canceled_code=$patient->code;
        $code->patient_id=$patient->id;
        $code->save();
        $patient->code=$request->code;
        $patient->companionName=$request->companionName;
        $patient->companionPhone=$request->companionPhone;
        $patient->save();

        $examination= new examination;
        $examination->investigation=$request->investigation;

        $examination->service_id=$service->id;
        $examination->save();
        $commission=serviceName::where('name',$examination->investigation)->first();

      }
      if($request->examinationType=='normalOperation'){
        $operation= new operation;
        $patient->companionName=$request->companionName;
        $patient->companionPhone=$request->companionPhone;
        $patient->save();
        $operation->surgeon=$request->surgeon_op;
        $operation->anesthetist=$request->anesthetist;
        $operation->assistantDoctor=$request->assistantDoctor;

        $operation->anesthesia=$request->anesthesia;
        $operation->operation=$request->operation;
        $operation->service_id=$service->id;
        $operation->save();
        $commission=serviceName::where('name',$operation->operation)->first();

      }
      if($request->examinationType=='lasikOperation'){
        $code=new code;
        $code->canceled_code=$patient->code;
        $code->patient_id=$patient->id;
        $code->save();
        $patient->code=$request->code;
        $patient->companionName=$request->companionName;
        $patient->companionPhone=$request->companionPhone;
        $patient->save();
        $operation= new operation;
        $operation->surgeon=$request->surgeon_la;
        $operation->dataDoctor=$request->dataDoctor;
        $operation->treatment=$request->treatment;
        $operation->service_id=$service->id;
        $operation->save();
        $commission=serviceName::where('name',$operation->treatment)->first();




}
if($request->examinationType=='checkup'){
  $commission=serviceName::where('name','checkUp')->first();
}
if($request->examinationType=='repeat'){
  $commission=serviceName::where('name','repeat')->first();
}
$service->commission=$commission->commission;
$service->save();
//return json_encode($request->treatment);
 return redirect('receptionist');

}

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

}
