<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\item;
use App\store;
use App\storeItem;
use App\User;
use App\role;
use App\permission;
use Auth;
use Illuminate\Support\Facades\Session;
class StoreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $x=0;
      $loginroles=array();
     if(!Auth::check()&&$permission){
       return redirect ('login');
     }

    $permissions=permission::where('user_id',Auth::user()->id)->get();
   foreach ($permissions as $permission) {

  $role=role::findOrFail($permission->role_id);
    $loginroles[$x++]=$role;
     }
        $stores=store::all();
        $items=item::all();
        //$items=item::where('store_id',1)->get();
        //$store=store::findOrFail(1);
        foreach ($items as $item) {
          $store=storeItem::where('item_id',$item->id)->first();
          // $item->quantity+=$store->storeQuantity;
          // $item->save();
        }
        if($store=store::first()){

            $storeName=$store->name;
        }
        else{
           $storeName="";
        }

        return view('store', [
            'stores' => $stores,
            'items' => $items,
            'storeName'=>$storeName,
            'loginroles' => $loginroles,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      if($request->item_id){
        $item=item::findOrFail($request->item_id);
        $item->quantity+=$request->quantity;
        $item->save();
        Session::flash('message','تم اضافه كميه للعنصر'.$item->name);
      }
      //add new item
      elseif($request->id){
        $item=new item();
        $item->name=$request->name;
        $item->description=$request->description;
        $item->quantity=$request->quantity;
        $item->price=$request->price;
        $item->limitQuantity=$request->limitQuantity;
        $item->buyPrice=$request->buyPrice;
        $item->save();
        Session::flash('message','تم اضافه عنصر');
      }
      //add new store
      else{
        $store=new store();
        $store->name=$request->name;
        $store->description=$request->description;
        $store->save();
        Session::flash('message', 'تم اضافه مخزن');
      }
      //add item quantity


        return redirect()->action('StoreController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function select(Request $request)
      {
        $i=0;
        $items=array();
        $store=store::findOrFail($request->id);

        if($request->id ==1){
          $items=item::all();
        }
        else{
          $storeitems=storeItem::where('store_id',$request->id)->get();
          foreach ($storeitems as $item) {
            $object=item::findOrFail($item->item_id);
            $object->storeQuantity=$item->storeQuantity;
            $items[$i++]=$object;
          }

        }
        $title=$store->name;
        $response=new \stdClass;

        $response->title=$title;
        $response->id=$request->id;
        $response->items=$items;
        //$response->storeQuantity=$store->storeQuantity;

       return json_encode($response);
      //Response::json(array('items'=>$items,'title'=>$title));
      }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function transform(Request $request)
    {
      $store=store::findOrFail($request->store_id);
      $newItem=storeItem::where('item_id',$request->item_id)->where('store_id',$request->store_id)->first();
      if($request->oldStore_id==1){
        $oldStore=item::findOrFail($request->item_id);
          if($request->storeQuantity>$oldStore->quantity){
            Session::flash('error', 'الكميه الموجوده فى المخزن الرئيسى غير كافيه');
           return redirect()->action('StoreController@index');
          }
          else{
            $oldStore->quantity-=$request->storeQuantity;
            $oldStore->save();
          }

      }
      else{
        $oldStore=storeItem::where('item_id',$request->item_id)->where('store_id',$request->oldStore_id)->first();
        // if($request->storeQuantity>$oldStore->storeQuantity){
        //   Session::flash('error', 'الكميه الموجوده فى المخزن الرئيسى غير كافيه');
        //  return redirect()->action('StoreController@index');
        // }
        // else{
          $oldStore->storeQuantity-=$request->storeQuantity;
          $oldStore->save();
        //}

      }
      if($request->store_id==1){
        $newStore=item::findOrFail($request->item_id);
        $newStore->quantity+=$request->storeQuantity;
        $newStore->save();
      }
      if($newItem){
        $newItem->item_id=$request->item_id;
        $newItem->store_id=$request->store_id;
        $newItem->storeQuantity+=$request->storeQuantity;
        $newItem->save();
      }
      else{
        $newItem=new storeItem();
        $newItem->item_id=$request->item_id;
        $newItem->store_id=$request->store_id;
        $newItem->storeQuantity+=$request->storeQuantity;
        $newItem->save();
      }


        //  Session::flash('message', $store->name. 'تم نقل العنصر للمخزن الفرعى');
//        }
        // else{

        // }

      return redirect()->action('StoreController@index');
      }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
      $item=item::findOrFail($request->id);
      if($request->ajax()){
        return json_encode($item);
      }
       else{
        $item->name=$request->name;
        $item->description=$request->description;
        $item->quantity=$request->quantity;
        $item->price=$request->price;
        $item->limitQuantity=$request->limitQuantity;
        $item->buyPrice=$request->buyPrice;
        $item->save();
          return redirect()->action('StoreController@index');
      }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
      $item=item::findOrFail($request->id);
      $item->delete();
      Session::flash('message', 'تم حذف العنصر');
      return redirect()->action('StoreController@index');

    }
}
