<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\operation;
use App\examination;
use App\service;
use App\Doctor;
use App\type;
use App\doctorType;
use App\store;
use App\item;
use App\storeItem;
use Auth;

use Barryvdh\DomPDF\Facade as PDF;
use App\User;
class reportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     // public function index(){
     //   return view('reports');
     // }
    public function index()
    {
      // if(!Auth::check()){
      //   return redirect ('login');
      // }(
      $stores=store::all();
      $items=item::all();
      $doctors=Doctor::all();
        $examinations=new \stdClass;
        $operations=new \stdClass;
        $checkups=new \stdClass;
        $billings=new \stdClass;
        $from=\Request::get('from');
        $to=\Request::get('to');
        $filter=\Request::get('select');
        $date=\Request::get('filter');
        $store_id=\Request::get('storeId');

      //$checkups=service::where('examinationType','checkUp')->get();
      $lasikOperationNum=service::whereBetween('created_at',[$from, $to])->where('examinationType','lasikOperation')->where('serviceStatus','confirmed')->count();
      $lasikExaminationNum=service::whereBetween('created_at',[$from, $to])->where('examinationType','lasikExamination')->where('serviceStatus','confirmed')->count();
      $normalExaminationNum=service::whereBetween('created_at',[$from, $to])->where('examinationType','normalExamination')->where('serviceStatus','confirmed')->count();
      $normalOperationNum=service::whereBetween('created_at',[$from, $to])->where('examinationType','normalOperation')->where('serviceStatus','confirmed')->count();
      $checkupNum=service::whereBetween('created_at',[$from, $to])->where('examinationType','checkUp')->where('status','confirmed')->count();
      $lasikOperationEyeNum=service::whereBetween('created_at',[$from, $to])->where('examinationType','lasikOperation')->where('eye','OU')->where('serviceStatus','confirmed')->count();
      $lasikOperationEyeNum*=2;
      $lasikOperationEyeNum+=service::whereBetween('created_at',[$from, $to])->where('examinationType','lasikOperation')->whereIn('eye', ['OS', 'OD'])->where('serviceStatus','confirmed')->count();
      $lasikExaminationEyeNum=service::whereBetween('created_at',[$from, $to])->where('examinationType','lasikExamination')->where('eye','OU')->where('serviceStatus','confirmed')->count();
      $lasikExaminationEyeNum*=2;
      $lasikExaminationEyeNum+=service::whereBetween('created_at',[$from, $to])->where('examinationType','lasikExamination')->whereIn('eye', ['OS', 'OD'])->where('serviceStatus','confirmed')->count();
      $normalExaminationEyeNum=service::whereBetween('created_at',[$from, $to])->where('examinationType','normalExamination')->where('eye','OU')->where('serviceStatus','confirmed')->count();
      $normalExaminationEyeNum*=2;
      $normalExaminationEyeNum+=service::whereBetween('created_at',[$from, $to])->where('examinationType','normalExamination')->whereIn('eye', ['OS', 'OD'])->where('serviceStatus','confirmed')->count();
      $normalOperationEyeNum=service::whereBetween('created_at',[$from, $to])->where('examinationType','normalOperation')->where('eye','OU')->where('serviceStatus','confirmed')->count();
      $normalOperationEyeNum*=2;
      $normalOperationEyeNum+=service::whereBetween('created_at',[$from, $to])->where('examinationType','normalOperation')->whereIn('eye', ['OS', 'OD'])->where('serviceStatus','confirmed')->count();

      $checkupEyeNum=service::whereBetween('created_at',[$from, $to])->where('examinationType','checkUp')->where('eye','OU')->where('status','confirmed')->count();
      $checkupEyeNum*=2;
      $checkupEyeNum+=service::whereBetween('created_at',[$from, $to])->where('examinationType','checkUp')->whereIn('eye', ['OS', 'OD'])->where('status','confirmed')->count();
      $examinations->lasik=$lasikExaminationNum;
      $examinations->normal=$normalExaminationNum;
      $examinations->normalEye=$normalExaminationEyeNum;
      $examinations->lasikEye=$lasikExaminationEyeNum;
      $operations->lasik=$lasikOperationNum;
      $operations->normal=$normalOperationNum;
      $operations->lasikEye=$lasikOperationEyeNum;
      $operations->normalEye=$normalOperationEyeNum;
      $checkups->checkup=$checkupNum;
      $checkups->Eye=$checkupEyeNum;
      $lasikOperationMoney=service::whereBetween('created_at',[$from, $to])->where('examinationType','lasikOperation')->where('serviceStatus','confirmed')->sum('owedMoney');
      $lasikExaminationMoney=service::whereBetween('created_at',[$from, $to])->where('examinationType','lasikExamination')->where('serviceStatus','confirmed')->sum('owedMoney');
      $normalExaminationMoney=service::whereBetween('created_at',[$from, $to])->where('examinationType','normalExamination')->where('serviceStatus','confirmed')->sum('owedMoney');
      $normalOperationMoney=service::whereBetween('created_at',[$from, $to])->where('examinationType','normalOperation')->where('serviceStatus','confirmed')->sum('owedMoney');
      $checkupMoney=service::whereBetween('created_at',[$from, $to])->where('examinationType','checkUp')->where('status','confirmed')->sum('owedMoney');
      $billings->lasikOperation=$lasikOperationMoney;
      $billings->lasikExamination=$lasikExaminationMoney;
      $billings->normalExamination=$normalExaminationMoney;
      $billings->normalOperation=$normalOperationMoney;
      $billings->checkup=$checkupMoney;
      $services=service::whereBetween('updated_at',[$from, $to])->pluck('id');
      $examinerDoctors=service::whereIn('id',$services)->pluck('examinerDoc');
      $surgeonDoctors=operation::whereIn('service_id',$services)->pluck('surgeon');
      $assistantDoctors=operation::whereIn('service_id',$services)->pluck('assistantDoctor');
      $anesthetistDoctors=operation::whereIn('service_id',$services)->pluck('anesthetist');
      $dataDoctors=operation::whereIn('service_id',$services)->pluck('dataDoctor');


  foreach ($doctors as $doctor) {
    $doctor->afterLoss=$doctor->money-$doctor->loss;
  }
  if($filter){

    switch ($filter) {
      case '1':
          $doctors=$surgeonDoctors;
      break;
      case '2':
          $doctors=$examinerDoctors;
        break;
      case '3':
          $doctors=$anesthetistDoctors;
          break;
      case '4':
          $doctors=$assistantDoctors;
          break;
     case '5':
          $doctors=$dataDoctors;
              break;

      default:
        // code...
        break;
    }
    foreach ($doctors as $doctor) {
      $doctor->afterLoss=$doctor->money-$doctor->loss;
    }
     return json_encode($doctors);

  }

  if($store_id){
    if($store_id==1){
      $items=item::all();
      foreach ($items as $item) {
      $item->storeQuantityLoss=0;
      $item->storeQuantity=$item->quantity;
      }

      return json_encode($items);
    }
    $items=storeItem::where('store_id',$store_id)->get();
    foreach ($items as $item) {
    $item->name=item::where('id',$item->item_id)->pluck('name');
    }

    return json_encode($items);

  }

      return view('reports', [
         'billings' => $billings,
         'examinations' => $examinations,
         'operations' => $operations,
         'checkups' => $checkups,
         'doctors'=>$doctors,
         'stores'=>$stores,
         'items'=>$items,

     ]);
     // return json_encode($doctors->examin_doctors);
         }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function find($filter,$from,$to)
    {
      $doctors_name=Doctor::whereIn('id',type::where('type_id',$filter)->pluck('doctor_id'))->pluck('docName');
      $services=service::whereBetween('updated_at',[$from, $to])->pluck('id');
    switch ($filter) {
      case '1':
          $surgeonDoctors=operation::whereIn('service_id',$services)->get();
          return json_encode($surgeonDoctors);
      break;
      case '2':
            // code...
        break;
      case '3':
              // code...
          break;
      case '4':
                // code...
          break;
     case '5':
                    // code...
              break;

      default:
        // code...
        break;
    }

    }
    // return json_encode($assistant_doctors);



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function doctor(Request $request)
    {
      $services=service::all();
      $doctors=Doctor::all();
        foreach ($doctors as $doctor) {
      foreach ($services as $service) {
        $doctor_money=service::groupBy('transformerDoc')->sum('transformerDocMoney');

      }
    }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
