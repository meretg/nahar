<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\role;
use App\Doctor;
use App\type;
use App\permission;
use Auth;
class receptionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $x=0;
      $loginroles=array();
      $surgeonDoctors=Doctor::whereIn('id',type::where('type_id',1)->pluck('doctor_id'))->get();
      $anesthetistDoctors=Doctor::whereIn('id',type::where('type_id',3)->pluck('doctor_id'))->get();
      $assistantDoctors=Doctor::whereIn('id',type::where('type_id',4)->pluck('doctor_id'))->get();
      $checkupDoctors=Doctor::whereIn('id',type::where('type_id',2)->pluck('doctor_id'))->get();
      $dataDoctors=Doctor::whereIn('id',type::where('type_id',5)->pluck('doctor_id'))->get();
      $permissions=permission::where('user_id',Auth::user()->id)->get();
     foreach ($permissions as $permission) {
      $role=role::findOrFail($permission->role_id);
      $loginroles[$x++]=$role;
       }
       $code=rand(1000, 9999);
       return view('index', [
           'loginroles' => $loginroles,
           'surgeonDoctors'=>$surgeonDoctors,
           'assistantDoctors'=>$assistantDoctors,
           'anesthetistDoctors'=>$anesthetistDoctors,
           'checkupDoctors'=>$checkupDoctors,
           'dataDoctors'=>$dataDoctors,
           'code'=>$code,

       ]);
       //return json_encode($dataDoctors);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
