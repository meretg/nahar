<?php

namespace App\Http\Controllers;
use App\patient;
use App\operation;
use App\examination;
use App\service;
use App\permission;
use App\role;
use Storage;
use Auth;
use  Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Input;

class examinationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $z=0;
      $loginroles=array();
     if(!Auth::check()&&$permission){
       return redirect ('login');
     }

    $permissions=permission::where('user_id',Auth::user()->id)->get();
   foreach ($permissions as $permission) {

  $role=role::findOrFail($permission->role_id);
    $loginroles[$z++]=$role;
     }
      $i=0;
      $x=0;
      $y=0;
      $services=array();
      $services_confirmed=array();
      $services_rejected=array();
      $services_array=service::where('examinationType','normalExamination')
        ->orwhere('examinationType','lasikExamination')->get();
        foreach ($services_array as $service)
        {
          $reception= new receptionistController();
          $patient=patient::findOrFail($service->patient_id);

          $examination=examination::where('service_id',$service->id)->first();
          $service->age=$reception->date($patient->DOB);
        $service->patient=$patient;
        $service->examination=$examination;

        if($service->status=='confirmed'){
          if($service->serviceStatus=='pending'){
              $services[$i++]=$service;
          }
          if($service->serviceStatus=='confirmed'){
              $services_confirmed[$x++]=$service;
          }
          if($service->serviceStatus=='rejected'){
              $services_rejected[$y++]=$service;
          }
        }

        }

        return view('examinations', [
            'services' => $services,
            'services_confirmed' => $services_confirmed,
            'services_rejected' => $services_rejected,
              'loginroles' => $loginroles,
        ]);
        // return json_encode($services_array);


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $service=service::findOrFail($request->id);
      if($request->reason){
        $examination=examination::where('service_id',$request->id)->first();
        $examination->reason=$request->reason;
        $service->serviceStatus='rejected';
        $examination->save();
        $service->save();

      }
      else{

      $service->serviceStatus='confirmed';
      $service->updated_at= Carbon\Carbon::now();
      $service->save();
    }
      // $i=0;
      // $services=array();
      // $services_confirmed=array();
      //   $services_rejected=array();
      //   $services_array=service::where('examinationType','normalExamination')
      //   ->orwhere('examinationType','lasikExamination')->get();
      //   foreach ($services_array as $service)
      //   {
      //     $reception= new receptionistController();
      //     $patient=patient::findOrFail($service->patient_id);
      //
      //     $examination=examination::where('service_id',$service->id)->first();
      //     $service->age=$reception->date($patient->DOB);
      //   $service->patient=$patient;
      //   $service->examination=$examination;
      // if($service->status=='confirmed'){
      //   if($service->serviceStatus=='pending'){
      //       $services[$i++]=$service;
      //   }
      //   if($service->serviceStatus=='confirmed'){
      //       $services_confirmed[$i++]=$service;
      //   }
      //   if($service->serviceStatus=='rejected'){
      //       $services_confirmed[$i++]=$service;
      //   }
      // }
      //
      //
      //   }

        // return view('examinations', [
        //     'services' => $services,
        //     'services_confirmed' => $services_confirmed,
        // ]);
        return redirect()->action('examinationController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request)
    {
      $examination=examination::where('service_id',$request->id)->first();
        if($request->file){
          Storage::put(
             $request->file,
            file_get_contents($request->file('file')->getRealPath())
                    );
     $examination->result=$request->file;
       $examination->save();

        }
        return redirect()->action('examinationController@index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
