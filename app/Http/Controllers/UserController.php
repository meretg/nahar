<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Input;
use App\User;
use App\role;
use App\permission;
use App\Doctor;
use App\service;
use App\serviceName;
use App\type;
use App\doctorType;
use Hash;
use Auth;
use View;
use Redirect;
use Validator;
use Illuminate\Support\Facades\Session;
class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function login(Request $request){
       $loginroles=array();
       $loginroles=role::all();
       $check=Auth::attempt(['userName'=>$request->userName,'password'=>$request->password]);

       if($check){
         $user=User::where('userName',$request->userName)->first();
         $permissions=permission::where('user_id',$user->id)->get();
         if($user->superAdmin==1){
           return redirect()->action('UserController@index');
         }
         else{


           $permissions=permission::where('user_id',Auth::user()->id)->get();
           foreach ($permissions as $permission) {
             $x=0;
             $role=role::findOrFail($permission->role_id);
             $loginroles[$x++]=$role;
           }
           foreach ($permissions as $permission) {
             $role=role::findOrFail($permission->role_id);
             switch ($role->name) {
             case 'استقبال':
                    return redirect()->action('receptionController@index');
                 break;
             case 'المرضى':
                   return redirect()->action('receptionistController@index');
                   break;
            case 'الخزنه':
                 return redirect()->action('billingController@index');
                break;
              case 'الفحوصات':
                   return redirect()->action('examinationController@index');
                  break;
              case 'العمليات':

               return redirect()->action('operationController@index');
                    break;
                case 'المخازن':
                       return redirect()->action('StoreController@index');
                      break;
             }
           }

         }
}

       else{
         Session::flash('error', 'اسم المستخدم او الرقم السرى خطا');
            return redirect()->back();
       }
     }
public function logout()
 {
    Auth::logout();
     $check=Auth::check();
    // echo($check);
   return redirect ('login');
 }
    public function index()
    {
      if(!Auth::check()){
        return redirect ('login');
      }

      $users=User::where('id','>',1)->get();
      $loginroles=array();
      $roles=array();
      $types=array();

      $doctors=Doctor::all();
      foreach ($doctors as $doctor) {
        // $check=type::where('doctor_id',$doctor->id)->first();
        // if(!$check){
        //   $doctors->unset($doctor);
        //   // return json_encode('jhj');
        // }
        $doctor->types=type::where('doctor_id',$doctor->id)->get();


      }
       $services=serviceName::all();
      foreach ($users as $user) {

        $permissions=permission::where('user_id',$user->id)->get();
        $i=0;
        $roles=array();
        foreach ($permissions as $permission) {
          $role=role::findOrFail($permission->role_id);
          $roles[$i++]=$role;
        }
        $user->roles=$roles;
      }
      $permissions=permission::where('user_id',Auth::user()->id)->get();
     foreach ($permissions as $permission) {
    $x=0;
    $role=role::findOrFail($permission->role_id);
      $loginroles[$x++]=$role;
       }

      return view('admin_dashboard', [
          'users' => $users,
          'roles' => $roles,
          'loginroles' => $loginroles,
          'doctors' => $doctors,
          'services' => $services,
      ]);
      //return json_encode($doctors[0]->types);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

      if($request->id){
        $validation = Validator::make( $request->all(), [
          'NID'=> 'required|numeric',
          'phone' => 'required|min:11|numeric',
          'roles' => 'required',
          'userName' => 'required|alpha',
            ]);

          if( $validation->fails() )
        {
          return json_encode([
                  'errors' => $validation->errors()->getMessages(),
                  'code' => 422
               ]);
        }
        $user=User::findOrFail($request->id);
        $oldPermissions=permission::where('user_id',$user->id)->delete();
      }
      else{
        $validation = Validator::make( $request->all(), [
          'NID'=> 'required|unique:users,NID,numeric',
          'phone' => 'required|min:11|numeric',
          'roles' => 'required',
          'userName' => 'required|alpha',
          'password' => 'required',
            ]);

          if( $validation->fails() )
        {
          return json_encode([
                  'errors' => $validation->errors()->getMessages(),
                  'code' => 422
               ]);
        }


        $user=new User();
      }

      $user->userName=$request->userName;
      $user->NID=$request->NID;
      if($request->password){
        $user->password=Hash::make($request->password);
      }
      $user->phone=$request->phone;
      $user->superAdmin=0;
      $user->save();
      if($request->roles){
        foreach ($request->roles as $id) {
          if($id==7){
            $user->superAdmin=1;
            $user->save();
          }
      $permission= new permission();
      $permission->user_id=$user->id;

        $permission->role_id=$id;
          $permission->save();
      }


      }
      $response=new \stdClass;
      $response->success="تم اضافه موظف";
 return json_encode($response);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function viewDoctor(Request $request){
       $doctor=Doctor::findOrFail($request->id);
         return json_encode($doctor);
     }
     public function editDoctor(Request $request){
         $doctor=Doctor::findOrFail($request->id);
         $types=type::where('doctor_id',$request->id)->pluck('id');
         $oldTypes=type::whereIn('id',$types)->delete();
         $doctor->docName=$request->docName;
       //   if($request->type!='none'){
       //   $doctor->type=$request->type;
       // }

       foreach ($request->type as $doctor_type) {
         $doctorType=doctorType::where('name',$doctor_type)->first();
         $type=new type;
         $type->type_id=$doctorType->id;
         $type->doctor_id=$doctor->id;
         $type->save();
       }
         $doctor->phone=$request->phone;
         $doctor->save();
        // return redirect()->action('UserController@index');

        $response=new \stdClass;
        $response->success="تم اضافه طبييب";
      return json_encode($response);


     }
     // public function addService(Request $request)
     // {
     //
     //
     //     $service=new service;
     //     $service->name=$request->name;
     //     $service->examinationType=$request->examinationType;
     //     $service->commission=$request->commission;
     //     $service->save();
     //     $response=new \stdClass;
     //
     //       Session::flash('message', 'تم اضافه خدمه');
     //     return redirect()->action('UserController@index');
     //
     //
     // }
     public function editService(Request $request){
       $service=serviceName::findOrFail($request->id);
       if($request->ajax()){
         return json_encode($service);
       }
       else{
         // $service->name=$request->name;
         // $service->examinationType=$request->examinationType;
         $service->commission=$request->commission;
         $service->save();
         return redirect()->action('UserController@index');

       }
     }
    public function addDoctor(Request $request)
    {


        $doctor=new Doctor;
        $doctor->docName=$request->docName;
        $doctor->phone=$request->phone;
        $doctor->save();
        foreach ($request->type as $doctor_type) {
          $doctorType=doctorType::where('name',$doctor_type)->first();
          $type=new type;
          $type->type_id=$doctorType->id;
          $type->doctor_id=$doctor->id;
          $type->save();
        }

        $response=new \stdClass;
        $response->success="تم اضافه طبيب.$doctorType->name";
        return json_encode($response);
        //   Session::flash('message', 'تم اضافه طبيب');
        // return redirect()->action('UserController@index');


    }
      public function deleteDoctor(Request $request){
        $doctor= Doctor::findOrFail($request->id);
        $doctor->delete();
        Session::flash('message', 'تم حذف طبيب');
        return redirect()->action('UserController@index');

      }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
      $roles=array();
      $i=0;
        $user= User::findOrFail($request->id);
        $permissions=permission::where('user_id',$user->id)->get();
        foreach ($permissions as $id) {
          $role=role::findOrFail($id);
          $roles[$i++]=$role;

        }
        $user->roles=$roles;

        return json_encode($user);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
    $user= User::findOrFail($request->id);
    $user->delete();
    Session::flash('message', 'تم حذف الموظف');
    return redirect()->action('UserController@index');
    }
}
