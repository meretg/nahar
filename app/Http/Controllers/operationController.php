<?php

namespace App\Http\Controllers;
use App\patient;
use App\operation;
use App\examination;
use App\permission;
use App\role;
use App\service;
use Auth;
use  Carbon;
use Illuminate\Http\Request;

class operationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $z=0;
      $loginroles=array();
     if(!Auth::check()&&$permission){
       return redirect ('login');
     }

    $permissions=permission::where('user_id',Auth::user()->id)->get();
   foreach ($permissions as $permission) {

  $role=role::findOrFail($permission->role_id);
    $loginroles[$z++]=$role;
     }
      $i=0;
      $x=0;
      $y=0;
      $services=array();
      $services_confirmed=array();
      $services_rejected=array();
        $services_array=service::where('examinationType','normalOperation')->orwhere('examinationType','lasikOperation')->get();
        foreach ($services_array as $service)
        {
          $reception= new receptionistController();
          $patient=patient::findOrFail($service->patient_id);

          $operation=operation::where('service_id',$service->id)->first();
          $service->operation=$operation;
          $service->age=$reception->date($patient->DOB);
          $service->patient=$patient;

      if($service->status=='confirmed'){
          if($service->serviceStatus=='pending'){
              $services[$i++]=$service;
          }
          if($service->serviceStatus=='confirmed'){
              $services_confirmed[$x++]=$service;
          }
          if($service->serviceStatus=='rejected'){
              $services_rejected[$y++]=$service;
          }


        }

        }

        return view('operations', [
            'services' => $services,
            'services_confirmed' => $services_confirmed,
              'services_rejected' => $services_rejected,
              'loginroles' => $loginroles,
        ]);
    //   return json_encode($services_array);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $x=0;
      $i=0;
      $y=0;

      $services_rejected=array();
      $service=service::findOrFail($request->id);
      if($request->reason){
        $operation=operation::findOrFail($request->operationId);
        $operation->reason=$request->reason;
        $service->serviceStatus='rejected';
        $operation->save();
        $service->save();

      }
      else{

        $service->serviceStatus='confirmed';
        $service->updated_at= Carbon\Carbon::now();
        $service->save();

      }

      // $services=array();
      // $services_confirmed=array();
      //   $services_array=service::where('examinationType','normalOperation')->orwhere('examinationType','lasikOperation')->get();
        // foreach ($services_array as $service)
        // {
        //   $reception= new receptionistController();
        //   $patient=patient::findOrFail($service->patient_id);
        //
        //   $operation=operation::where('service_id',$service->id)->first();
        //   $service->operation=$operation;
        //   $service->age=$reception->date($patient->DOB);
        // $service->patient=$patient;
        //
        //
        //       if($service->status=='confirmed'){
        //           if($service->serviceStatus=='pending'){
        //               $services[$i++]=$service;
        //           }
        //           if($service->serviceStatus=='confirmed'){
        //               $services_confirmed[$x++]=$service;
        //           }
        //           if($service->serviceStatus=='rejected'){
        //               $services_rejected[$y++]=$service;
        //           }
        //
        //
        //         }
        //
        // }
        //
        // return view('operations', [
        //     'services' => $services,
        //     'services_confirmed' => $services_confirmed,
        //     'services_rejected' => $services_rejected,
        // ]);
      return redirect()->action('operationController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
