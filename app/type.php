<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class type extends Model
{
  public $table="types";
  public function doctorType(){
       return $this->hasMany('App\doctorType');
    }
    public function doctor(){
         return $this->hasMany('App\doctor');
      }
}
