<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class permission extends Model
{
      public $table="permissions";
      public function user(){
         return $this->hasMany('App\User');
      }
      public function role(){
         return $this->hasMany('App\role');
      }
}
