<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class storeItem extends Model
{
  public $table="storeItems";
  public function item(){
       return $this->hasMany('App\item');
    }
    public function store(){
         return $this->hasMany('App\store');
      }
}
