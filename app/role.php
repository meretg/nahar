<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class role extends Model
{
      public $table="roles";
      public function permission(){
         return $this->belongsTo('App\permission');
      }
}
