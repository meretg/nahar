<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Doctor extends Model
{
    public $table="doctors";
    public function type(){
       return $this->belongsTo('App\type');
    }
}
