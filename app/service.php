<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class service extends Model
{
  public $table="services";
  public function examination(){
       return $this->hasMany('App\examination');
    }
    public function operation(){
       return $this->hasMany('App\operation');
    }
    public function patient(){
       return $this->belongsTo('App\patient');
    }
}
