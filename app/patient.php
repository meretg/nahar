<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class patient extends Model
{
  //protected $dates = ['DOB'];
  public $table="patients";
  public function service(){
       return $this->hasMany('App\service');
    }
    public function code(){
       return $this->belongsTo('App\code');
    }

}
