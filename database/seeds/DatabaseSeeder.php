<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('stores')->insert([
          'id' => 1,
          'name' => 'store',
          'description' => 'main store',

      ]);
      DB::table('stores')->insert([
          'id' => 2,
          'name' => 'normal_operation',
          'description' => 'store',

      ]);
      DB::table('stores')->insert([
          'id' => 3,
          'name' => 'lasik_operation',
          'description' => ' store',

      ]);
      DB::table('items')->insert([
          'id' => 1,
          'name' => 'عدسه خلفيه صلبه',
          'description' => ' item',
          'quantity' => '5',

          'price' => ' 20',


      ]);
      DB::table('items')->insert([
          'id' => 2,
          'name' =>'عدسه خلفيه رخوه',
          'description' => ' item',
          'quantity' => ' 5',

          'price' => ' 20',


      ]);
      DB::table('items')->insert([
          'id' => 3,
          'name' => 'عدسه اماميه',
          'description' => ' item',
          'quantity' => ' 5',

          'price' => ' 20',


      ]);
      DB::table('items')->insert([
          'id' => 4,
          'name' => 'هايلون',
          'description' => ' item',
          'quantity' => '5',

          'price' => ' 20',


      ]);
      DB::table('items')->insert([
          'id' => 5,
          'name' => 'ميثيل',
          'description' => ' item',
          'quantity' => ' 5',

          'price' => ' 20',


      ]);
      DB::table('items')->insert([
          'id' => 6,
          'name' => 'صبغه',
          'description' => ' item',
          'quantity' => ' 5',

          'price' => ' 20',

      ]);
      DB::table('items')->insert([
          'id' => 7,
          'name' => 'local',
          'description' => ' item',
          'quantity' => ' 5',

          'price' => ' 20',


      ]);
      DB::table('items')->insert([
          'id' => 8,
          'name' => 'كيراتوم new',
          'description' => ' item',
          'quantity' => ' 5',

          'price' => ' 20',


      ]);
      DB::table('items')->insert([
          'id' => 9,
          'name' => 'كيراتوم الكون',
          'description' => ' item',
          'quantity' => ' 5',

          'price' => ' 20',


      ]);
      DB::table('items')->insert([
          'id' => 10,
          'name' => 'new حربه',
          'description' => ' item',
          'quantity' => ' 5',

          'price' => ' 20',


      ]);
      DB::table('items')->insert([
          'id' => 11,
          'name' => 'new بروب',
          'description' => ' item',
          'quantity' => ' 5',

          'price' => ' 20',


      ]);
      DB::table('items')->insert([
          'id' => 12,
          'name' => 'مشرط 15',
          'description' => ' item',
          'quantity' => ' 5',

          'price' => ' 20',


      ]);
      DB::table('items')->insert([
          'id' => 13,
          'name' => 'كارتج',
          'description' => ' item',
          'quantity' => ' 5',

          'price' => ' 20',


      ]);
      DB::table('items')->insert([
          'id' => 14,
          'name' => 'كارتج الكون',
          'description' => ' item',
          'quantity' => ' 5',

          'price' => ' 20',

      ]);
      DB::table('items')->insert([
          'id' => 15,
          'name' => 'حرير 0/4',
          'description' => ' item',
          'quantity' => ' 5',

          'price' => ' 20',


      ]);
      DB::table('items')->insert([
          'id' => 16,
          'name' => 'برلين 0/5',
          'description' => ' item',
          'quantity' => ' 5',

          'price' => ' 20',


      ]);
      DB::table('items')->insert([
          'id' => 17,
          'name' => 'برلين 0/6',
          'description' => ' item',
          'quantity' => ' 5',
          'price' => ' 20',


      ]);
      DB::table('items')->insert([
          'id' => 18,
          'name' => 'برلين 5//0',
          'description' => ' item',
          'quantity' => ' 5',
          'price' => ' 20',


      ]);
      DB::table('items')->insert([
          'id' => 19,
          'name' => 'برلين 6//0',
          'description' => ' item',
          'quantity' => ' 5',

          'price' => ' 20',


      ]);
      DB::table('items')->insert([
          'id' => 20,
          'name' => 'فيكريل 0/6',
          'description' => ' item',
          'quantity' => ' 5',

          'price' => ' 20',

      ]);
      DB::table('items')->insert([
          'id' => 21,
          'name' => 'عدسه لاصقه',
          'description' => ' item',
          'quantity' => ' 5',

          'price' => ' 20',


      ]);
      DB::table('items')->insert([
          'id' => 22,
          'name' => 'blades',
          'description' => ' item',
          'quantity' => ' 5',

          'price' => ' 20',


      ]);
      DB::table('items')->insert([
          'id' => 23,
          'name' => 'فوطه عين',
          'description' => ' item',
          'quantity' => ' 5',
          'price' => ' 20',


      ]);
      DB::table('items')->insert([
          'id' => 24,
          'name' => 'جاون طبيب',
          'description' => ' item',
          'quantity' => ' 5',
          'price' => ' 20',


      ]);
      DB::table('items')->insert([
          'id' => 25,
          'name' => 'جاون مريض',
          'description' => ' item',
          'quantity' => ' 5',
          'price' => ' 20',


      ]);
      DB::table('items')->insert([
          'id' => 26,
          'name' => 'كانويلا',
          'description' => ' item',
          'quantity' => ' 5',

          'price' => ' 20',


      ]);
      DB::table('items')->insert([
          'id' => 27,
          'name' => 'كارتيلا',
          'description' => ' item',
          'quantity' => ' 5',
          'price' => ' 20',


      ]);
      DB::table('items')->insert([
          'id' => 28,
          'name' => 'ماسك',
          'description' => ' item',
          'quantity' => ' 5',

          'price' => ' 20',


      ]);
      DB::table('items')->insert([
          'id' => 29,
          'name' => 'اوفر هيد',
          'description' => ' item',
          'quantity' => ' 5',

          'price' => ' 20',


      ]);
      DB::table('roles')->insert([
          'id' => 1,
          'name' => 'استقبال',
      ]);
      DB::table('roles')->insert([
          'id' => 2,
          'name' => 'المرضى',
      ]);
      DB::table('roles')->insert([
          'id' => 3,
          'name' => 'الخزنه',
      ]);
      DB::table('roles')->insert([
          'id' => 4,
          'name' => 'الفحوصات',
      ]);
      DB::table('roles')->insert([
          'id' => 5,
          'name' => 'العمليات',
      ]);
      DB::table('roles')->insert([
          'id' => 6,
          'name' => 'المخازن',
      ]);
      DB::table('roles')->insert([
          'id' => 7,
          'name' => 'المديرين',
      ]);
      DB::table('users')->insert([
          'id' => 1,
          'userName' => 'alnahar',
          'NID' => '12345678901234',
          'password' => bcrypt('alnahar123'),
          'superAdmin' => '1',

      ]);
      DB::table('serviceNames')->insert([
          'id' => 1,
          'name' => 'checkUp',
          'commission' => 0,

      ]);
       // DB::table('serviceNames')->insert([
       //     'id' => 2,
       //     'name' => 'normalExamination',
       //     'commission' => 0,
       //
       // ]);
       //
       // DB::table('serviceNames')->insert([
       //     'id' => 3,
       //     'name' => 'lasikExamination',
       //     'commission' => 0,
       //
       // ]);
       // DB::table('serviceNames')->insert([
       //     'id' => 4,
       //     'name' => 'lasikOperation',
       //     'commission' => 0,
       //
       // ]);
       // DB::table('serviceNames')->insert([
       //     'id' => 5,
       //     'name' => 'normalOperation',
       //     'commission' => 0,
       //
       // ]);

       DB::table('serviceNames')->insert([
           'id' => 6,
           'name' => 'repeat',
           'commission' => 0,

       ]);
      DB::table('serviceNames')->insert([
          'id' => 7,
          'name' => 'جلسه_ياج_ليزر_مسح_عدسه_yag_OU',
          'commission' => 0,

      ]);
      DB::table('serviceNames')->insert([
          'id' => 8,
          'name' => 'جلسه_ياج_ليزر_مسح_عدسه_yag',
          'commission' => 0,

      ]);
      DB::table('serviceNames')->insert([
          'id' => 9,
          'name' => 'اشعه_تلفزيونيه_US_OU',
          'commission' => 0,

      ]);
      DB::table('serviceNames')->insert([
          'id' => 10,
          'name' => 'اشعه_تلفزيونيه_US',
          'commission' => 0,

      ]);
      DB::table('serviceNames')->insert([
          'id' => 11,
          'name' => 'مقاس_عدسه_OU',
          'commission' => 0,

      ]);
      DB::table('serviceNames')->insert([
          'id' => 12,
          'name' => 'مقاس_عدسه',
          'commission' => 0,

      ]);
      DB::table('serviceNames')->insert([
          'id' => 13,
          'name' => 'ازاله_جسم_غريب_حجره_الكشف',
          'commission' => 0,

      ]);
      DB::table('serviceNames')->insert([
          'id' => 14,
          'name' => 'ازاله_جسم_غريب_حجره_الكشف_OU',
          'commission' => 0,

      ]);

      DB::table('serviceNames')->insert([
          'id' => 15,
          'name' => 'Pantcam',
          'commission' => 0,

      ]);
      DB::table('serviceNames')->insert([
          'id' => 16,
          'name' => 'lasikAssessment',
          'commission' => 0,

      ]);
      DB::table('serviceNames')->insert([
          'id' => 17,
          'name' => 'aberrometry',
          'commission' => 0,

      ]);
      DB::table('serviceNames')->insert([
          'id' => 18,
          'name' => 'pentacam',
          'commission' => 0,

      ]);
      DB::table('serviceNames')->insert([
          'id' => 19,
          'name' => 'topography',
          'commission' => 0,

      ]);
      DB::table('serviceNames')->insert([
          'id' => 20,
          'name' =>'مياه_بيضا_عدسه_هندى_phaco_زرع_العدسه(CLE)',
          'commission' => 0,

      ]);
      DB::table('serviceNames')->insert([
          'id' => 21,
          'name' => 'مياه_بيضا_عدسه_امريكى_phaco',
          'commission' => 0,

      ]);
      DB::table('serviceNames')->insert([
          'id' => 22,
          'name' => 'مياه_بيضا_جراحه_Extra_cap(ازاله_عدسه+مياه_بيضاء)',
          'commission' => 0,

      ]);
      DB::table('serviceNames')->insert([
          'id' => 23,
          'name' => 'مياه_بيضا-زرقا_phaco_trabe',
          'commission' => 0,

      ]);
      DB::table('serviceNames')->insert([
          'id' => 24,
          'name' => 'عدسه_هندى',
          'commission' => 0,

      ]);
      DB::table('serviceNames')->insert([
          'id' => 25,
          'name' => 'مياه_زرقا_فقط',
          'commission' => 0,

      ]);
      DB::table('serviceNames')->insert([
          'id' => 26,
          'name' => 'الظفره(ptery_gium)',
          'commission' => 0,

      ]);
      DB::table('serviceNames')->insert([
          'id' => 27,
          'name' => 'حقن_افاستين_بالحقنه(AV_astin)_عين',
          'commission' => 0,

      ]);
      DB::table('serviceNames')->insert([
          'id' => 28,
          'name' => 'حقن_ليوسنتيس_عين',
          'commission' => 0,

      ]);
      DB::table('serviceNames')->insert([
          'id' => 29,
          'name' => 'حقن_الايليا_عين',
          'commission' => 0,

      ]);
      DB::table('serviceNames')->insert([
          'id' => 30,
          'name' => 'كيس_دهنى',
          'commission' => 0,

      ]);
      DB::table('serviceNames')->insert([
          'id' => 31,
          'name' => 'ازاله_حسنه/ازاله_وحمه',
          'commission' => 0,

      ]);
      DB::table('serviceNames')->insert([
          'id' => 32,
          'name' => 'زرع_قرنيه(Keratoplasty)',
          'commission' => 0,

      ]);
      DB::table('serviceNames')->insert([
          'id' => 33,
          'name' => 'ارتخاء_الجفن',
          'commission' => 0,

      ]);
      DB::table('serviceNames')->insert([
          'id' => 34,
          'name' => 'squintحول(عين_اتنين)(تجميل)',
          'commission' => 0,

      ]);
      DB::table('serviceNames')->insert([
          'id' => 35,
          'name' => 'كيس_دمعى',
          'commission' => 0,

      ]);
      DB::table('serviceNames')->insert([
          'id' => 36,
          'name' => 'تسليك_قناه_دمعيه(عين_عنين)(probing)',
          'commission' => 0,

      ]);
      DB::table('serviceNames')->insert([
          'id' => 37,
          'name' => 'فحص_تحت_مخدر_عام(قياس_ضغط_عين_تحت_تخدير)',
          'commission' => 0,

      ]);
      DB::table('serviceNames')->insert([
          'id' => 38,
          'name' => 'ازاله_كيس_بالملتحمه/ازاله_غده',
          'commission' => 0,

      ]);
      DB::table('serviceNames')->insert([
          'id' => 39,
          'name' => 'تفريغ_عين',
          'commission' => 0,

      ]);
      DB::table('serviceNames')->insert([
          'id' => 40,
          'name' => 'شبكية',
          'commission' => 0,

      ]);
      DB::table('serviceNames')->insert([
          'id' => 41,
          'name' => 'شبكيه_من_غير_سيليكون_وبرفلوكاريون',
          'commission' => 0,

      ]);
      DB::table('serviceNames')->insert([
          'id' => 42,
          'name' => 'ازاله_سيليكون',
          'commission' => 0,

      ]);
      DB::table('serviceNames')->insert([
          'id' => 43,
          'name' => 'صبغ_القرنيه_التاتون',
          'commission' => 0,

      ]);
      DB::table('serviceNames')->insert([
          'id' => 44,
          'name' => 'زانس_لازمه',
          'commission' => 0,

      ]);
      DB::table('serviceNames')->insert([
          'id' => 45,
          'name' => 'خياطه_غرز_بالجفن',
          'commission' => 0,

      ]);
      DB::table('serviceNames')->insert([
          'id' => 46,
          'name' => 'عمليه_شعره(ازاله_شعره)',
          'commission' => 0,

      ]);
      DB::table('serviceNames')->insert([
          'id' => 47,
          'name' => 'ازاله_غرز',
          'commission' => 0,

      ]);
      DB::table('serviceNames')->insert([
          'id' => 48,
          'name' => 'اصابه_بالعين',
          'commission' => 0,

      ]);
      DB::table('serviceNames')->insert([
          'id' => 49,
          'name' => 'مياه_بيضا_+ازاله_سيليكون',
          'commission' => 0,

      ]);
      DB::table('serviceNames')->insert([
          'id' => 50,
          'name' => 'ازاله_جسم_غريب_بالقرنيه',
          'commission' => 0,

      ]);
      DB::table('serviceNames')->insert([
          'id' => 51,
          'name' => 'proscan_OU',
          'commission' => 0,

      ]);
      DB::table('serviceNames')->insert([
          'id' => 52,
          'name' => 'TouchUP_OU',
          'commission' => 0,

      ]);
      DB::table('serviceNames')->insert([
          'id' => 53,
          'name' => 'CustomLasik_OU',
          'commission' => 0,

      ]);
      DB::table('serviceNames')->insert([
          'id' => 54,
          'name' => 'supracor_OU',
          'commission' => 0,

      ]);
      DB::table('serviceNames')->insert([
          'id' => 55,
          'name' => 'CustomLasik',
          'commission' => 0,

      ]);
      DB::table('serviceNames')->insert([
          'id' => 56,
          'name' => 'supracor',
          'commission' => 0,

      ]);
      DB::table('serviceNames')->insert([
          'id' => 57,
          'name' => 'crossLiniking_OU',
          'commission' => 0,

      ]);


      DB::table('serviceNames')->insert([
          'id' => 58,
          'name' => 'proscan',
          'commission' => 0,

      ]);
      DB::table('serviceNames')->insert([
          'id' => 59,
          'name' => 'TouchUP',
          'commission' => 0,

      ]);
      DB::table('serviceNames')->insert([
          'id' => 60,
          'name' => 'مياه_بيضاء_للاطفال_بدون_زرع_عدسه',
          'commission' => 0,

      ]);
      DB::table('serviceNames')->insert([
          'id' => 61,
          'name' => 'زرع_عدسه_ثانويه_صلبه',
          'commission' => 0,

      ]);
      DB::table('serviceNames')->insert([
          'id' => 62,
          'name' => 'زرع_عدسه_ثانويه_ثلاثيه',
          'commission' => 0,

      ]);
      DB::table('serviceNames')->insert([
          'id' => 63,
          'name' => 'زرع_عدسه_ذكيه',
          'commission' => 0,

      ]);

      DB::table('serviceNames')->insert([
          'id' => 64,
          'name' => 'زرع_عدسه_ثانويه_ارتيزان(من_غير_phaco)',
          'commission' => 0,

      ]);
      DB::table('serviceNames')->insert([
          'id' => 65,
          'name' => 'تعديل_عدسه',
          'commission' => 0,

      ]);
      DB::table('serviceNames')->insert([
          'id' => 66,
          'name' => 'ازاله_جسم_غريب',
          'commission' => 0,

      ]);
       DB::table('serviceNames')->insert([

            'id' => 67,
            'name' => 'مسح_عدسه_بالجراحه',
            'commission' => 0,

        ]);
        DB::table('serviceNames')->insert([
            'id' => 68,
            'name' => 'phaco+AV_astin',
            'commission' => 0,

        ]);
        DB::table('serviceNames')->insert([
            'id' => 69,
            'name' => 'مياه_بيضا_عدسه(بوش_اندلومب)',
            'commission' => 0,

        ]);
        DB::table('serviceNames')->insert([
            'id' => 70,
            'name' => 'مياه_بيضا_جراحه_بدون_زرع_عدسه',
            'commission' => 0,

        ]);
        DB::table('serviceNames')->insert([
            'id' => 71,
            'name' => 'phacoبدون_عدسه',
            'commission' => 0,

        ]);
        DB::table('serviceNames')->insert([
            'id' => 72,
            'name' => 'عدسه_امريكى',
            'commission' => 0,

        ]);
        DB::table('serviceNames')->insert([
            'id' => 73,
            'name' => 'مياه_بيضا_عدسه_ارتيزان',
            'commission' => 0,

        ]);
        DB::table('serviceNames')->insert([
            'id' => 74,
            'name' => 'حقن_افاستين_بالحقنه_عين',
            'commission' => 0,

        ]);
        DB::table('serviceNames')->insert([
            'id' => 75,
            'name' => 'حقن_افاستين_بالحقنه_OU',
            'commission' => 0,

        ]);
        DB::table('serviceNames')->insert([
            'id' => 76,
            'name' => 'حقن_كيناكورت_عين',
            'commission' => 0,

        ]);
        DB::table('serviceNames')->insert([
            'id' => 77,
            'name' => 'حقن_كيناكورت_OU',
            'commission' => 0,

        ]);
        DB::table('serviceNames')->insert([
            'id' => 78,
            'name' => 'حقن_ليوسنتيس_OU',
            'commission' => 0,

        ]);
        DB::table('serviceNames')->insert([
            'id' => 79,
            'name' => 'حقن_الايليا_OU',
            'commission' => 0,

        ]);
        DB::table('serviceNames')->insert([
            'id' => 80,
            'name' => 'ارتخاء_الجفن_OU',
            'commission' => 0,

        ]);
        DB::table('serviceNames')->insert([
            'id' => 81,
            'name' => 'تركيب_عدسه_لاصقه_للاطفال',
            'commission' => 0,

        ]);
        DB::table('serviceNames')->insert([
            'id' => 82,
            'name' => 'crossLiniking',
            'commission' => 0,

        ]);
        DB::table('serviceNames')->insert([
            'id' => 83,
            'name' => 'proscan+CustomLasik',
            'commission' => 0,

        ]);
        DB::table('serviceNames')->insert([
            'id' => 84,
            'name' => 'proscan+supracor',
            'commission' => 0,

        ]);
        DB::table('serviceNames')->insert([
            'id' => 85,
            'name' => 'proscan+crossLiniking',
            'commission' => 0,

        ]);
        DB::table('serviceNames')->insert([
            'id' => 86,
            'name' => 'CustomLasik+supracor',
            'commission' => 0,

        ]);
        DB::table('serviceNames')->insert([
            'id' => 87,
            'name' => 'CustomLasik+crossLiniking',
            'commission' => 0,

        ]);
        DB::table('serviceNames')->insert([
            'id' => 88,
            'name' => 'supracor+crossLiniking',
            'commission' => 0,

        ]);
        DB::table('serviceNames')->insert([
            'id' => 89,
            'name' => 'Pantcam_OU',
            'commission' => 0,

        ]);
        DB::table('serviceNames')->insert([
            'id' => 90,
            'name' => 'orb_scan',
            'commission' => 0,

        ]);
        DB::table('serviceNames')->insert([
            'id' => 91,
            'name' => 'orb_scan_OU',
            'commission' => 0,

        ]);
        DB::table('serviceNames')->insert([
            'id' => 92,
            'name' => 'فحص_فلورسين_للعين_FFAA',
            'commission' => 0,

        ]);
        DB::table('serviceNames')->insert([
            'id' => 93,
            'name' =>  'فحص_فلورسين_للعينين_FFAA',
            'commission' => 0,

        ]);
        DB::table('serviceNames')->insert([
            'id' => 94,
            'name' => 'فحص_للعين_OCT',
            'commission' => 0,

        ]);
        DB::table('serviceNames')->insert([
            'id' => 95,
            'name' => 'فحص_للعينين_OCT',
            'commission' => 0,

        ]);
        DB::table('serviceNames')->insert([
            'id' => 96,
            'name' => 'فحص_عين_فلورسين_OCT_عين',
            'commission' => 0,

        ]);
        DB::table('serviceNames')->insert([
            'id' => 97,
            'name' => 'فحص_عين_عينين_فلورسين_OCT_لعينين',
            'commission' => 0,

        ]);
        DB::table('serviceNames')->insert([
            'id' => 98,
            'name' => 'فحص_عين_عنين_OCT_فلورستين_للعنين',
            'commission' => 0,

        ]);
        DB::table('serviceNames')->insert([
            'id' => 99,
            'name' => 'فحص_عين_OCT_فلورسين_للعنين',
            'commission' => 0,

        ]);
        // DB::table('serviceNames')->insert([
        //     'id' => 100,
        //     'name' => 'فحص_عين_فلورسين_OCT_عين',
        //     'commission' => 0,
        //
        // ]);
        // DB::table('serviceNames')->insert([
        //     'id' => 101,
        //     'name' => 'فحص_عين_فلورسين_OCT_عين',
        //     'commission' => 0,
        //
        // ]);
        // DB::table('serviceNames')->insert([
        //     'id' => 102,
        //     'name' => 'فحص_عين_فلورسين_OCT_عين',
        //     'commission' => 0,
        //
        // ]);
        // DB::table('serviceNames')->insert([
        //     'id' => 103,
        //     'name' => 'فحص_عين_فلورسين_OCT_عين',
        //     'commission' => 0,
        //
        // ]);
        // DB::table('serviceNames')->insert([
        //     'id' => 104,
        //     'name' => 'فحص_عين_فلورسين_OCT_عين',
        //     'commission' => 0,
        //
        // ]);
        // DB::table('serviceNames')->insert([
        //     'id' => 105,
        //     'name' => 'فحص_عين_فلورسين_OCT_عين',
        //     'commission' => 0,
        //
        // ]);
        // DB::table('serviceNames')->insert([
        //     'id' => 106,
        //     'name' => 'فحص_عين_فلورسين_OCT_عين',
        //     'commission' => 0,
        //
        // ]);
        // DB::table('serviceNames')->insert([
        //     'id' => 96,
        //     'name' => 'فحص_عين_فلورسين_OCT_عين',
        //     'commission' => 0,
        //
        // ]);
        // DB::table('serviceNames')->insert([
        //     'id' => 96,
        //     'name' => 'فحص_عين_فلورسين_OCT_عين',
        //     'commission' => 0,
        //
        // ]);
        // DB::table('serviceNames')->insert([
        //     'id' => 96,
        //     'name' => 'فحص_عين_فلورسين_OCT_عين',
        //     'commission' => 0,
        //
        // ]);
        // DB::table('serviceNames')->insert([
        //     'id' => 96,
        //     'name' => 'فحص_عين_فلورسين_OCT_عين',
        //     'commission' => 0,
        //
        // ]);


      DB::table('doctorTypes')->insert([
          'id' => 1,
          'name' => 'surgeon',

      ]);
      DB::table('doctorTypes')->insert([
          'id' => 2,
          'name' => 'checkup',

      ]);
      DB::table('doctorTypes')->insert([
          'id' => 3,
          'name' => 'anesthetist',

      ]);
      DB::table('doctorTypes')->insert([
          'id' => 4,
          'name' => 'assistantDoctor',

      ]);
      DB::table('doctorTypes')->insert([
          'id' => 5,
          'name' => 'data',

      ]);

    }
}
