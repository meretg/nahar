<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoreItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('storeItems', function (Blueprint $table) {
        $table->increments('id');
        $table->integer('store_id')->unsigned();
        $table->foreign('store_id')->references('id')->on('stores')
        ->onUpdate('cascade')->onDelete('cascade');
        $table->integer('item_id')->unsigned();
        $table->foreign('item_id')->references('id')->on('items')
        ->onUpdate('cascade')->onDelete('cascade');
        $table->integer('storeQuantity')->default(0);
        $table->integer('storeQuantityLoss')->default(0);


          $table->timestamps();
      });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('storeItems');
    }
}
