<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExaminationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('examinations', function (Blueprint $table) {
          $table->increments('id');
          $table->string('result')->default('null');
          $table->enum('diagonistics', ['none','جلسه_ياج_ليزر_مسح_عدسه_yag_OU','جلسه_ياج_ليزر_مسح_عدسه_yag','اشعه_تلفزيونيه_US_OU','اشعه_تلفزيونيه_US',
          'مقاس_عدس','مقاس_عدسه','ازاله_جسم_غريب_حجره_الكشف','ازاله_جسم_غريب_حجره_الكشف_OU','كشف','اعاده_كشف',
           'فحص_فلورسين_للعين_FFAA','فحص_فلورسين_للعينين_FFAA','فحص_للعين_OCT','فحص_للعينين_OCT','فحص_عين_فلورسين_OCT_عين',
           'فحص_عين_عينين_فلورسين_OCT_لعينين','فحص_عين_عنين_OCT_فلورستين_للعنين','فحص_عين_OCT_فلورسين_للعنين'])->default('none');
          $table->string('other')->default('null');
          $table->enum('investigation', ['none','Pantcam','Pantcam_OU','orb_scan','orb_scan_OU'])->default('none');
          $table->string('others')->default('null');
          $table->integer('service_id')->unsigned();
          $table->foreign('service_id')->references('id')->on('services')
          ->onUpdate('cascade')->onDelete('cascade');
          $table->boolean('refund')->default(0);
          $table->string('reason')->default('null');
          $table->timestamps();
    });
  }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
         Schema::dropIfExists('examinations');
     }
}
