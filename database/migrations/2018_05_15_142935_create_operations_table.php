<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOperationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('operations', function (Blueprint $table) {
          $table->increments('id');
          $table->json('items')->nullable();
          $table->integer('storeIdLoss')->nullable();
          $table->string('surgeon')->default('none');
          $table->string('dataDoctor')->default('none');
          $table->string('anesthetist')->default('none');
          $table->string('assistantDoctor')->default('none');
          $table->enum('anesthesia', ['none','general', 'local'])->default('none');
          $table->enum('operation', ['none','مياه_بيضا_عدسه_هندى_phaco_زرع_العدسه(CLE)','مياه_بيضا_عدسه_امريكى_phaco','مياه_بيضا_جراحه_Extra_cap(ازاله_عدسه+مياه_بيضاء)','مياه_بيضا-زرقا_phaco_trabe','عدسه_هندى','مياه_زرقا_فقط','الظفره(ptery_gium)','حقن_افاستين_بالحقنه(AV_astin)_عين'
          ,'حقن_ليوسنتيس_عين','حقن_الايليا_عين','كيس_دهنى','ازاله_حسنه/ازاله_وحمه','زرع_قرنيه(Keratoplasty)','ارتخاء_الجفن','squintحول(عين_اتنين)(تجميل)','كيس_دمعى',
          'تسليك_قناه_دمعيه(عين_عنين)(probing)','فحص_تحت_مخدر_عام(قياس_ضغط_عين_تحت_تخدير)','ازاله_كيس_بالملتحمه/ازاله_غده','تفريغ_عين','شبكية','شبكيه_من_غير_سيليكون_وبرفلوكاريون','ازاله_سيليكون','صبغ_القرنيه_التاتون','زانس_لازمه','خياطه_غرز_بالجفن','عمليه_شعره(ازاله_شعره)','ازاله_غرز',
          'اصابه_بالعين','مياه_بيضا_+ازاله_سيليكون','ازاله_جسم_غريب_بالقرنيه','مياه_بيضاء_للاطفال_بدون_زرع_عدسه','زرع_عدسه_ثانويه_صلبه','زرع_عدسه_ثانويه_ثلاثيه','زرع_عدسه_ذكيه','زرع_عدسه_ثانويه_ارتيزان(من_غير_phaco)','تعديل_عدسه','ازاله_جسم_غريب','مسح_عدسه_بالجراحه','phaco+AV_astin','مياه_بيضا_عدسه(بوش_اندلومب)',
          'مياه_بيضا_جراحه_بدون_زرع_عدسه','phacoبدون_عدسه', 'عدسه_امريكى','مياه_بيضا_عدسه_ارتيزان', 'حقن_افاستين_بالحقنه_عين','حقن_افاستين_بالحقنه_OU','حقن_كيناكورت_عين', 'حقن_كيناكورت_OU','حقن_ليوسنتيس_OU','حقن_الايليا_OU','ارتخاء_الجفن_OU','تركيب_عدسه_لاصقه_للاطفال'])->default('none');
          $table->string('expensesFile')->default('null');
          $table->enum('treatment', ['none','crossLiniking','proscan+CustomLasik','proscan+supracor','proscan+crossLiniking','CustomLasik+supracor','CustomLasik+crossLiniking','supracor+crossLiniking',
          'proscan_OU','TouchUP_OU','CustomLasik_OU','supracor_OU', 'CustomLasik','supracor','crossLiniking_OU','proscan', 'TouchUP'])->default('none');
          $table->integer('service_id')->unsigned();
          $table->foreign('service_id')->references('id')->on('services')->onUpdate('cascade')->onDelete('cascade');
          $table->boolean('refund')->default(0);
          $table->string('reason')->default('null');
          $table->integer('loss')->default(0);
          $table->enum('discount', ['none','الطبيب الجراح', 'المساعد','','الطبيب المحول','طبيب التخدير'])->default('none');

          $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('operations');
    }
}
