<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('services', function (Blueprint $table) {
          $table->increments('id');
          $table->enum('examinationType', ['repeat','checkUp', 'normalExamination','lasikExamination','normalOperation','lasikOperation']);
          $table->enum('eye', ['none','OD', 'OS','OU'])->default('none');
          $table->integer('commission')->default(0);
          $table->integer('owedMoney')->nullable();
          $table->integer('transformerDocMoney')->default(0);
          $table->integer('examinerDocMoney')->default(0);
          $table->integer('surgeonMoney')->default(0);
          $table->string('dataDoctorMoney')->default(0);
          $table->integer('anesthetistMoney')->default(0);
          $table->integer('assistantDoctorMoney')->default(0);
          $table->string('transformerDoc')->default('none');
          $table->string('examinerDoc')->nullable()->default('none');
          $table->integer('patient_id')->unsigned();
          $table->foreign('patient_id')->references('id')->on('patients')->onUpdate('cascade')->onDelete('cascade');
          $table->enum('status', ['pending','confirmed'])->default('pending');
          $table->enum('serviceStatus', ['pending','confirmed','rejected'])->default('pending');
          $table->date('created_at');
          $table->date('updated_at');

      });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::dropIfExists('services');
    }
}
