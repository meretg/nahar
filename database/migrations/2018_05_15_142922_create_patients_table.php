<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePatientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('patients', function (Blueprint $table) {
          $table->increments('id');
          $table->string('name');
          $table->string('code')->unique();
          $table->string('NID')->nullable();
          $table->string('phone');
          $table->string('address')->nullable();
          $table->date('DOB');
          $table->date('created_at');
          $table->date('updated_at');
          $table->string('parentName')->nullable();
          $table->string('parentPhone')->nullable();
          $table->string('companionName')->nullable();
          $table->string('companionPhone')->nullable();
          $table->unique(['name', 'code', 'phone']);
          //$table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('patients');
    }
}
